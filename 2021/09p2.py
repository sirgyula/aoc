import timeit


def checkIfDeepest(map, row, col, checking):
    vals = []

    try:
        if col != 0:
            vals.append(checking < map[row][col - 1])
    except IndexError:
        pass

    try:
        vals.append(checking < map[row][col + 1])
    except IndexError:
        pass

    try:
        if row != 0:
            vals.append(checking < map[row - 1][col])
    except IndexError:
        pass

    try:
        vals.append(checking < map[row + 1][col])
    except IndexError:
        pass

    return len(list(filter(lambda v: v is True, vals))) == len(vals)


def checkAndAddValidNeighbours(map, current, basinCoords):
    r = current[0]
    c = current[1]

    try:
        if c != 0 and map[r][c] < map[r][c - 1] and map[r][c - 1] != 9:
            if (r, c - 1) not in basinCoords:
                basinCoords.append((r, c - 1))
    except IndexError:
        pass

    try:
        if map[r][c] < map[r][c + 1] and map[r][c + 1] != 9:
            if (r, c + 1) not in basinCoords:
                basinCoords.append((r, c + 1))
    except IndexError:
        pass

    try:
        if r != 0 and map[r][c] < map[r - 1][c] and map[r - 1][c] != 9:
            if (r - 1, c) not in basinCoords:
                basinCoords.append((r - 1, c))
    except IndexError:
        pass

    try:
        if map[r][c] < map[r + 1][c] and map[r + 1][c] != 9:
            if (r + 1, c) not in basinCoords:
                basinCoords.append((r + 1, c))
    except IndexError:
        pass


def second(filename):
    map = [[int(numeric_string) for numeric_string in arr] for arr in [list(line.rstrip()) for line in open(filename, "r")]]

    colnum = len(map[0])
    rownum = len(map)
    basins = []

    for row in range(0, rownum):
        for col in range(0, colnum):
            if checkIfDeepest(map, row, col, map[row][col]):
                checkedBasinPoints = 0
                basinCoords = [(row, col)]

                while checkedBasinPoints != len(basinCoords):
                    checkAndAddValidNeighbours(map, basinCoords[checkedBasinPoints], basinCoords)
                    checkedBasinPoints += 1

                basins.append(checkedBasinPoints)

    basins = sorted(basins)
    l = len(basins)

    return basins[l - 1] * basins[l - 2] * basins[l - 3]


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("2-0: " + str(second("090.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("091.txt"))), number=1))
