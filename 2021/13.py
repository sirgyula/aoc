import timeit
import re
from operator import itemgetter


def splitCoordsFromInstructions(filename):
    prefilteredlines = list(filter(lambda l: len(l) != 0, [line.rstrip() for line in open(filename, "r")]))
    lines = list([re.search(r'^(\d+),(\d+)$|^fold along ([xy])=(\d+)$', line.rstrip(), ).groups() for line in prefilteredlines])
    coordinates = [(int(line[0]), int(line[1])) for line in lines if line[0] is not None]
    instructions = [(line[2], int(line[3])) for line in lines if line[0] is None]

    return coordinates, instructions


def getMax(coords, axis):
    return max(coords, key=itemgetter(axis))


def changeTupleValue2(p, axis, instruction):
    tmp = list(p)
    tmp[axis] = instruction[1] - (tmp[axis] - instruction[1])
    return tuple(tmp)


def changeTupleValue3(p, axis, diff):
    tmp = list(p)
    tmp[axis] += diff
    return tuple(tmp)


def splitByInstruction(coordinates, instruction):
    axis = 1 if instruction[0] == 'y' else 0

    p1 = list(filter(lambda coord: coord[axis] < instruction[1], coordinates))
    p2 = list(filter(lambda coord: coord[axis] > instruction[1], coordinates))

    p2max = getMax(p2, axis)

    if p2max[axis] >= (instruction[1] * 2):
        p2 = [changeTupleValue2(p, axis, instruction) for p in p2]

        diff = p2max[axis] - instruction[1] * 2

        if diff != 0:
            p1 = [changeTupleValue3(p, axis, diff) for p in p1]

    p1.extend(p2)

    return set(p1)


def draw(coordinates):
    bottomRightCoord = (getMax(coordinates, 0)[0], getMax(coordinates, 1)[1])

    matrix = [['.' for x in range(bottomRightCoord[0] + 1)] for y in range(bottomRightCoord[1] + 1)]

    # elbsztam, végig fordított tengelyen számoltam, rajzolásnál flippelni kell a tuple-t
    for coord in coordinates:
        matrix[coord[1]][coord[0]] = '#'

    for x in range(0, bottomRightCoord[1] + 1):
        print(" ".join(matrix[x]))


def first(filename):
    coordinates, instructions = splitCoordsFromInstructions(filename)
    coordinates = splitByInstruction(coordinates, instructions[0])

    return len(coordinates)


def second(filename):
    coordinates, instructions = splitCoordsFromInstructions(filename)

    for instruction in instructions:
        coordinates = splitByInstruction(coordinates, instruction)

    draw(coordinates)

    return -1


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("130.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("131.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("130.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("131.txt"))), number=1))
