import timeit
import re


class Coordinate:
    def __init__(self, r, c):
        self.r = r
        self.c = c


class BingoBoard:
    def __init__(self):
        self.boardSum = 0
        self.col = [0, 0, 0, 0, 0]
        self.row = [0, 0, 0, 0, 0]
        self.board = []
        self.fin = 0

    def addLine(self, line):
        splittedLine = re.split(r'\s+(?=[0-9])', line)
        self.board.append(splittedLine)
        self.boardSum += sum([int(numeric_string) for numeric_string in splittedLine])

    def checkContainsAndWinCondition(self, num):
        contains = self.contains(num)

        if contains is not None:
            if self.col[contains.c] == 5 or self.row[contains.r] == 5:
                self.fin = 1
                return self.boardSum * int(num)

        return -1

    def contains(self, num):
        for c in range(0, len(self.col)):
            for r in range(0, len(self.row)):
                if self.board[r][c] == num:
                    self.col[c] += 1
                    self.row[r] += 1
                    self.boardSum -= int(self.board[r][c])
                    self.board[r][c] = '-'
                    return Coordinate(r, c)

        return None

    def getSumCol(self, c):
        return sum(n for n in self.board[c])

    def getSumRow(self, r):
        return sum(self.board[r])

    def getBoard(self):
        return self.board


def parseFile(f):
    boards = []

    while f:
        if f.readline() == "":
            f.close()
            break

        b = BingoBoard()

        for i in range(0, 5):
            b.addLine(f.readline().rstrip().lstrip())

        boards.append(b)

    return boards


def first(filename):
    f = open(filename, "r")
    numbers = f.readline().rstrip().split(',')
    boards = parseFile(f)

    for num in numbers:
        for board in boards:
            win = board.checkContainsAndWinCondition(num)

            if win != -1:
                return win

    return -1


def second(filename):
    f = open(filename, "r")
    numbers = f.readline().rstrip().split(',')
    boards = parseFile(f)
    finalWin = 0

    for num in numbers:
        for board in boards:
            if board.fin == 1:
                continue

            win = board.checkContainsAndWinCondition(num)

            if win != -1:
                finalWin = win

    return finalWin


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("040.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("041.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("040.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("041.txt"))), number=1))
