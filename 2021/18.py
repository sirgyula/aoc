import timeit
import re
import math
import sys
import asserthelper


def getLeftRightOfPair(pair):
    return [int(strnum) for strnum in re.search(r'^\[(\d+)\,(\d+)\]$', pair).groups()]


def getFourthPairSubstrPos(snailnum):
    br = 0

    for i in range(len(snailnum) - 1):
        current = snailnum[i]

        if current == '[':
            br += 1
        elif current == ']':
            br -= 1
        else:
            continue

        if br > 4:
            f = i
            t = snailnum[i:].find(']') + 1 + i
            return f, t

    return -1, -1


def explodeLeft(snailnumLeft, left):
    for i in reversed(range(len(snailnumLeft))):
        if snailnumLeft[i].isdigit():
            num = snailnumLeft[i]
            j = i - 1

            while True:
                if snailnumLeft[j].isdigit():
                    num = snailnumLeft[j] + num
                    j -= 1
                else:
                    j += 1
                    break

            num = int(num) + left

            snailnumLeft = snailnumLeft[:j] + str(num) + snailnumLeft[i + 1:]

            return snailnumLeft

    return snailnumLeft


def explodeRight(snailnumRight, right):
    for i in range(len(snailnumRight)):
        if snailnumRight[i].isdigit():
            num = snailnumRight[i]
            j = i + 1

            while True:
                if snailnumRight[j].isdigit():
                    num = num + snailnumRight[j]
                    j += 1
                else:
                    j -= 1
                    break

            num = int(num) + right

            snailnumRight = snailnumRight[:i] + str(num) + snailnumRight[j + 1:]

            return snailnumRight

    return snailnumRight


def explode(snailnum):
    f, t = getFourthPairSubstrPos(snailnum)
    snailnumLeft = snailnum[:f]
    snailnumRight = snailnum[t:]

    if f == -1 and t == -1:
        return "NaP"

    left, right = getLeftRightOfPair(snailnum[f:t])

    snailnumRight = explodeRight(snailnumRight, right)
    snailnumLeft = explodeLeft(snailnumLeft, left)
    snailnum = snailnumLeft + str("0") + snailnumRight

    return snailnum


def getFirstSplitPos(snailnum):
    found = False

    for i in range(len(snailnum)):
        if snailnum[i].isdigit():
            f = i
            t = i

            while snailnum[t + 1].isdigit():
                t += 1
                found = True

            if found:
                return f, t + 1

    return -1, -1


def splitReplace(snailnum, f, t, newPair):
    return snailnum[:f] + newPair + snailnum[t:]


def splitGetNewPair(splittableNum):
    left = math.floor(splittableNum / 2)
    right = math.ceil(splittableNum / 2)
    return "[" + str(left) + "," + str(right) + "]"


def split(snailnum):
    f, t = getFirstSplitPos(snailnum)

    if f == -1 and t == -1:
        return "NaS"

    newPair = splitGetNewPair(float(snailnum[f:t]))
    snailnum = splitReplace(snailnum, f, t, newPair)

    return snailnum


def add(l, r):
    if l == "":
        return r
    else:
        return "[" + l + "," + r + "]"


def reduce(snailnum):
    while True:
        result = explode(snailnum)

        if result == "NaP":
            result = split(snailnum)

            if result == "NaS":
                return snailnum
            else:
                snailnum = result
                continue
        else:
            snailnum = result
            continue


def getMagnitudeOfPair(pair):
    left, right = getLeftRightOfPair(pair)

    return (left * 3) + (right * 2)


def magnitude(snailnum):
    while True:
        r = re.search(r'(\[\d+,\d+\])', snailnum)

        if r is None:
            return snailnum
        else:
            pair = r.groups()[0]
            magnitudeOfPair = getMagnitudeOfPair(pair)
            snailnum = snailnum.replace(pair, str(magnitudeOfPair))


def first(filename):
    lines = [x.rstrip() for x in open(filename, "r").readlines()]
    snailnum = lines[0]

    for i in range(1, len(lines)):
        line = lines[i]
        sum = add(snailnum, line)
        reduced = reduce(sum)
        snailnum = reduced

    return magnitude(snailnum)


def second(filename):
    lines = [x.rstrip() for x in open(filename, "r").readlines()]
    lenLines = len(lines)
    maxMagnitude = -1 * sys.maxsize

    for i in range(lenLines):
        for j in range(i + 1, lenLines):
            magnitude1 = int(magnitude(reduce(add(lines[i], lines[j]))))
            magnitude2 = int(magnitude(reduce(add(lines[j], lines[i]))))

            if magnitude1 > maxMagnitude:
                maxMagnitude = magnitude1

            if magnitude2 > maxMagnitude:
                maxMagnitude = magnitude2

    return maxMagnitude


if __name__ == '__main__':
    print("explode tests:")
    asserthelper.printAssert(explode("[[[[[9,8],1],2],3],4]"), "[[[[0,9],2],3],4]")
    asserthelper.printAssert(explode("[[[[[9,8],11],2],3],4]"), "[[[[0,19],2],3],4]")
    asserthelper.printAssert(explode("[7,[6,[5,[4,[3,2]]]]]"), "[7,[6,[5,[7,0]]]]")
    asserthelper.printAssert(explode("[[6,[5,[11,[3,2]]]],1]"), "[[6,[5,[14,0]]],3]")
    asserthelper.printAssert(explode("[[6,[5,[30,[11,2]]]],1]"), "[[6,[5,[41,0]]],3]")
    asserthelper.printAssert(explode("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"), "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")
    asserthelper.printAssert(explode("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"), "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")
    asserthelper.printAssert(explode("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]"), "[[[[0,7],4],[15,[0,13]]],[1,1]]")
    asserthelper.printAssert(explode("[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]"), "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")

    print("split tests:")
    asserthelper.printAssert(split("[[[[0,7],4],[15,[0,13]]],[1,1]]"), "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]")
    asserthelper.printAssert(split("[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"), "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]")
    asserthelper.printAssert(split("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"), "NaS")

    print("add tests:")
    asserthelper.printAssert(add("[1,2]", "[[3,4],5]"), "[[1,2],[[3,4],5]]")
    asserthelper.printAssert(add("", "[[3,4],5]"), "[[3,4],5]")

    print("magntide tests:")
    asserthelper.printAssert(magnitude("[[1,2],[[3,4],5]]"), "143")
    asserthelper.printAssert(magnitude("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"), "1384")
    asserthelper.printAssert(magnitude("[[[[1,1],[2,2]],[3,3]],[4,4]]"), "445")
    asserthelper.printAssert(magnitude("[[[[3,0],[5,3]],[4,4]],[5,5]]"), "791")
    asserthelper.printAssert(magnitude("[[[[5,0],[7,4]],[5,5]],[6,6]]"), "1137")
    asserthelper.printAssert(magnitude("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"), "3488")
    asserthelper.printAssert(magnitude("[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"), "4140")

    print(timeit.timeit(lambda: print("1-0: " + asserthelper.returnAssert(str(first("180.txt")), "1384")), number=1))
    print(timeit.timeit(lambda: print("1-1: " + asserthelper.returnAssert(str(first("181.txt")), "4140")), number=1))
    print(timeit.timeit(lambda: print("1-2: " + asserthelper.returnAssert(str(first("182.txt")), "3987")), number=1))
    print(timeit.timeit(lambda: print("2-1: " + asserthelper.returnAssert(str(second("181.txt")), "3993")), number=1))
    print(timeit.timeit(lambda: print("2-2: " + asserthelper.returnAssert(str(second("182.txt")), "4500")), number=1))
