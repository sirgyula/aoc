import sys
import timeit
import re
import numpy


def parseInput(input):
    gr = re.search(r'^target area: x=(-?\d*)..(-?\d*), y=(-?\d*)..(-?\d*)$', input).groups()
    return (int(gr[0]), int(gr[2])), (int(gr[1]), int(gr[3]))


def containsAny(aaa, d):
    for aaaa in aaa:
        if aaaa in d:
            return aaa[0]

    return -1


def getSumsUntil(until):
    res = []
    sum = 0

    for i in reversed(range(until + 1)):
        sum += i
        res.append(sum)

    return res


def step(currentCoord, trajectory):
    newCoord = (currentCoord[0] + trajectory[0], currentCoord[1] + trajectory[1])

    if trajectory[0] > 0:
        newTrajectoryX = trajectory[0] - 1
    elif trajectory[0] < 0:
        newTrajectoryX = trajectory[0] + 1
    else:
        newTrajectoryX = 0

    return (newTrajectoryX, trajectory[1] - 1), newCoord


def solve(input):
    bottomleft, topright = parseInput(input)
    xcoords = list(filter(lambda coord: coord != -1, [containsAny(getSumsUntil(i), range(bottomleft[0], topright[0] + 1)) for i in reversed(range(topright[0] + 1))]))
    validTrajectories = []
    maxy = -1 * sys.maxsize
    initvelos = 0

    for x in reversed(xcoords):
        y = bottomleft[1]

        while True:
            originalTrajectory = (x, y)
            trajectory = originalTrajectory
            visitedCoords = []
            visitedCoord = (0, 0)

            while True:
                trajectory, visitedCoord = step(visitedCoord, trajectory)
                visitedCoords.append(visitedCoord)

                if topright[1] >= visitedCoord[1] >= bottomleft[1] and bottomleft[0] <= visitedCoord[0] <= topright[0]:
                    initvelos += 1
                    validTrajectories.append(originalTrajectory)
                    cmaxy = max([c[1] for c in visitedCoords])

                    if maxy < cmaxy:
                        maxy = cmaxy

                    break
                elif visitedCoord[1] < bottomleft[1] or visitedCoord[0] > topright[0]:
                    break

            y += 1

            if y > 150:
                break

    return maxy, initvelos


def first(input):
    return solve(input)[0]


def second(input):
    return solve(input)[1]


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("target area: x=20..30, y=-10..-5"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("target area: x=206..250, y=-105..-57"))), number=1))
    print(timeit.timeit(lambda: print("1-0: " + str(second("target area: x=20..30, y=-10..-5"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(second("target area: x=206..250, y=-105..-57"))), number=1))
