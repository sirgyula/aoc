import sys
import timeit
import numpy


class Packet:
    def __init__(self, version, typeId, literal=None, subpackets=None):
        self.version = version
        self.typeId = typeId
        self.literal = literal
        self.subpackets = subpackets


def hexToBin(hex):
    binary = bin(int(hex, 16))[2:]
    extend = len(binary) % 4
    return binary if extend == 0 else (4 - extend) * '0' + binary


def binToDec(bin):
    return int(bin, 2)


def parseLiteral(binary):
    num = ''
    i = 0

    while True:
        part = binary[i:i + 5]
        i += 5
        num += part[1:5]

        if part[0] == '0':
            break

    return binToDec(num), i


def sumVersions(packets):
    return sum([packet.version if packet.subpackets is None else packet.version + sumVersions(packet.subpackets) for packet in packets])


def parse(binary, maxIter=sys.maxsize) -> [Packet]:
    i = 0
    packets = []
    iter = 0

    while True:
        version = binToDec(binary[i:i+3])
        i += 3
        typeId = binToDec(binary[i:i+3])
        i += 3

        if typeId == 4:
            literal, parsed = parseLiteral(binary[i:])
            i += parsed
            packets.append(Packet(version, typeId, literal=literal))
        else:
            lengthTypeId = int(binary[i])
            i += 1

            if lengthTypeId == 0:
                totalLengthInBits = binToDec(binary[i:i+15])
                i += 15
                subtotal = 0
                subpackets = []

                while subtotal < totalLengthInBits:
                    sp, subi = parse(binary[i:i+totalLengthInBits])
                    subpackets.extend(sp)
                    subtotal += subi

                packets.append(Packet(version, typeId, subpackets=subpackets))
                i += subtotal
            else:
                numOfSubPackets = binToDec(binary[i:i+11])
                i += 11
                subpackets = []

                sp, subi = parse(binary[i:], numOfSubPackets)
                subpackets.extend(sp)
                i += subi

                packets.append(Packet(version, typeId, subpackets=subpackets))

        iter += 1

        if len(binary) == i or binary[i:] == (len(binary) - i) * '0' or iter == maxIter:
            return packets, i


def getMainPacket(hex):
    binary = hexToBin(hex)
    packets, i = parse(binary)

    return packets


def solve(packet):
    result = {
        0: lambda p: psum(p.subpackets),
        1: lambda p: pprod(p.subpackets),
        2: lambda p: pmin(p.subpackets),
        3: lambda p: pmax(p.subpackets),
        4: lambda p: p.literal,
        5: lambda p: pgt(p.subpackets),
        6: lambda p: plt(p.subpackets),
        7: lambda p: pet(p.subpackets)
    }[packet.typeId](packet)

    return result


def psum(subpackets):
    return sum([solve(p) for p in subpackets])


def pprod(subpackets):
    return numpy.prod([solve(p) for p in subpackets])


def pmin(subpackets):
    return min([solve(p) for p in subpackets])


def pmax(subpackets):
    return max([solve(p) for p in subpackets])


def pgt(subpackets):
    return 1 if solve(subpackets[0]) > solve(subpackets[1]) else 0


def plt(subpackets):
    return 1 if solve(subpackets[0]) < solve(subpackets[1]) else 0


def pet(subpackets):
    return 1 if solve(subpackets[0]) == solve(subpackets[1]) else 0


def first(hex):
    return sumVersions(getMainPacket(hex))


def second(hex):
    return solve(getMainPacket(hex)[0])


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("D2FE28"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("38006F45291200"))), number=1))
    print(timeit.timeit(lambda: print("1-2: " + str(first("EE00D40C823060"))), number=1))
    print(timeit.timeit(lambda: print("1-3: " + str(first("8A004A801A8002F478"))), number=1))
    print(timeit.timeit(lambda: print("1-4: " + str(first("620080001611562C8802118E34"))), number=1))
    print(timeit.timeit(lambda: print("1-5: " + str(first("C0015000016115A2E0802F182340"))), number=1))
    print(timeit.timeit(lambda: print("1-6: " + str(first("A0016C880162017C3686B18A3D4780"))), number=1))
    print(timeit.timeit(lambda: print("1-7: " + str(first(open("161.txt", "r").readline().rstrip()))), number=1))

    print(timeit.timeit(lambda: print("2-0: " + str(second("C200B40A82"))), number=1))
    # print(timeit.timeit(lambda: print("2-1: " + str(second("04005AC33890"))), number=1))
    print(timeit.timeit(lambda: print("2-2: " + str(second("880086C3E88112"))), number=1))
    print(timeit.timeit(lambda: print("2-3: " + str(second("CE00C43D881120"))), number=1))
    print(timeit.timeit(lambda: print("2-4: " + str(second("D8005AC2A8F0"))), number=1))
    print(timeit.timeit(lambda: print("2-5: " + str(second("F600BC2D8F"))), number=1))
    print(timeit.timeit(lambda: print("2-6: " + str(second("9C005AC2F8F0"))), number=1))
    print(timeit.timeit(lambda: print("2-7: " + str(second("9C0141080250320F1802104A08"))), number=1))
    print(timeit.timeit(lambda: print("2-8: " + str(second(open("161.txt", "r").readline().rstrip()))), number=1))
