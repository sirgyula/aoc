import timeit
import re


def getUnique(patterns, cnt):
    pattern = next(filter(lambda p: len(p) == cnt, patterns), None)
    patterns.remove(pattern)
    return pattern


def getByContainsAndCnt(patterns, contains, cnt):
    pattern = ''

    for p in patterns:
        if len([value for value in list(p) if value in contains]) == len(contains) and len(p) == cnt:
            pattern = p
            break

    patterns.remove(pattern)
    return pattern


def getByContains(patterns, contains):
    pattern = ''

    for p in patterns:
        if len([value for value in list(p) if value in contains]) == len(contains):
            pattern = p
            break

    patterns.remove(pattern)
    return pattern


def getSix(patterns, contains, cnt):
    pattern = ''

    for p in patterns:
        if (contains[0] in p or contains[1] in p) and len(p) == cnt:
            pattern = p
            break

    patterns.remove(pattern)
    return pattern


def getFive(patterns, contains):
    pattern = ''

    for p in patterns:
        if len([value for value in list(p) if value in contains]) == len(contains) - 1:
            pattern = p
            break

    patterns.remove(pattern)
    return pattern


def assignPatternsForNumbers(patterns):
    numbers = ['', '', '', '', '', '', '', '', '', '']

    numbers[1] = getUnique(patterns, 2)
    numbers[4] = getUnique(patterns, 4)
    numbers[7] = getUnique(patterns, 3)
    numbers[8] = getUnique(patterns, 7)
    numbers[3] = getByContainsAndCnt(patterns, list(numbers[1]), 5)
    numbers[9] = getByContainsAndCnt(patterns, list(numbers[3]), 6)
    numbers[0] = getByContains(patterns, list(numbers[1]))
    numbers[6] = getSix(patterns, list(numbers[1]), 6)
    numbers[5] = getFive(patterns, list(numbers[6]))
    numbers[2] = patterns[0]

    return numbers


def decodeScreen(screen, numbers):
    screenResult = ''

    for s in screen:
        screenResult += str(numbers.index(s))

    return int(screenResult)


def first(filename):
    lines = list([re.search(r'^(.+ ?) \| (.+ ?)$', line.rstrip()).groups() for line in open(filename, "r")])
    sum = 0

    for line in lines:
        sum += len(list(filter(lambda seg: len(seg) == 2 or len(seg) == 3 or len(seg) == 4 or len(seg) == 7, line[1].split(' '))))

    return sum


def second(filename):
    lines = list([re.search(r'^(.+ ?) \| (.+ ?)$', line.rstrip()).groups() for line in open(filename, "r")])
    sum = 0

    for line in lines:
        patterns = ["".join(sorted(p)) for p in line[0].split(' ')]
        screen = ["".join(sorted(p)) for p in line[1].split(' ')]
        numbers = assignPatternsForNumbers(patterns)

        sum += decodeScreen(screen, numbers)

    return sum


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("080.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("081.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("080.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-2: " + str(second("082.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("081.txt"))), number=1))
