import timeit
import re
from numpy import *


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return str(self.x) + " " + str(self.y)


def ccw(A, B, C):
    y = (C.y - A.y)
    x = (B.x - A.x)
    c = (B.y - A.y)
    v = (C.x - A.x)
    return y * x > c * v


# Return true if line segments AB and CD intersect
def hasIntersection(A, B, C, D):
    q = ccw(A, C, D)
    w = ccw(B, C, D)
    e = ccw(A, B, C)
    r = ccw(A, B, D)
    return q != w and e != r


def intersection(lst1, lst2, s):
    [s.add(value) for value in lst1 if value in lst2]


def bresenham(x0, y0, x1, y1):
    "Bresenham's line algorithm"
    points_in_line = []
    dx = abs(x1 - x0)
    dy = abs(y1 - y0)
    x, y = x0, y0
    sx = -1 if x0 > x1 else 1
    sy = -1 if y0 > y1 else 1
    if dx > dy:
        err = dx / 2.0
        while x != x1:
            points_in_line.append((x, y))
            err -= dy
            if err < 0:
                y += sy
                err += dx
            x += sx
    else:
        err = dy / 2.0
        while y != y1:
            points_in_line.append((x, y))
            err -= dx
            if err < 0:
                x += sx
                err += dy
            y += sy
    points_in_line.append((x, y))
    return points_in_line


def generatePoints(filteredLines):
    return list(bresenham(line[0], line[1], line[2], line[3]) for line in filteredLines)


# Given three collinear points p, q, r, the function checks if
# point q lies on line segment 'pr'
def onSegment(p, q, r):
    if ((q.x <= max(p.x, r.x)) and (q.x >= min(p.x, r.x)) and
            (q.y <= max(p.y, r.y)) and (q.y >= min(p.y, r.y))):
        return True
    return False


def orientation(p, q, r):
    # to find the orientation of an ordered triplet (p,q,r)
    # function returns the following values:
    # 0 : Collinear points
    # 1 : Clockwise points
    # 2 : Counterclockwise

    # See https://www.geeksforgeeks.org/orientation-3-ordered-points/amp/
    # for details of below formula.

    val = (float(q.y - p.y) * (r.x - q.x)) - (float(q.x - p.x) * (r.y - q.y))
    if (val > 0):

        # Clockwise orientation
        return 1
    elif (val < 0):

        # Counterclockwise orientation
        return 2
    else:

        # Collinear orientation
        return 0


# The main function that returns true if
# the line segment 'p1q1' and 'p2q2' intersect.
def doIntersect(p1, q1, p2, q2):
    # Find the 4 orientations required for
    # the general and special cases
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    # General case
    if ((o1 != o2) and (o3 != o4)):
        return True

    # Special Cases

    # p1 , q1 and p2 are collinear and p2 lies on segment p1q1
    if ((o1 == 0) and onSegment(p1, p2, q1)):
        return True

    # p1 , q1 and q2 are collinear and q2 lies on segment p1q1
    if ((o2 == 0) and onSegment(p1, q2, q1)):
        return True

    # p2 , q2 and p1 are collinear and p1 lies on segment p2q2
    if ((o3 == 0) and onSegment(p2, p1, q2)):
        return True

    # p2 , q2 and q1 are collinear and q1 lies on segment p2q2
    if ((o4 == 0) and onSegment(p2, q1, q2)):
        return True

    # If none of the cases
    return False


def first(filename):
    lines = list([re.search(r'^(\d+),(\d+) -> (\d+),(\d+)$', line.rstrip()).groups() for line in open(filename, "r")])
    filteredLines = list(filter(lambda line: line[0] == line[2] or line[1] == line[3], lines))
    filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in filteredLines]
    linecnt = len(filteredLines)
    s = set([])

    for i in range(0, linecnt - 1):
        p1 = Point(filteredLines[i][0], filteredLines[i][1])
        p2 = Point(filteredLines[i][2], filteredLines[i][3])

        for j in range(i + 1, linecnt):
            p3 = Point(filteredLines[j][0], filteredLines[j][1])
            p4 = Point(filteredLines[j][2], filteredLines[j][3])

            if doIntersect(p1, p2, p3, p4):
                l1 = list(bresenham(p1.x, p1.y, p2.x, p2.y))
                l2 = list(bresenham(p3.x, p3.y, p4.x, p4.y))
                intersection(l1, l2, s)

    return len(s)


def second(filename):
    lines = list([re.search(r'^(\d+),(\d+) -> (\d+),(\d+)$', line.rstrip()).groups() for line in open(filename, "r")])
    filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in lines]
    filteredLines = list(filter(lambda line: line[0] == line[2]
                                             or line[1] == line[3]
                                             or abs(line[0] - line[2]) == abs(line[1] - line[3])
                                , filteredLines))
    # filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in filteredLines]
    linecnt = len(filteredLines)
    s = set([])

    for i in range(0, linecnt - 1):
        p1 = Point(filteredLines[i][0], filteredLines[i][1])
        p2 = Point(filteredLines[i][2], filteredLines[i][3])

        for j in range(i + 1, linecnt):
            p3 = Point(filteredLines[j][0], filteredLines[j][1])
            p4 = Point(filteredLines[j][2], filteredLines[j][3])

            if doIntersect(p1, p2, p3, p4):
                l1 = list(bresenham(p1.x, p1.y, p2.x, p2.y))
                l2 = list(bresenham(p3.x, p3.y, p4.x, p4.y))
                intersection(l1, l2, s)

    return len(s)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("050.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("051.txt"))), number=1))
    # print(timeit.timeit(lambda: print("1-3: " + str(first("053.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("050.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("051.txt"))), number=1))
