import timeit
import sys
import numpy as np


def getNeighbours(center, maxrowi, maxcoli):
    neighbours = [(center[0] - 1, center[1]),
                  (center[0] + 1, center[1]),
                  (center[0], center[1] - 1),
                  (center[0], center[1] + 1)]

    return list(filter(lambda item: 0 <= item[0] <= maxrowi and 0 <= item[1] <= maxcoli, neighbours))


def first(filename):
    mappy = np.array([[int(numeric_string) for numeric_string in arr] for arr in [list(line.rstrip()) for line in open(filename, "r")]])
    maxrowi = len(mappy) - 1
    maxcoli = len(mappy[0]) - 1
    distancespy = np.array([[sys.maxsize for x in range(maxrowi + 1)] for y in range(maxcoli + 1)])
    distancespy[(0, 0)] = 0
    start = (0, 0)
    end = (maxrowi, maxcoli)
    nextCenters = [start]

    while True:
        checkNext = []

        if len(nextCenters) == 0:
            break

        for current in nextCenters:

            neighbours = getNeighbours(current, maxrowi, maxcoli)

            for neighbour in neighbours:
                if mappy[neighbour] + distancespy[current] < distancespy[neighbour]:
                    distancespy[neighbour] = mappy[neighbour] + distancespy[current]

                    if neighbour != end:
                        checkNext.append(neighbour)

        nextCenters = checkNext

    return distancespy[end]


def second(filename):
    return first(filename)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("150.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("151.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("152.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("153.txt"))), number=1))
