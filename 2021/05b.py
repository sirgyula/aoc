import timeit
import re
from numpy import *


def intersection(lst1, lst2, s):
    [s.add(value) for value in lst1 if value in lst2]


def bresenham(x0, y0, x1, y1):
    "Bresenham's line algorithm"
    points_in_line = []
    dx = abs(x1 - x0)
    dy = abs(y1 - y0)
    x, y = x0, y0
    sx = -1 if x0 > x1 else 1
    sy = -1 if y0 > y1 else 1
    if dx > dy:
        err = dx / 2.0
        while x != x1:
            points_in_line.append((x, y))
            err -= dy
            if err < 0:
                y += sy
                err += dx
            x += sx
    else:
        err = dy / 2.0
        while y != y1:
            points_in_line.append((x, y))
            err -= dx
            if err < 0:
                x += sx
                err += dy
            y += sy
    points_in_line.append((x, y))
    return points_in_line


def generatePoints(filteredLines):
    return list(bresenham(line[0], line[1], line[2], line[3]) for line in filteredLines)


def first(filename):
    lines = list([re.search(r'^(\d+),(\d+) -> (\d+),(\d+)$', line.rstrip()).groups() for line in open(filename, "r")])
    filteredLines = list(filter(lambda line: line[0] == line[2] or line[1] == line[3], lines))
    filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in filteredLines]
    linecnt = len(filteredLines)
    points = generatePoints(filteredLines)
    s = set([])

    for i in range(0, linecnt):
        for j in range(i + 1, linecnt):
            intersection(points[i], points[j], s)

    return len(s)


def second(filename):
    lines = list([re.search(r'^(\d+),(\d+) -> (\d+),(\d+)$', line.rstrip()).groups() for line in open(filename, "r")])
    filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in lines]
    filteredLines = list(filter(lambda line: line[0] == line[2]
                                             or line[1] == line[3]
                                             or abs(line[0] - line[2]) == abs(line[1] - line[3])
                                , filteredLines))
    filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in filteredLines]
    linecnt = len(filteredLines)
    points = generatePoints(filteredLines)
    s = set([])

    for i in range(0, linecnt):
        for j in range(i + 1, linecnt):
            intersection(points[i], points[j], s)

    return len(s)


if __name__ == '__main__':
    # print(timeit.timeit(lambda: print("1-0: " + str(first("050.txt"))), number=1))
    # print(timeit.timeit(lambda: print("1-1: " + str(first("051.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("050.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("051.txt"))), number=1))
