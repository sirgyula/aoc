import timeit


def age(numbers, i):
    numbers[i] = numbers[i] - 1
    return -3


def spawn(numbers, i):
    numbers.append(8)
    numbers[i] = 6
    return -2


def first(filename, targetDays):
    f = open(filename, "r")
    numbers = [int(x) for x in f.readline().rstrip().split(',')]
    elapsedDays = 0

    while elapsedDays != targetDays:
        currentFishCnt = len(numbers)

        for i in range(0, currentFishCnt):
            res = {
                0: lambda index: spawn(numbers, index),
                1: lambda index: age(numbers, index),
                2: lambda index: age(numbers, index),
                3: lambda index: age(numbers, index),
                4: lambda index: age(numbers, index),
                5: lambda index: age(numbers, index),
                6: lambda index: age(numbers, index),
                7: lambda index: age(numbers, index),
                8: lambda index: age(numbers, index)
            }[numbers[i]](i)

        elapsedDays += 1

    return len(numbers)


def second(filename, targetDays):
    return first(filename, targetDays)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("060.txt", 80))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("061.txt", 80))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("060.txt", 256))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("061.txt", 256))), number=1))
