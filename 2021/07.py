import timeit
import sys


def consumption1(a, b):
    return abs(a - b)


def consumption2(a, b):
    """Move from 16 to 5: 66 fuel
    diff is 11, sum of numbers from 1 to 11 is 66"""
    y = 1
    x = abs(a - b)

    return ((x - y + 1) * (y + x))/2


def first(filename, consumption):
    hpos = -1
    fuel = sys.maxsize

    with open(filename) as file:
        nums = [int(x) for x in file.readline().rstrip().split(',')]

    minH = min(nums)
    maxH = max(nums)

    for currenthpos in range(minH, maxH + 1):
        currentfuel = 0
        fail = False

        # sum of costs for moving from given position to currenthpos
        for i in range(0, len(nums)):
            currentfuel += consumption(nums[i], currenthpos)

            if currentfuel > fuel:
                # current iteration costs more
                fail = True
                break

        # cost calculated, lower than before
        if not fail:
            hpos = currenthpos
            fuel = currentfuel

    print(hpos)
    return fuel


def second(filename, consumption):
    return first(filename, consumption)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("070.txt", consumption1))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("071.txt", consumption1))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("070.txt", consumption2))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("071.txt", consumption2))), number=1))
