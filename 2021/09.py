import timeit
import numpy as np


def checkMiddle(map, row, col):
    checking = map[row][col]
    if checking < map[row][col - 1] \
            and checking < map[row][col + 1] \
            and checking < map[row - 1][col] \
            and checking < map[row + 1][col]:
        return checking + 1

    return 0


def checkTopRow(map, row, col):
    checking = map[row][col]
    if checking < map[row][col - 1] \
            and checking < map[row][col + 1] \
            and checking < map[row + 1][col]:
        return checking + 1

    return 0


def checkBottomRow(map, row, col):
    checking = map[row][col]
    if checking < map[row][col - 1] \
            and checking < map[row][col + 1] \
            and checking < map[row - 1][col]:
        return checking + 1

    return 0


def checkFirstCol(map, row, col):
    checking = map[row][col]
    if checking < map[row][col + 1] \
            and checking < map[row - 1][col] \
            and checking < map[row + 1][col]:
        return checking + 1

    return 0


def checkLastCol(map, row, col):
    checking = map[row][col]
    if checking < map[row][col - 1] \
            and checking < map[row - 1][col] \
            and checking < map[row + 1][col]:
        return checking + 1

    return 0


def check(map, row, col):
    pass


def first(filename):
    map = [[int(numeric_string) for numeric_string in arr] for arr in
           [list(line.rstrip()) for line in open(filename, "r")]]

    colnum = len(map[0])
    rownum = len(map)
    risklevel = 0

    # mid
    for row in range(1, rownum - 1):
        for col in range(1, colnum - 1):
            risklevel += checkMiddle(map, row, col)

    # row 0
    for col in range(1, colnum - 2):
        risklevel += checkTopRow(map, 0, col)

    # row max
    for col in range(1, colnum - 2):
        risklevel += checkBottomRow(map, rownum - 1, col)

    # col 0
    for row in range(1, rownum - 2):
        risklevel += checkFirstCol(map, row, 0)

    # col max
    for row in range(1, rownum - 2):
        risklevel += checkLastCol(map, row, colnum - 1)

    # 0,0
    if map[0][0] < map[0][1] and map[0][0] < map[1][0]:
        risklevel += map[0][0] + 1

    # 0,max
    if map[0][colnum - 1] < map[0][colnum - 2] and map[0][colnum - 1] < map[1][colnum - 1]:
        risklevel += map[0][colnum - 1] + 1

    # max,0
    if map[rownum - 1][0] < map[rownum - 2][0] and map[rownum - 1][0] < map[rownum - 1][1]:
        risklevel += map[rownum - 1][0] + 1

    # max,max
    if map[rownum - 1][colnum - 1] < map[rownum - 2][colnum - 1] and map[rownum - 1][colnum - 1] < map[rownum - 1][colnum - 2]:
        risklevel += map[rownum - 1][colnum - 1] + 1

    return risklevel


def second(filename):
    return -1


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("090.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("091.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("090.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("091.txt"))), number=1))
