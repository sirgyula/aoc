import timeit
import numpy as np
import sys


def step(mappy):
    return mappy + 1


def getNeighbours(center, neighbours=[]):
    if not center:
        yield neighbours
    else:
        yield from [(idx[0], idx[1]) for j in range(center[0] - 1, center[0] + 2) for idx in getNeighbours(center[1:], neighbours + [j])]


def stepNeighbours(mappy, center, rownum, colnum):
    neighbours = list(getNeighbours(center))
    neighbours.remove(center)
    neighbours = list(filter(lambda item: 0 <= item[0] < colnum and 0 <= item[1] < rownum, neighbours))

    newFlashers = []

    for neighbour in neighbours:
        if mappy[neighbour] != 0:
            mappy[neighbour] += 1

            if mappy[neighbour] > 9:
                newFlashers.append(neighbour)

    return mappy, newFlashers


def addUniquely(flashers, newFlashers):
    for item in newFlashers:
        if item not in flashers:
            flashers.append(item)


def first(filename, steps):
    mappy = np.array([[int(numeric_string) for numeric_string in arr] for arr in [list(line.rstrip()) for line in open(filename, "r")]])
    colnum = len(mappy[0])
    rownum = len(mappy)
    flashes = 0

    for i in range(0, steps):
        mappy = step(mappy)
        flashers = list(tuple(zip(*np.where(mappy > 9))))
        j = 0

        if len(flashers) == 0:
            continue

        while j != len(flashers):
            currentCoordinate = flashers[j]
            mappy[currentCoordinate] = 0
            mappy, newFlashers = stepNeighbours(mappy, currentCoordinate, rownum, colnum)
            addUniquely(flashers, newFlashers)
            j += 1

        flashes += len(flashers)

    return flashes


def second(filename, steps):
    mappy = np.array([[int(numeric_string) for numeric_string in arr] for arr in [list(line.rstrip()) for line in open(filename, "r")]])
    colnum = len(mappy[0])
    rownum = len(mappy)

    for i in range(0, steps):
        mappy = step(mappy)
        flashers = list(tuple(zip(*np.where(mappy > 9))))
        j = 0

        if len(flashers) == 0:
            continue

        while j != len(flashers):
            currentCoordinate = flashers[j]
            mappy[currentCoordinate] = 0
            mappy, newFlashers = stepNeighbours(mappy, currentCoordinate, rownum, colnum)
            addUniquely(flashers, newFlashers)
            j += 1

        if len(list(tuple(zip(*np.where(mappy == 0))))) == colnum * rownum:
            return i + 1

    return -1


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("110.txt", 100))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("111.txt", 100))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("110.txt", sys.maxsize))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("111.txt", sys.maxsize))), number=1))
