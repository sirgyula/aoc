import timeit
import re
from collections import Counter


def parse(filename):
    with open(filename) as file:
        template = file.readline().rstrip()
        file.readline() # empty

        insertions = {}

        while line := file.readline().rstrip():
            gr = re.search(r'^(\w{2}) -> (\w)$', line.rstrip(), ).groups()
            insertions[gr[0]] = gr[1]

    return template, insertions


def increaseCnt(cnts, letter, cnt):
    try:
        cnts[letter] += cnt
    except KeyError:
        cnts[letter] = cnt


def splitIt(template):
    res = []

    for i in range(len(template) - 1):
        res.append(template[i:i + 2])

    return res


def first(filename, steps):
    template, insertions = parse(filename)

    lettercnts = Counter(template)
    paircnts = Counter(splitIt(template))

    for i in range(steps):
        newpaircnts = {}

        for item in paircnts.items():
            ins = insertions[item[0]]
            increaseCnt(lettercnts, ins, item[1])
            increaseCnt(newpaircnts, item[0][0] + ins, item[1])
            increaseCnt(newpaircnts, ins + item[0][1], item[1])

        paircnts = newpaircnts

    return max(lettercnts.values()) - min(lettercnts.values())


def second(filename, steps):
    return first(filename, steps)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("140.txt", 10))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("141.txt", 10))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("140.txt", 40))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("141.txt", 40))), number=1))
