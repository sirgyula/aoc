import timeit


def checkMiddle(map, row, col, checking):
    if checking < map[row][col - 1] \
            and checking < map[row][col + 1] \
            and checking < map[row - 1][col] \
            and checking < map[row + 1][col]:
        return True

    return False


def checkTopRow(map, row, col, checking):
    if checking < map[row][col - 1] \
            and checking < map[row][col + 1] \
            and checking < map[row + 1][col]:
        return True

    return False


def checkBottomRow(map, row, col, checking):
    if checking < map[row][col - 1] \
            and checking < map[row][col + 1] \
            and checking < map[row - 1][col]:
        return True

    return False


def checkFirstCol(map, row, col, checking):
    if checking < map[row][col + 1] \
            and checking < map[row - 1][col] \
            and checking < map[row + 1][col]:
        return True

    return False


def checkLastCol(map, row, col, checking):
    if checking < map[row][col - 1] \
            and checking < map[row - 1][col] \
            and checking < map[row + 1][col]:
        return True

    return False


def checkTopLeft(map):
    if map[0][0] < map[0][1] and map[0][0] < map[1][0]:
        return True

    return False


def checkTopRight(map, maxColIndex):
    if map[0][maxColIndex] < map[0][maxColIndex - 1] and map[0][maxColIndex] < map[1][maxColIndex]:
        return True

    return False


def checkBottomLeft(map, maxRowIndex):
    if map[maxRowIndex][0] < map[maxRowIndex - 1][0] and map[maxRowIndex][0] < map[maxRowIndex][1]:
        return True

    return False


def checkBottomRight(map, maxRowIndex, maxColIndex):
    if map[maxRowIndex][maxColIndex] < map[maxRowIndex - 1][maxColIndex] and map[maxRowIndex][maxColIndex] < map[maxRowIndex][maxColIndex - 1]:
        return True

    return False


def check(map, row, col, maxRowIndex, maxColIndex):
    checking = map[row][col]

    if row == 0 and col == 0:
        if checkTopLeft(map):
            return True
    elif row == 0 and col != maxColIndex:
        if checkTopRow(map, 0, col, checking):
            return True
    elif row == 0 and col == maxColIndex:
        if checkTopRight(map, maxColIndex):
            return True
    elif row != maxRowIndex and col == 0:
        if checkFirstCol(map, row, 0, checking):
            return True
    elif row != maxRowIndex and col != maxColIndex:
        if checkMiddle(map, row, col, checking):
            return True
    elif row != maxRowIndex and col == maxColIndex:
        if checkLastCol(map, row, maxColIndex, checking):
            return True
    elif row == maxRowIndex and col == 0:
        if checkBottomLeft(map, maxRowIndex):
            return True
    elif row == maxRowIndex and col != maxColIndex:
        if checkBottomRow(map, maxRowIndex, col, checking):
            return True
    elif row == maxRowIndex and col == maxColIndex:
        if checkBottomRight(map, maxRowIndex, maxColIndex):
            return True

    return False


def first(filename):
    map = [[int(numeric_string) for numeric_string in arr] for arr in
           [list(line.rstrip()) for line in open(filename, "r")]]

    colnum = len(map[0])
    rownum = len(map)
    risklevel = 0

    for row in range(0, rownum):
        for col in range(0, colnum):
            if check(map, row, col, rownum - 1, colnum - 1):
                risklevel += map[row][col] + 1

    return risklevel


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("090.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("091.txt"))), number=1))
