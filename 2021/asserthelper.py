def printAssert(l, r):
    try:
        assert l == r
        print("\t t " + l + " = " + r)
    except AssertionError:
        print("\t F " + l + " =/= " + r)


def returnAssert(l, r):
    try:
        assert l == r
        return "\t t " + l + " = " + r
    except AssertionError:
        return "\t F " + l + " =/= " + r
