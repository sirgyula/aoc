import timeit
import sys


def cleanup(line):
    while True:
        if "<>" in line or "[]" in line or "{}" in line or "()" in line:
            line = line.replace("<>", "")
            line = line.replace("[]", "")
            line = line.replace("{}", "")
            line = line.replace("()", "")
        else:
            return line


def errorhandledIndex(string, substring):
    try:
        return string.index(substring)
    except ValueError:
        return sys.maxsize


def firstErrorChar(cleanedUpLine, closingTags):
    try:
        return cleanedUpLine[min(list([errorhandledIndex(cleanedUpLine, t) for t in closingTags]))]
    except IndexError:
        # incomplete
        return "-"


def first(filename):
    lines = [line.rstrip() for line in open(filename, "r")]
    closigTags = ["]", "}", ">", ")"]
    score = 0

    for line in lines:
        cleanedUpLine = cleanup(line)
        errorChar = firstErrorChar(cleanedUpLine, closigTags)

        score += {
            ')': lambda x: 3,
            ']': lambda x: 57,
            '}': lambda x: 1197,
            '>': lambda x: 25137,
            '-': lambda x: 0
        }[errorChar](1)

    return score


def second(filename):
    lines = [line.rstrip() for line in open(filename, "r")]
    closigTags = ["]", "}", ">", ")"]
    scores = []

    for line in lines:
        cleanedUpLine = cleanup(line)
        errorChar = firstErrorChar(cleanedUpLine, closigTags)

        if errorChar == "-":
            score = 0
            for i in reversed(range(0, len(cleanedUpLine))):
                score = {
                    '(': lambda s: (s * 5) + 1,
                    '[': lambda s: (s * 5) + 2,
                    '{': lambda s: (s * 5) + 3,
                    '<': lambda s: (s * 5) + 4
                }[cleanedUpLine[i]](score)

            scores.append(score)

    return sorted(scores)[int(len(scores) / 2)]


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("100.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("101.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("100.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("101.txt"))), number=1))
