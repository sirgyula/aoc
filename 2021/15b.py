import timeit
import sys
import numpy as np


def getNeighbours(center, maxrowi, maxcoli):
    neighbours = [(center[0] - 1, center[1]),
                  (center[0] + 1, center[1]),
                  (center[0], center[1] - 1),
                  (center[0], center[1] + 1)]

    return list(filter(lambda item: 0 <= item[0] <= maxrowi and 0 <= item[1] <= maxcoli, neighbours))


def getNeighbourDistance(mappy, coord, maxrow, maxcol):
    x = coord[0]
    y = coord[1]

    repx = int(x / maxrow)

    if repx != 0:
        x = x % maxrow

    repy = int(y / maxcol)

    if repy != 0:
        y = y % maxcol

    res = mappy[(x, y)] + repx + repy

    if res > 9:
        return res % 9
    else:
        return res


def first(filename, enlargeFactor):
    mappy = np.array([[int(numeric_string) for numeric_string in arr] for arr in [list(line.rstrip()) for line in open(filename, "r")]])
    inputmaxrow = len(mappy)
    inputmaxcol = len(mappy[0])
    maxrowi = (len(mappy) * enlargeFactor) - 1
    maxcoli = (len(mappy[0]) * enlargeFactor) - 1
    distancespy = np.array([[sys.maxsize for x in range(maxrowi + 1)] for y in range(maxcoli + 1)])
    distancespy[(0, 0)] = 0
    start = (0, 0)
    end = (maxrowi, maxcoli)
    nextCenters = [start]

    while True:
        checkNext = []

        if len(nextCenters) == 0:
            break

        for current in nextCenters:

            neighbours = getNeighbours(current, maxrowi, maxcoli)

            for neighbour in neighbours:
                a = getNeighbourDistance(mappy, neighbour, inputmaxrow, inputmaxcol)

                if a + distancespy[current] < distancespy[neighbour]:
                    distancespy[neighbour] = a + distancespy[current]

                    if neighbour != end:
                        checkNext.append(neighbour)

        nextCenters = checkNext

    return distancespy[end]


def second(filename, enlargeFactor):
    return first(filename, enlargeFactor)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("150.txt", 1))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("151.txt", 1))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("150.txt", 5))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("151.txt", 5))), number=1))
