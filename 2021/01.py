import timeit


def first(filename):
    file = open(filename, "r")
    lines = file.readlines()

    inc = 0

    for i in range(1, len(lines)):
        if int(lines[i]) > int(lines[i - 1]):
            inc += 1

    return inc


def second(filename):
    file = open(filename, "r")
    lines = file.readlines()

    inc = 0

    for i in range(3, len(lines)):
        f = int(lines[i - 3]) + int(lines[i - 2]) + int(lines[i - 1])
        s = int(lines[i - 2]) + int(lines[i - 1]) + int(lines[i - 0])
        if f < s:
            inc += 1

    return inc


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("010.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("011.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("012.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("011.txt"))), number=1))
