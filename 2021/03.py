import timeit


def first(filename):
    f = open(filename, "r")
    lineSize = len(f.readline().rstrip())

    numLines = sum(1 for line in f) + 1
    f.close()

    nums = []

    for i in range(0, lineSize):
        nums.append(0)

    with open(filename) as file:
        while line := file.readline().rstrip():
            for i in range(0, lineSize):
                nums[i] += int(line[i])

    for i in range(0, lineSize):
        if numLines - nums[i] > numLines / 2 :
            nums[i] = 0
        else:
            nums[i] = 1

    joined = ''.join([str(num) for num in nums])

    g = int(joined, 2)
    e = int(''.join('1' if x == '0' else '0' for x in joined), 2)

    return e * g


def second(filename):
    f = open(filename, "r")
    lines = f.readlines()
    lineSize = len(lines[0].rstrip())
    f.close()

    generator = lines
    scrubber = lines

    for i in range(0, lineSize):
        currentNumLinesGen = len(generator)
        currentNumLinesScr = len(scrubber)

        genCnt = 0
        scrCnt = 0

        for j in range(0, currentNumLinesGen):
            genCnt += int(generator[j][i])

        for j in range(0, currentNumLinesScr):
            scrCnt += int(scrubber[j][i])

        if len(generator) != 1:
            if currentNumLinesGen - genCnt > currentNumLinesGen / 2:
                generator = list(filter(lambda line: line[i] == '0', generator))
            else:
                generator = list(filter(lambda line: line[i] == '1', generator))

        if len(scrubber) != 1:
            if currentNumLinesScr - scrCnt > currentNumLinesScr / 2:
                scrubber = list(filter(lambda line: line[i] == '1', scrubber))
            else:
                scrubber = list(filter(lambda line: line[i] == '0', scrubber))

    return int(generator[0], 2) * int(scrubber[0], 2)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("030.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("031.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("030.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("031.txt"))), number=1))
