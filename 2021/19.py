import timeit
import re
import math
import sys
import asserthelper


class Distance:
    def __init__(self, frm, to, dist):
        self.frm = frm
        self.to = to
        self.dist = dist

    def toString(self):
        return "[" + str(self.frm) + " " + str(self.to) + " " + str(self.dist) + "]"


class Point:
    def __init__(self, coords):
        self.x = int(coords[0])
        self.y = int(coords[1])
        self.z = int(coords[2])


class Scanner:
    def __init__(self, name):
        self.name = name
        self.points = []
        self.distances = []
        self.Xorigo = 0
        self.Yorigo = 0
        self.Zorigo = 0

    def addPoint(self, point):
        self.points.append(point)

    def addDistance(self, distance: Distance):
        self.distances.append(distance)

    def hasMatchingDistance(self, aaa: Distance) -> Distance:
        for distance in self.distances:
            if distance.dist == aaa.dist:
                return distance

        return None


def calcSingleDistance(a: Point, b: Point):
    dx = b.x - a.x
    dy = b.y - a.y
    dz = b.z - a.z

    dxx = dx ** 2
    dyy = dy ** 2
    dzz = dz ** 2

    dsum = dxx + dyy + dzz

    d = dsum ** 0.5

    return d


def calcDistancesForScanner(scanner):
    for i in range(len(scanner.points)):
        for j in range(i + 1, len(scanner.points)):
            d = Distance(i, j, calcSingleDistance(scanner.points[i], scanner.points[j]))
            scanner.addDistance(d)


def calcDistances(scanners):
    for scanner in scanners:
        calcDistancesForScanner(scanner)

    return scanners


def parseInput(filename):
    lines = [x.rstrip() for x in open(filename, "r").readlines()]
    scanners = []
    currentScanner = None

    for line in lines:
        if line.startswith("--- "):
            name = re.search(r'(\d+)', line).groups()[0]
            currentScanner = Scanner(name)
        elif len(line) == 0:
            scanners.append(currentScanner)
            currentScanner = None
        else:
            a = re.search(r'^(-?\d+),(-?\d+),(-?\d+)$', line).groups()
            p = Point(a)
            currentScanner.addPoint(p)

    return scanners


def first(filename):
    scanners = parseInput(filename)
    calcDistances(scanners)

    for i in range(len(scanners)):
        for j in range(i + 1, len(scanners)):
            m = 0

            for d in scanners[i].distances:
                res = scanners[j].hasMatchingDistance(d)
                if res is not None:
                    print("scanner" + scanners[i].name + ": " + d.toString() + " ---------- scanner" + scanners[j].name + ": " + res.toString())
                    m += 1

            print("scanner" + scanners[i].name + " has " + str(m) + " matching beacons with scanner" + scanners[j].name)
            print("-------------")

    return -1


def second(filename):
    return -1


if __name__ == '__main__':
    asserthelper.printAssert(str(calcSingleDistance(Point([1, 1, 0]), Point([2, 1, 2]))), "2.23606797749979")
    print(timeit.timeit(lambda: print("1-0: " + asserthelper.returnAssert(str(first("190.txt")), "79")), number=1))
    # print(timeit.timeit(lambda: print("1-1: " + asserthelper.returnAssert(str(first("191.txt")), "-")), number=1))
