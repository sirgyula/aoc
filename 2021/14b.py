import timeit
import re
from collections import Counter


def parse(filename):
    with open(filename) as file:
        template = file.readline().rstrip()
        file.readline() # empty

        insertions = {}

        while line := file.readline().rstrip():
            gr = re.search(r'^(\w{2}) -> (\w)$', line.rstrip(), ).groups()
            insertions[gr[0]] = gr[1]

    return [c for c in template], insertions


def splitIt(template):
    return [[e, ''] for e in template]


def reSplitIt(template):
    a = []

    for t in template:
        a.append([t[0], ''])

        if t[1] != '':
            a.append([t[1], ''])

    return a


def joinIt(template):
    tmp = [''.join(e) for e in template]
    return ''.join(tmp)


def getResult(template):
    cnts = Counter(template).most_common()
    return cnts[0][1] - cnts[len(cnts) - 1][1]


def first(filename, steps):
    template, insertions = parse(filename)

    for step in range(steps):
        hasNext = True
        idx = 0
        checked = 0
        length = len(template) - 1

        while hasNext:
            checking = template[idx] + template[idx + 1]
            inserting = insertions[checking]
            template.insert(idx + 1, inserting)
            idx += 2
            checked += 1

            if checked >= length:
                break

    return getResult(joinIt(template))


def second(filename, steps):
    return first(filename, steps)


if __name__ == '__main__':
    # print(timeit.timeit(lambda: print("1-0: " + str(first("140.txt", 3))), number=1))

    print(timeit.timeit(lambda: print("1-0: " + str(first("140.txt", 10))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("141.txt", 10))), number=1))
    # print(timeit.timeit(lambda: print("2-0: " + str(second("140.txt", 40))), number=1))
    # print(timeit.timeit(lambda: print("2-1: " + str(second("141.txt", 40))), number=1))
