import timeit


def checkIfDeepest(map, row, col, checking):
    vals = []

    try:
        if col != 0:
            vals.append(checking < map[row][col - 1])
    except IndexError:
        pass

    try:
        vals.append(checking < map[row][col + 1])
    except IndexError:
        pass

    try:
        if row != 0:
            vals.append(checking < map[row - 1][col])
    except IndexError:
        pass

    try:
        vals.append(checking < map[row + 1][col])
    except IndexError:
        pass

    return len(list(filter(lambda v: v is True, vals))) == len(vals)


def first(filename):
    map = [[int(numeric_string) for numeric_string in arr] for arr in
           [list(line.rstrip()) for line in open(filename, "r")]]

    colnum = len(map[0])
    rownum = len(map)
    risklevel = 0

    for row in range(0, rownum):
        for col in range(0, colnum):
            if checkIfDeepest(map, row, col, map[row][col]):
                risklevel += map[row][col] + 1

    return risklevel


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("090.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("091.txt"))), number=1))
