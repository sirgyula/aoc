import timeit


def first(filename):
    h = 0
    d = 0

    with open(filename) as file:
        while line := file.readline().rstrip():
            firstchar = line[0]
            lastchar = int(line[len(line) - 1])

            if firstchar == 'd':
                d += lastchar
            elif firstchar == 'u':
                d -= lastchar
            elif firstchar == 'f':
                h += lastchar
            else:
                return -1

    return h * d


def second(filename):
    h = 0
    d = 0
    a = 0

    with open(filename) as file:
        while line := file.readline().rstrip():
            firstchar = line[0]
            lastchar = int(line[len(line) - 1])

            if firstchar == 'd':
                a += lastchar
            elif firstchar == 'u':
                a -= lastchar
            elif firstchar == 'f':
                h += lastchar
                d += (a * lastchar)
            else:
                return -1

    return h * d


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("020.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("021.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("020.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("021.txt"))), number=1))
