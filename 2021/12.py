import timeit
import re
from collections import Counter


def hasMaxSmall(path, node, maxSmallVisit):
    if node.isupper():
        return False

    if len(path) > 1 and node == 'start':
        return True

    if maxSmallVisit == 1:
        return node in path

    if node not in path:
        return False

    filteredPath = list(filter(lambda p: p.islower() and p != 'start' and p != 'end', path))
    cnts = Counter(filteredPath)

    for item in cnts.items():
        if item[1] == maxSmallVisit:
            return True

    return False


def findAllPaths(graph, start, end, smallMaxVisit, path=[]):
    path = path + [start]

    if start == end:
        return [path]

    if start not in graph:
        return []

    paths = []

    for node in graph[start]:
        if hasMaxSmall(path, node, smallMaxVisit):
            continue
        else:
            newpaths = findAllPaths(graph, node, end, smallMaxVisit, path)

            for newpath in newpaths:
                paths.append(newpath)

    return paths


def getFromPoints(lines):
    points = set()
    for l in lines:
        if l[1] != 'end':
            points.add(l[0])
            points.add(l[1])

    return points


def getGraphDict(lines):
    caves = getFromPoints(lines)
    graph = {}

    for cave in caves:
        fromCave = list(filter(lambda l: l[0] == cave, lines))
        destCave = list(filter(lambda l: l[1] == cave, lines))

        graph[cave] = [i[1] for i in fromCave]
        graph[cave].extend(list(filter(lambda s: s != 'start', [i[0] for i in destCave])))

    return graph


def first(filename, smallMaxVisit):
    lines = list([re.search(r'^(\w+)-(\w+)$', line.rstrip()).groups() for line in open(filename, "r")])
    graph = getGraphDict(lines)
    paths = findAllPaths(graph, 'start', 'end', smallMaxVisit)

    return len(paths)


def second(filename, smallMaxVisit):
    return first(filename, smallMaxVisit)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-00: " + str(first("120.txt", 1))), number=1))
    print(timeit.timeit(lambda: print("1-01: " + str(first("121.txt", 1))), number=1))
    print(timeit.timeit(lambda: print("1-02: " + str(first("122.txt", 1))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("123.txt", 1))), number=1))
    print(timeit.timeit(lambda: print("2-00: " + str(second("120.txt", 2))), number=1))
    print(timeit.timeit(lambda: print("2-01: " + str(second("121.txt", 2))), number=1))
    print(timeit.timeit(lambda: print("2-02: " + str(second("122.txt", 2))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("123.txt", 2))), number=1))
