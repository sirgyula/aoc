import timeit
import re
from numpy import *


class Coordinate:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)


class Line:
    def __init__(self, start: Coordinate, end: Coordinate):
        self.start = start
        self.end = end


def createLine(arr):
    return Line(Coordinate(arr[0], arr[1]), Coordinate(arr[2], arr[3]))


def perp(x):
    y = empty_like(x)
    y[0] = -x[1]
    y[1] = x[0]

    return y


def segIntersect(x1, x2, y1, y2):
    dx = x2 - x1
    dy = y2 - y1
    dp = x1 - y1
    dxp = perp(dx)
    denom = dot(dxp, dy)
    num = dot(dxp, dp)

    return (num / denom.astype(float)) * dy + y1


def get_intersect(a1, a2, b1, b2):
    """
    Returns the point of intersection of the lines passing through a2,a1 and b2,b1.
    a1: [x, y] a point on the first line
    a2: [x, y] another point on the first line
    b1: [x, y] a point on the second line
    b2: [x, y] another point on the second line
    """
    s = vstack([a1,a2,b1,b2])        # s for stacked
    h = hstack((s, ones((4, 1)))) # h for homogeneous
    l1 = cross(h[0], h[1])           # get first line
    l2 = cross(h[2], h[3])           # get second line
    x, y, z = cross(l1, l2)          # point of intersection
    if z == 0:                          # lines are parallel
        return (float('inf'), float('inf'))
    return (x/z, y/z)


def first(filename):
    lines = list([re.search(r'^(\d+),(\d+) -> (\d+),(\d+)$', line.rstrip()).groups() for line in open(filename, "r")])
    filteredLines = list(filter(lambda line: line[0] == line[2] or line[1] == line[3], lines))
    filteredLines = [[int(numeric_string) for numeric_string in arr] for arr in filteredLines]
    linecnt = len(filteredLines)

    for i in range(0, linecnt):
        for j in range(i + 1, linecnt):
            x1 = array([filteredLines[i][0], filteredLines[i][1]])
            x2 = array([filteredLines[i][2], filteredLines[i][3]])
            y1 = array([filteredLines[j][0], filteredLines[j][1]])
            y2 = array([filteredLines[j][2], filteredLines[j][3]])
            # num = segIntersect(x1, x2, y1, y2)
            num = get_intersect(x1, x2, y1, y2)
            print(x1, x2, y1, y2)
            print(num)
            print('----')

    return -1


def second(filename):
    return -1


if __name__ == '__main__':
    print(get_intersect((0, 1), (0, 2), (1, 10), (1, 9)))  # parallel  lines
    print(get_intersect((0, 1), (0, 2), (1, 10), (2, 10))) # vertical and horizontal lines
    print(get_intersect((0, 1), (1, 2), (0, 10), (1, 9)))  # another line for fun
    print(timeit.timeit(lambda: print("1-0: " + str(first("050.txt"))), number=1))
    # print(timeit.timeit(lambda: print("1-1: " + str(first("051.txt"))), number=1))
    # print(timeit.timeit(lambda: print("2-0: " + str(second("050.txt"))), number=1))
    # print(timeit.timeit(lambda: print("2-1: " + str(second("051.txt"))), number=1))
