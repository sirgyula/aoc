import timeit
from collections import Counter


def getCounts(filename):
    f = open(filename, "r")
    cnts = Counter([int(x) for x in f.readline().rstrip().split(',')])
    cntList = []

    for i in range(0, 9):
        if cnts.get(i) is None:
            cntList.append(0)
        else:
            cntList.append(cnts.get(i))

    return cntList


def first(filename, targetDays):
    cntList = getCounts(filename)
    elapsedDays = 0

    while elapsedDays != targetDays:
        spawnerCnt = cntList.pop(0)
        cntList.append(spawnerCnt)
        cntList[6] = cntList[6] + spawnerCnt
        elapsedDays += 1

    return sum(cntList)


def second(filename, targetDays):
    return first(filename, targetDays)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("060.txt", 80))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("061.txt", 80))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("060.txt", 256))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("061.txt", 256))), number=1))
