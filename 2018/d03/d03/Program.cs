﻿using Accord.Collections;
using RTree;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d03
{
	class Program
	{
		static RTree.RTree<string> tree = null;
		static Dictionary<string, RectangleDetail> rectangles = null;
		static HashSet<Point> intersectingPoints = null;

		static void Main(string[] args)
		{
			//c_KDTree();
			//c_rtree();

			p1();
		}

		static void p1()
		{
			string[] lines = File.ReadAllLines("input.txt");
			tree = new RTree.RTree<string>();
			rectangles = new Dictionary<string, RectangleDetail>();
			intersectingPoints = new HashSet<Point>();

			foreach (var line in lines)
			{
				RectangleDetail currentRectangleDetail = new RectangleDetail(line);

				var intersectIds = tree.Intersects(currentRectangleDetail.toRectangle());

				foreach (var intersectingRectangleId in intersectIds)
				{
					RectangleDetail intersectingRectangle = rectangles[intersectingRectangleId];

					var a = new RTree.Rectangle(Math.Min(intersectingRectangle.x1, currentRectangleDetail.x1) -
												Math.Max())
												,
											   (Math.Min() -
												Math.Max());
						
						
						)

					//meg kell kapni a currentRectangleDetail és a intersectingRectangle metsző pontjait,
					//és hozzáadni egy set-hez.

					
				}

				add(currentRectangleDetail);
			}

			Console.ReadLine();
		}

		static void add(RectangleDetail rectandleDetail)
		{
			rectangles.Add(rectandleDetail.id, rectandleDetail);
			tree.Add(rectandleDetail.toRectangle(), rectandleDetail.id);
		}

		private static void c_rtree()
		{
			//https://www.nuget.org/packages/RTree/1.1.0#
			//Create a new instance:
			RTree.RTree<string> tree = new RTree.RTree<string>();

			//Create a rectangle:
			RTree.Rectangle rect1 = new RTree.Rectangle(1, 1, 3, 3);
			RTree.Rectangle rect2 = new RTree.Rectangle(2, 2, 5, 5);
			RTree.Rectangle rect3 = new RTree.Rectangle(5, 5, 9, 9);
			RTree.Rectangle rect4 = new RTree.Rectangle(6, 6, 8, 8);

			//Add a new rectangle to the RTree:
			tree.Add(rect1, "#1");
			tree.Add(rect2, "#2");
			tree.Add(rect3, "#3");
			tree.Add(rect4, "#4");
			tree.Add(new RTree.Rectangle(6, 6, 8, 8), "#5");
			tree.Add(new RTree.Rectangle(1, 6, 8, 8), "#6");
			tree.Add(new RTree.Rectangle(2, 6, 8, 8), "#7");
			tree.Add(new RTree.Rectangle(3, 6, 8, 8), "#8");
			tree.Add(new RTree.Rectangle(4, 6, 8, 8), "#9");
			tree.Add(new RTree.Rectangle(5, 6, 8, 8), "#19");
			tree.Add(new RTree.Rectangle(7, 6, 8, 8), "#29");
			tree.Add(new RTree.Rectangle(8, 6, 8, 8), "#39");

			//Check which objects are inside the rectangle:
			List<string> objects = tree.Contains(rect4);

			//Count how many items in the RTree:
			int i = tree.Count;

			//Check which objects intersect with the rectangle:
			List<string> objects2 = tree.Intersects(rect4);

			var res = tree.getBounds();

			//Create a point:
			//RTree.Point point = new RTree.Point(1, 2, 3);

			//Get a list of rectangles close to the point with maximum distance:
			//var objects3 = tree.Nearest(point, 10);

			Console.Read();
		}

		private static void c_KDTree()
		{
			// This is the same example found in Wikipedia page on
			// k-d trees: http://en.wikipedia.org/wiki/K-d_tree

			// Suppose we have the following set of points:

			double[][] points =
			{
				new double[] { 2, 3 },
				new double[] { 5, 4 },
				new double[] { 9, 6 },
				new double[] { 4, 7 },
				new double[] { 8, 1 },
				new double[] { 7, 2 },
			};

			// To create a tree from a set of points, we use
			KDTree<int> tree = KDTree.FromData<int>(points);

			// Now we can manually navigate the tree
			KDTreeNode<int> node = tree.Root.Left.Right;

			// Or traverse it automatically
			foreach (KDTreeNode<int> n in tree)
			{
				double[] location = n.Position;
				double[] location2 = n.Position;
				//Assert.AreEqual(2, location.Length);
			}

			// Given a query point, we can also query for other
			// points which are near this point within a radius

			/*double[] query = new double[] { 5, 3 };

			// Locate all nearby points within an euclidean distance of 1.5
			// (answer should be be a single point located at position (5,4))
			KDTreeNodeCollection<int> result = tree.Nearest(query, radius: 1.5);

			// We can also use alternate distance functions
			tree.Distance = Accord.Math.Distance.Manhattan;

			// And also query for a fixed number of neighbor points
			// (answer should be the points at (5,4), (7,2), (2,3))
			KDTreeNodeCollection<int> neighbors = tree.Nearest(query, neighbors: 3);*/
		}
	}
}
