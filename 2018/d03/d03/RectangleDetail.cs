﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d03
{
	class RectangleDetail
	{
		public string id { get; set; }
		public int x1 { get; set; }
		public int y1 { get; set; }
		public int width { get; set; }
		public int height { get; set; }

		public int x2
		{
			get { return x1 + width; }
		}

		public int y2
		{
			get { return y1 + height; }
		}

		public RectangleDetail(String line)
		{
			string[] split = line.Split(' ');

			id = split[0];
			
			//split[1] left out, only @

			string[] coords = split[2].Split(',');
			x1 = Convert.ToInt32(coords[0]);
			y1 = Convert.ToInt32(coords[1].TrimEnd(':'));

			string[] size = split[3].Split('x');
			width = Convert.ToInt32(size[0]);
			height = Convert.ToInt32(size[1]);
		}

		public RTree.Rectangle toRectangle()
		{
			return new RTree.Rectangle(x1, y1, x2, y2);
		}

		public string ToString()
		{
			return "" + id + ": " + x1 + "," + y1 + " - " + x2 + "," + y2;
		}
	}
}
