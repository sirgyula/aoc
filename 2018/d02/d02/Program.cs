﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d02
{
	class Program
	{
		static void Main(string[] args)
		{
			p1();
			p2();

			Console.Read();
		}

		private static void p1()
		{
			String[] lines = File.ReadAllLines("input.txt");
			int twos = 0;
			int threes = 0;

			foreach (String line in lines)
			{
				ConcurrentDictionary<char, int> occ = new ConcurrentDictionary<char, int>();

				foreach (char c in line)
				{
					occ.AddOrUpdate(c, 1, (cc, count) => count + 1);
				}

				if(occ.Values.Contains(2))
				{
					twos++;
				}

				if (occ.Values.Contains(3))
				{
					threes++;
				}
			}

			Console.WriteLine(twos + "*" + threes + "=" + twos * threes);
		}

		private static void p2()
		{
			String[] lines = File.ReadAllLines("input.txt");

			for (int i = 0; i < lines.Length - 1; i++)
			{
				for (int j = i + 1; j < lines.Length - 1; j++)
				{
					if(GetHammingDistance(lines[i], lines[j]) == 1)
					{
						String res = "";

						for (int k = 0; k < lines[i].Length; k++)
						{
							if(lines[i][k] == lines[j][k])
							{
								res += lines[i][k];
							}
						}

						Console.WriteLine(res);
					}
				}
			}
		}

		public static int GetHammingDistance(string s, string t)
		{
			if (s.Length != t.Length)
			{
				throw new Exception("Strings must be equal length");
			}

			int distance =
				s.ToCharArray()
				.Zip(t.ToCharArray(), (c1, c2) => new { c1, c2 })
				.Count(m => m.c1 != m.c2);

			return distance;
		}
	}
}
