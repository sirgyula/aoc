﻿using NCalc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d01
{
	class Program
	{
		static void Main(string[] args)
		{
			p1();
			p2();

			Console.Read();
		}

		static void p1()
		{
			String exp = "0" + File.ReadAllText("input.txt").Replace("\n", "");
			Expression e = new Expression(exp);
			Console.WriteLine(e.Evaluate());
		}

		static void p2()
		{
			bool done = false;
			var expItems = File.ReadAllLines("input.txt");
			Expression e = null;
			String lastVal = "0";
			List<long> results = new List<long>();

			while (!done)
			{
				foreach (String item in expItems)
				{
					String current = "";
					e = new Expression(lastVal + item);
					current = e.Evaluate().ToString();

					if (results.Contains(Convert.ToInt64(current)))
					{
						Console.WriteLine(current);
						done = true;
						break;
					}
					else
					{
						lastVal = String.Copy(current);
						results.Add(Convert.ToInt64(current));
					}
				}
			}
		}
	}
}
