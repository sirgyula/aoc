package d03

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type coord struct {
	x int
	y int
}

var schematic [][]rune
var maxX int = 0
var maxY int = 0

func parse(filepath string) {
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		schematic = append(schematic, []rune(scanner.Text()))
	}

	maxX = len(schematic[0]) - 1
	maxY = len(schematic) - 1
}

func isSymbol(r rune) bool {
	return !isNumber(r) && r != 46
}

func isGear(r rune) bool {
	return r == 42
}

func isNumber(r rune) bool {
	return r >= 48 && r <= 57
}

func checkNeighbours(c coord, loc rune) bool {
	// loc = e rajta
	if loc == 'e' && c.x != maxX {
		if isSymbol(schematic[c.y][c.x]) {
			return true
		}
	}

	// felette
	if c.y != 0 {
		if isSymbol(schematic[c.y-1][c.x]) {
			return true
		}
	}

	// alatta
	if c.y != maxY {
		if isSymbol(schematic[c.y+1][c.x]) {
			return true
		}
	}

	// loc = s előtte
	if loc == 's' && c.x != 0 {
		if isSymbol(schematic[c.y][c.x-1]) {
			return true
		}

		if c.y != 0 {
			if isSymbol(schematic[c.y-1][c.x-1]) {
				return true
			}
		}

		if c.y != maxY {
			if isSymbol(schematic[c.y+1][c.x-1]) {
				return true
			}
		}
	}

	return false
}

func checkNeighboursForGear(c coord, loc rune) (bool, coord) {
	// loc = e rajta
	if loc == 'e' && c.x != maxX {
		if isGear(schematic[c.y][c.x]) {
			return true, c
		}
	}

	// felette
	if c.y != 0 {
		if isGear(schematic[c.y-1][c.x]) {
			return true, coord{c.x, c.y - 1}
		}
	}

	// alatta
	if c.y != maxY {
		if isGear(schematic[c.y+1][c.x]) {
			return true, coord{c.x, c.y + 1}
		}
	}

	// loc = s előtte
	if loc == 's' && c.x != 0 {
		if isGear(schematic[c.y][c.x-1]) {
			return true, coord{c.x - 1, c.y}
		}

		if c.y != 0 {
			if isGear(schematic[c.y-1][c.x-1]) {
				return true, coord{c.x - 1, c.y - 1}
			}
		}

		if c.y != maxY {
			if isGear(schematic[c.y+1][c.x-1]) {
				return true, coord{c.x - 1, c.y + 1}
			}
		}
	}

	return false, coord{}
}

func first(filepath string) int {
	var res int = 0
	schematic = make([][]rune, 0)

	parse(filepath)
	started := false
	isPart := false
	var loc rune
	num := ""

	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			r := schematic[y][x]

			if isNumber(r) {
				num += string(r)
				loc = 'm'

				if !started {
					started = true
					loc = 's'
				}

				if !isPart {
					isPart = checkNeighbours(coord{x, y}, loc)
				}
			} else if started {
				started = false
				loc = 'e'

				if !isPart {
					isPart = checkNeighbours(coord{x, y}, loc)
				}

				if isPart {
					n, _ := strconv.Atoi(num)
					res += n
					isPart = false
				}

				num = ""
			}
		}
	}

	return res
}

func second(filepath string) int {
	var res int = 0
	schematic = make([][]rune, 0)

	parse(filepath)
	started := false
	isGear := false
	gearCoord := coord{}
	gearCoords := make(map[coord][]int)
	var loc rune
	num := ""

	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			r := schematic[y][x]

			if isNumber(r) {
				num += string(r)
				loc = 'm'

				if !started {
					started = true
					loc = 's'
				}

				if !isGear {
					isGear, gearCoord = checkNeighboursForGear(coord{x, y}, loc)
				}
			} else if started {
				started = false
				loc = 'e'

				if !isGear {
					isGear, gearCoord = checkNeighboursForGear(coord{x, y}, loc)
				}

				if isGear {
					isGear = false
					n, _ := strconv.Atoi(num)

					if _, ok := gearCoords[gearCoord]; ok {
						gearCoords[gearCoord] = append(gearCoords[gearCoord], n)
					} else {
						arr := make([]int, 0)
						gearCoords[gearCoord] = append(arr, n)
					}
				}

				num = ""
			}
		}
	}

	for _, nums := range gearCoords {
		if len(nums) != 2 {
			continue
		}

		res += (nums[0] * nums[1])
	}

	return res
}

func Run() {
	fmt.Println(first("d03/demo.txt"))
	fmt.Println(first("d03/input.txt"))
	fmt.Println(second("d03/demo.txt"))
	fmt.Println(second("d03/input.txt"))
}
