package d04

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var re = regexp.MustCompile(`(\d+)`)

func parseLine(line string) (map[string]interface{}, []string, int) {
	colon := strings.Index(line, ":")
	pipe := strings.Index(line, "|")
	card := line[:colon]
	cardId, _ := strconv.Atoi(card[strings.LastIndex(card, " ")+1:])

	winning := line[colon+2 : pipe-1]
	myNumbers := line[pipe+2:]

	matchWinning := re.FindAllString(winning, -1)
	matchMyNumbers := re.FindAllString(myNumbers, -1)

	winningMap := make(map[string]interface{}, len(matchWinning))

	for i := 0; i < len(matchWinning); i++ {
		winningMap[matchWinning[i]] = nil
	}

	return winningMap, matchMyNumbers, cardId
}

func first(filepath string) int {
	var res int = 0

	readFile, _ := os.Open(filepath)
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		cardPoint := 0
		winningMap, myNumbers, _ := parseLine(fileScanner.Text())

		for i := 0; i < len(myNumbers); i++ {
			if _, ok := winningMap[myNumbers[i]]; ok {
				if cardPoint == 0 {
					cardPoint = 1
				} else {
					// bitshifting for increasing points to power of 2
					cardPoint = cardPoint << 1
				}
			}
		}

		res += cardPoint
	}

	return res
}

func second(filepath string) int {
	scratches := make(map[int]int)
	var winningMap map[string]interface{}
	var myNumbers []string
	cardId := -1

	readFile, _ := os.Open(filepath)
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		winCnt := 0
		winningMap, myNumbers, cardId = parseLine(fileScanner.Text())

		if _, ok := scratches[cardId]; ok {
			scratches[cardId] += 1
		} else {
			scratches[cardId] = 1
		}

		for i := 0; i < len(myNumbers); i++ {
			if _, ok := winningMap[myNumbers[i]]; ok {
				winCnt += 1
				scratches[cardId+winCnt] += (1 * scratches[cardId])
			}
		}
	}

	res := 0
	for key, cnt := range scratches {
		// removing overshoot cards
		if key > cardId {
			continue
		}

		res += cnt
	}

	return res
}

func Run() {
	fmt.Println(first("d04/demo.txt"))
	fmt.Println(first("d04/input.txt"))
	fmt.Println(second("d04/demo.txt"))
	fmt.Println(second("d04/input.txt"))
}
