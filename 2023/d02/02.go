package d02

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var (
	reBlue  = regexp.MustCompile(`(\d+) blue`)
	reGreen = regexp.MustCompile(`(\d+) green`)
	reRed   = regexp.MustCompile(`(\d+) red`)
	r       = 12
	g       = 13
	b       = 14
)

func isValidDraw(draw string, max int, r *regexp.Regexp) bool {
	match := r.FindStringSubmatch(draw)
	if len(match) != 0 {
		cnt, _ := strconv.Atoi(match[1])
		if cnt > max {
			return false
		}
	}

	return true
}

func first(filepath string) int {
	var res int = 0
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()[5:]

		colonindex := strings.IndexRune(line, ':')
		game := line[:colonindex]

		line = line[colonindex+2:]

		isValidGame := true
		done := false
		draw := ""

		for {
			semi := strings.IndexRune(line, ';')
			if semi == -1 {
				done = true
				draw = line
			} else {
				draw = line[:semi]
				line = line[semi+2:]
			}

			if !(isValidDraw(draw, r, reRed) && isValidDraw(draw, g, reGreen) && isValidDraw(draw, b, reBlue)) {
				isValidGame = false
				break
			}

			if done {
				break
			}
		}

		if isValidGame {
			num, _ := strconv.Atoi(game)
			res += num
		}
	}

	return res

}

func getMax(line string, r *regexp.Regexp) int {
	match := r.FindAllStringSubmatch(line, -1)
	var max int = 0

	if len(match) > 0 {
		for i := 0; i < len(match); i++ {
			tmp := match[i][1]
			num, _ := strconv.Atoi(tmp)
			if num > max {
				max = num
			}
		}
	}

	return max
}

func second(filepath string) int {
	var res int = 0
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		red := getMax(line, reRed)
		green := getMax(line, reGreen)
		blue := getMax(line, reBlue)

		res += (red * green * blue)
	}

	return res
}

func Run() {
	fmt.Println(first("d02/demo.txt"))
	fmt.Println(first("d02/input.txt"))
	fmt.Println(second("d02/demo.txt"))
	fmt.Println(second("d02/input.txt"))
}
