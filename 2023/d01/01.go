package d01

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func first(filepath string) int {
	var res int = 0
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		re := regexp.MustCompile(`(\d{1})(.*)(\d{1})(\D*)`)
		match := re.FindStringSubmatch(scanner.Text())
		if len(match) == 5 {
			tmp := match[1] + match[3]
			num, _ := strconv.Atoi(tmp)
			res += num
		} else if len(match) == 0 {
			re2 := regexp.MustCompile(`(\d{1})`)
			match2 := re2.FindStringSubmatch(scanner.Text())
			tmp := match2[1] + match2[1]
			num, _ := strconv.Atoi(tmp)
			res += num
		}
	}

	return res
}

func convert(i string) string {
	if len(i) == 1 {
		return i
	} else {
		switch i {
		case "one":
			return "1"
		case "two":
			return "2"
		case "three":
			return "3"
		case "four":
			return "4"
		case "five":
			return "5"
		case "six":
			return "6"
		case "seven":
			return "7"
		case "eight":
			return "8"
		case "nine":
			return "9"
		}
	}

	return ""
}

func second(filepath string) int {
	var res int = 0
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		re := regexp.MustCompile(`(\d{1}|one|two|three|four|five|six|seven|eight|nine)(.*)(\d{1}|one|two|three|four|five|six|seven|eight|nine)(\D*)`)
		match := re.FindStringSubmatch(scanner.Text())
		if len(match) == 5 {
			tmp := convert(match[1]) + convert(match[3])
			num, _ := strconv.Atoi(tmp)
			res += num
		} else if len(match) == 0 {
			re2 := regexp.MustCompile(`(\d{1})`)
			match2 := re2.FindStringSubmatch(scanner.Text())
			tmp := match2[1] + match2[1]
			num, _ := strconv.Atoi(tmp)
			res += num
		}
	}

	return res
}

func Run() {
	fmt.Println(first("d01/demo01.txt"))
	fmt.Println(first("d01/input.txt"))
	fmt.Println(second("d01/demo02.txt"))
	fmt.Println(second("d01/input.txt"))
}
