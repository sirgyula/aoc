package d05

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

var re = regexp.MustCompile(`(\d+)`)

type converter struct {
	srcStart  int
	destStart int
	length    int
}

type seed struct {
	start  int
	length int
}

func between(start int, end int, n int) bool {
	return start <= n && n <= end
}

func parseNumbersLine(line string) []int {
	numStrings := re.FindAllString(line, -1)

	var nums []int
	for _, v := range numStrings {
		n, _ := strconv.Atoi(v)
		nums = append(nums, n)
	}

	return nums
}

func parseSeeds(line string, part1 bool) ([]int, []seed) {
	var nums []int
	var seeds []seed
	numStrings := re.FindAllString(line, -1)

	if part1 {
		for _, v := range numStrings {
			n, _ := strconv.Atoi(v)
			nums = append(nums, n)
		}
	} else {
		for i := 0; i < len(numStrings); i += 2 {
			start, _ := strconv.Atoi(numStrings[i])
			length, _ := strconv.Atoi(numStrings[i+1])
			s := seed{start: start, length: length}
			seeds = append(seeds, s)
		}
	}

	return nums, seeds
}

func parseInput(fileScanner *bufio.Scanner, part1 bool) ([]int, []seed, map[int8][]converter) {
	fileScanner.Scan()
	seeds, seedIntervals := parseSeeds(fileScanner.Text(), part1)
	// fmt.Println(seeds)

	almanac := make(map[int8][]converter, 0)
	var almanacPart int8 = 0
	header := true

	for fileScanner.Scan() {
		line := fileScanner.Text()

		if len(line) == 0 {
			header = true
			continue
		} else if header {
			almanacPart += 1
			almanac[almanacPart] = make([]converter, 0)

			header = false

			continue
		} else {
			conversion := parseNumbersLine(line)

			conv := converter{
				srcStart:  conversion[1],
				destStart: conversion[0],
				length:    conversion[2],
			}

			// fmt.Println(conv)

			almanac[almanacPart] = append(almanac[almanacPart], conv)
		}

		// fmt.Println("--------")
	}

	return seeds, seedIntervals, almanac
}

func first(filepath string) int {
	var res int = int(^uint(0) >> 1)

	readFile, _ := os.Open(filepath)
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	seeds, _, almanac := parseInput(fileScanner, true)

	for s := 0; s < len(seeds); s++ {
		checkingNumber := seeds[s]
		// fmt.Println("seed ", checkingNumber)

		for almanacId := 1; almanacId <= len(almanac); almanacId++ {
			currentConverters := almanac[int8(almanacId)]
			// fmt.Println("almanadId ", almanacId)

			for c := 0; c < len(currentConverters); c++ {
				conv := currentConverters[c]
				// fmt.Println(conv)

				if between(conv.srcStart, conv.srcStart+conv.length-1, checkingNumber) {
					diff := checkingNumber - conv.srcStart
					checkingNumber = conv.destStart + diff
					// fmt.Printf("match %v, next: %d, diff: %d\n", conv, checkingNumber, diff)
					break
				} else {
					// fmt.Println("no match ", conv.srcStart, conv.srcEnd, checkingNumber)
					continue
				}
			}

			// fmt.Printf("map %d: %d\n", almanacId, checkingNumber)
		}

		if checkingNumber < res {
			res = checkingNumber
		}

		// fmt.Println("---------")
	}

	return res
}

// func second(filepath string) int {
// 	var res int = int(^uint(0) >> 1)

// 	readFile, _ := os.Open(filepath)
// 	defer readFile.Close()

// 	fileScanner := bufio.NewScanner(readFile)
// 	fileScanner.Split(bufio.ScanLines)

// 	_, seeds, almanac := parseInput(fileScanner, false)

// 	for _, s := range seeds {
// 		start := s.start
// 		length := s.length

// 		fmt.Println(start)
// 		fmt.Println(length)
// 		var nextIntervals []seed

// 		for almanacId := 1; almanacId <= len(almanac); almanacId++ {
// 			a := almanac[int8(almanacId)]
// 			// fmt.Println(a)

// 			m := 0
// 			for {
// 				found := false

// 				if between(a[m].srcStart, a[m].srcStart+a[m].length-1, start) {
// 					fmt.Println("in")

// 					// add nextIntervals
// 					diff := start - a[m].srcStart
// 					nextIntervalStart := a[m].destStart + diff
// 					fmt.Println(nextIntervalStart)
// 					currentIntervalStop := a[m].srcStart + a[m].length - 1
// 					fmt.Println(currentIntervalStop)
// 					nextIntervalLength := currentIntervalStop - start + 1
// 					fmt.Println(nextIntervalLength)

// 					nextIntervals = append(nextIntervals, seed{start: nextIntervalStart, length: nextIntervalLength})

// 					// adjust start, length
// 					start += nextIntervalLength
// 					length -= nextIntervalLength

// 					// reset mapid in almanac
// 					found = true
// 					fmt.Println("in stop")
// 				} else {
// 					fmt.Println("out")
// 				}

// 				if found {
// 					m = 0
// 				} else {
// 					m++

// 					// handle conversion maps finished
// 					if m == len(a) {
// 						break
// 					}
// 				}
// 			}
// 		}

// 		fmt.Println("---------")
// 	}

// 	return res
// }

func second(filepath string) int {
	var res int = int(^uint(0) >> 1)

	readFile, _ := os.Open(filepath)
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	_, seeds, almanac := parseInput(fileScanner, false)

	for _, s := range seeds {
		start := s.start
		length := s.length

		fmt.Println(start)
		fmt.Println(length)
		var nextIntervals []seed
		var currentIntervals []seed
		currentIntervals = append(nextIntervals, seed{start: start, length: length})

		for _, ci := range currentIntervals {
			for almanacId := 1; almanacId <= len(almanac); almanacId++ {

			}
		}

	}

	return res
}

func Run() {
	// fmt.Println(first("d05/demo.txt"))
	// fmt.Println(first("d05/input.txt"))
	fmt.Println(second("d05/demo.txt"))
	// fmt.Println(second("d05/input.txt"))
}
