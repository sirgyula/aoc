package main

import (
	"aoc2023/d01"
	"aoc2023/d02"
	"aoc2023/d03"
	"aoc2023/d04"
	"aoc2023/d05"
)

func main() {

	switch 5 {
	case 1:
		d01.Run()
	case 2:
		d02.Run()
	case 3:
		d03.Run()
	case 4:
		d04.Run()
	case 5:
		d05.Run()
	}
}
