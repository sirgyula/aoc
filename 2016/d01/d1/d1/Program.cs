﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d1
{
	class Program
	{
		static void Main(string[] args)
		{
			List<string> directions = new List<string>() { "R4", "R5", "L5", "L5", "L3", "R2", "R1", "R1", "L5", "R5", "R2", "L1", "L3", "L4", "R3", "L1", "L1", "R2", "R3",
				"R3", "R1", "L3", "L5", "R3", "R1", "L1", "R1", "R2", "L1", "L4", "L5", "R4", "R2", "L192", "R5", "L2", "R53", "R1", "L5", "R73", "R5", "L5", "R186", "L3",
				"L2", "R1", "R3", "L3", "L3", "R1", "L4", "L2", "R3", "L5", "R4", "R3", "R1", "L1", "R5", "R2", "R1", "R1", "R1", "R3", "R2", "L1", "R5", "R1", "L5", "R2",
				"L2", "L4", "R3", "L1", "R4", "L5", "R4", "R3", "L5", "L3", "R4", "R2", "L5", "L5", "R2", "R3", "R5", "R4", "R2", "R1", "L1", "L5", "L2", "L3", "L4", "L5",
				"L4", "L5", "L1", "R3", "R4", "R5", "R3", "L5", "L4", "L3", "L1", "L4", "R2", "R5", "R5", "R4", "L2", "L4", "R3", "R1", "L2", "R5", "L5", "R1", "R1", "L1",
				"L5", "L5", "L2", "L1", "R5", "R2", "L4", "L1", "R4", "R3", "L3", "R1", "R5", "L1", "L4", "R2", "L3", "R5", "R3", "R1", "L3" };

			//part1
			int x = 0;
			int y = 0;
			char facing = 'N';

			//part2
			List<Tuple<int, int>> visitedCoords = new List<Tuple<int, int>>();
			Tuple<int, int> winnerTuple = null;

			foreach (string direction in directions)
			{
				char turn = direction[0];
				int move = Int32.Parse(direction.Substring(1));

				if (facing == 'N' && turn == 'R')
				{
					facing = 'E';

					for (int i = 0; i < move; i++)
					{
						x++;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'N' && turn == 'L')
				{
					facing = 'W';

					for (int i = 0; i < move; i++)
					{
						x--;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'E' && turn == 'R')
				{
					facing = 'S';

					for (int i = 0; i < move; i++)
					{
						y--;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'E' && turn == 'L')
				{
					facing = 'N';

					for (int i = 0; i < move; i++)
					{
						y++;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'S' && turn == 'R')
				{
					facing = 'W';

					for (int i = 0; i < move; i++)
					{
						x--;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'S' && turn == 'L')
				{
					facing = 'E';

					for (int i = 0; i < move; i++)
					{
						x++;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'W' && turn == 'R')
				{
					facing = 'N';

					for (int i = 0; i < move; i++)
					{
						y++;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
				else if (facing == 'W' && turn == 'L')
				{
					facing = 'S';

					for (int i = 0; i < move; i++)
					{
						y--;
						Tuple<int, int> t = new Tuple<int, int>(x, y);

						if (winnerTuple == null)
						{
							if (visitedCoords.Contains(t))
							{
								winnerTuple = t;
							}
							else
							{
								visitedCoords.Add(t);
							}
						}
					}
				}
			}

			Console.WriteLine("x: " + x.ToString());
			Console.WriteLine("y: " + y.ToString());
			Console.WriteLine("facing: " + facing);
			Console.WriteLine("distance: " + (Math.Abs(x) + Math.Abs(y)));
			Console.WriteLine("distance2: " + (Math.Abs(winnerTuple.Item1) + Math.Abs(winnerTuple.Item2)));
			Console.WriteLine(visitedCoords.Count);

			Console.Read();
		}
	}
}
