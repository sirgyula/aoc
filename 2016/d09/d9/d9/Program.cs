﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d9
{
	class Program
	{
		public static string inputText { get; set; }
		public static string outputText { get; set; }
		public static long part2Count { get; set; }

		static void Main(string[] args)
		{
			inputText = System.IO.File.ReadAllText("input.txt");
			part2Count = 0;

			Console.WriteLine("compressed length: " + inputText.Length.ToString());


			//part1();
			outputText = "";
			Console.WriteLine(part2(inputText));

			Console.WriteLine("decompressed length: " + outputText.Length.ToString());
			Console.Read();
		}

		private static void part1()
		{
			outputText = "";

			do
			{
				int firstParenthesisOpen = inputText.IndexOf('(');
				int firstParenthesisClose = inputText.IndexOf(')');
				int multipleLength = 0;
				int multipleBy = 0;


				outputText += inputText.Substring(0, firstParenthesisOpen);


				//Console.WriteLine("full text: " + inputText.Substring(0, 50));
				parseMarker(inputText.Substring(firstParenthesisOpen + 1, firstParenthesisClose - 1), out multipleLength, out multipleBy);
				//Console.WriteLine("multipleLength: " + multipleLength.ToString());
				//Console.WriteLine("multipleBy: " + multipleBy.ToString());
				inputText = inputText.Remove(0, firstParenthesisClose + 1);
				//Console.WriteLine("cropped text: " + inputText.Substring(0, 50));
				//Console.WriteLine("multipleText: " + inputText.Substring(0, multipleLength));
				outputText += multiply(inputText.Substring(0, multipleLength), multipleBy);
				inputText = inputText.Remove(0, multipleLength);
				//try { Console.WriteLine("cropped text: " + inputText.Substring(0, 50)); } catch (Exception) { }
				//Console.WriteLine("outputText: " + outputText);
				//Console.WriteLine("compressed length: " + inputText.Length.ToString());

				//Console.WriteLine("--------------------------");
			}
			while (inputText.Length > 0);
		}

		private static void parseMarker(string marker, out int multipleLength, out int multipleBy)
		{
			multipleLength = Int32.Parse(marker.Split('x')[0]);
			multipleBy = Int32.Parse(marker.Split('x')[1]);
		}

		private static string multiply(string input, int multipleBy)
		{
			string result = "";

			for (int i = 0; i < multipleBy; i++)
			{
				result += input;
				//result += "-";
			}

			return result;
		}

		private static long part2(string s)
		{
			if(!s.Contains('('))
			{
				return s.Length;
			}

			long ret = 0;

			while(s.Contains('('))
			{
				ret += s.IndexOf('(');

				s = s.Substring(s.IndexOf('('));
				string[] marker = s.Substring(1, s.IndexOf(')') - 1).Split('x');
				s = s.Substring(s.IndexOf(')') + 1);
				ret += part2(s.Substring(0, Int32.Parse(marker[0]))) * Int32.Parse(marker[1]);
				s = s.Substring(Int32.Parse(marker[0]));
			}

			ret += s.Length;

			return ret;
		}
	}
}
