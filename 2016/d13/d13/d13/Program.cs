﻿using System;
using System.Collections.Generic;

namespace d13
{
	public class Node
	{
		public Node parent;
		public int x { get; set; }
		public int y { get; set; }
		public int currentSteps { get; set; }

		public Node(int x, int y, int currentSteps, Node parent)
		{
			this.parent = parent;
			this.x = x;
			this.y = y;
			this.currentSteps = currentSteps;
		}

		public override string ToString()
		{
			string str = "(" + x + "," + y + ") -> ";

			if (parent != null)
			{
				str += parent.ToString();
			}
			else
			{
				str += "null";
			}

			return str;
		}
	}

	public class Program
	{
		const int MAX_X = 100;
		const int MAX_Y = 100;

		const int SRC_X = 1;
		const int SRC_Y = 1;

		const int DST_X = 31;
		const int DST_Y = 39;

		public static int input { get; set; }
		public static List<Node> nodes { get; set; }
		public static char[,] maze { get; set; }
		public static List<string> checkedCoords { get; set; }
		public static List<string> part2Coords { get; set; }

		static void Main(string[] args)
		{
			nodes = new List<Node>();
			maze = new char[MAX_X, MAX_Y];
			input = 1358;
			checkedCoords = new List<string>();
			part2Coords = new List<string>();

			createMaze();
			//printMaze();

			part1();
			part2();

			Console.Read();
		}

		private static void part1()
		{
			nodes.Add(new Node(SRC_X, SRC_Y, 0, null));
			checkedCoords.Add("(" + SRC_X + "," + SRC_Y + ")");

			for (int i = 0; i < nodes.Count; i++)
			{
				addNeighbours(nodes[i]);
			}
		}

		private static void part2()
		{
			foreach (var item in nodes)
			{
				if (item.currentSteps <= 50)
				{
					string str = "(" + item.x + "," + item.y + ")";

					if (!part2Coords.Contains(str))
					{
						part2Coords.Add(str);
					}
				}
			}

			Console.WriteLine();
			Console.WriteLine("part2: " + part2Coords.Count);
		}

		private static void addNeighbours(Node parent)
		{
			if ( ! isChecked(parent.x + 1, parent.y))
			{
				addNeighbour(parent.x + 1, parent.y, parent.currentSteps, parent); 
			}

			if (!isChecked(parent.x - 1, parent.y))
			{
				addNeighbour(parent.x - 1, parent.y, parent.currentSteps, parent); 
			}

			if (!isChecked(parent.x, parent.y + 1))
			{
				addNeighbour(parent.x, parent.y + 1, parent.currentSteps, parent);
			}

			if (!isChecked(parent.x, parent.y - 1))
			{
				addNeighbour(parent.x, parent.y - 1, parent.currentSteps, parent);
			}
		}

		private static bool isChecked(int x, int y)
		{
			string str = "(" + x + "," + y + ")";

			if( ! checkedCoords.Contains(str))
			{
				checkedCoords.Add(str);
				return false;
			}

			return true;
		}

		private static void addNeighbour(int x, int y, int currentSteps, Node parent)
		{
			if (x >= 0 && y >= 0 && x < MAX_X && y < MAX_Y)
			{
				if (!isWall(x, y))
				{
					if(x == DST_X && y == DST_Y)
					{
						int tmp = currentSteps + 1;
						Console.WriteLine("destination reached in " + tmp + " steps");
						Console.WriteLine("path: " + parent);
					}

					nodes.Add(new Node(x, y, currentSteps + 1, parent));
				}
			}
		}

		private static bool isWall(int x, int y)
		{
			if (maze[x, y] == '#')
			{
				return true;
			}

			return false;
		}

		private static void createMaze()
		{
			for (int y = 0; y < MAX_Y; y++)
			{
				for (int x = 0; x < MAX_X; x++)
				{
					int num = (x * x + 3 * x + 2 * x * y + y + y * y) + input;
					string binary = Convert.ToString(num, 2);
					int oneCnt = binary.Split('1').Length - 1;

					if (oneCnt % 2 == 0)
					{
						maze[x, y] = '.';
					}
					else
					{
						maze[x, y] = '#';
					}
				}
			}
		}

		private static void printMaze()
		{
			for (int y = 0; y < MAX_Y; y++)
			{
				for (int x = 0; x < MAX_X; x++)
				{
					Console.Write(maze[x, y]);
				}

				Console.WriteLine();
			}
		}
	}
}
