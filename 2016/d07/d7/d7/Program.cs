﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace d7
{
	class Program
	{
		public static int tlsSupportedIps { get; set; }
		public static int sslSupportedIps { get; set; }
		public static string[] lines { get; set; }

		static void Main(string[] args)
		{
			lines = System.IO.File.ReadAllLines("input.txt");

			//part1();
			part2();

			Console.WriteLine("all lines: " + lines.Length.ToString());
			Console.Read();
		}

		private static void part1()
		{
			tlsSupportedIps = 0;

			foreach (var item in lines)
			{
				string[] parts = item.Split(new string[] { "[", "]" }, StringSplitOptions.None);
				bool isAbba = false;

				for (int i = 0; i < parts.Length; i++)
				{
					if (i % 2 == 0)  //isOutHypernetAddress
					{
						if (!isAbba)
						{
							isAbba = checkAbba(parts[i]);
						}
					}
					else
					{
						if (checkAbba(parts[i]))
						{
							isAbba = false;
							break;
						}
					}
				}

				if (isAbba)
				{
					tlsSupportedIps++;
				}
			}

			Console.WriteLine("tlsSupportedIps: " + tlsSupportedIps.ToString());
		}

		private static bool checkAbba(string input)
		{
			Console.WriteLine("input: " + input);

			for (int i = 0; i < input.Length - 3; i++)
			{
				if (regexMatch(input.Substring(i, 4)))
				{
					return true;
				}
			}

			return false;
		}

		private static bool regexMatch(string input)
		{
			if (input.Substring(0, 2) == input.Substring(2, 2))
			{
				return false;
			}

			Regex r = new Regex(@"(\w+)(\w+)\2\1", RegexOptions.IgnoreCase);
			Match m = r.Match(input);

			Console.WriteLine(input + " " + m.Success);

			return m.Success;
		}

		private static void part2()
		{
			sslSupportedIps = 0;

			foreach (var item in lines)
			{
				string[] parts = item.Split(new string[] { "[", "]" }, StringSplitOptions.None);

				List<string> supernet = new List<string>();
				List<string> hypernet = new List<string>();
				List<string> matches = new List<string>();

				for (int i = 0; i < parts.Length; i += 2)
				{
					supernet.Add(parts[i]);
					if (i + 1 != parts.Length)
					{
						hypernet.Add(parts[i + 1]);
					}
				}

				foreach (var supernetItem in supernet)
				{
					for (int i = 0; i < supernetItem.Length - 2; i++)
					{
						if (match(supernetItem.Substring(i, 3)))
						{
							matches.Add(supernetItem.Substring(i, 3));
						}
					}
				}

				checkHypernet(matches, hypernet);
			}

			Console.WriteLine("sslSupportedIps: " + sslSupportedIps.ToString());
		}

		private static void checkHypernet(List<string> matches, List<string> hypernet)
		{
			List<string> invertedMatches = invert(matches);

			foreach (var item in invertedMatches)
			{
				foreach (var hypernetItem in hypernet)
				{
					if(hypernetItem.Contains(item))
					{
						sslSupportedIps++;
						return;
					}
				}
			}
		}

		private static List<string> invert(List<string> matches)
		{
			List<string> invertedMatches = new List<string>();

			foreach (var item in matches)
			{
				string inverted = item;
				inverted = inverted.Replace(item[0], item[1]);
				inverted = inverted.Remove(1, 1);
				inverted = inverted.Insert(1, item[0].ToString());
				invertedMatches.Add(inverted);
			}

			return invertedMatches;
		}

		private static bool match(string input)
		{
			if(input[0] == input[2] && input[0] != input[1])
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
