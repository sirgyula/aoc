﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d16
{
	class Program
	{
		public static string input { get; set; }
		public static int fillLength { get; set; }

		public static string checksum { get; set; }
		public static string tmp { get; set; }
		public static string fillData { get; set; }

		static void Main(string[] args)
		{
			checksum = "";
			tmp = "";

			input = "01110110101001000";
			fillLength = 272;               //<-part1
			fillLength = 35651584;          //<-part2

			fillData = input;

			do
			{
				fillData = doDragonCurve();
			}
			while (fillData.Length < fillLength);

			fillData = fillData.Substring(0, fillLength);

			doChecksum();

			Console.WriteLine("checksum: " + checksum);
			Console.Read();
		}

		private static void doChecksum()
		{
			checksum = fillData;
			tmp = "";

			do
			{
				tmp = checksum;
				checksum = "";

				do
				{
					try
					{
						if (tmp[0] == tmp[1])
							checksum += '1';
						else
							checksum += '0';

						tmp = tmp.Remove(0, 2);
					}
					catch (Exception)
					{
						break;
					}
				}
				while (true);
			}
			while (checksum.Length % 2 == 0);
		}

		private static string doDragonCurve()
		{
			return fillData + "0" + reverse(flipBinary(fillData));
		}

		private static string flipBinary(string input)
		{
			return input.Replace('0', 'x').Replace('1', '0').Replace('x', '1');
		}

		private static string reverse(string input)
		{
			char[] charArray = input.ToCharArray();
			Array.Reverse(charArray);
			return new string(charArray);
		}
	}
}
