﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace d17
{
	public class Node
	{
		public Node parent;
		public int x { get; set; }
		public int y { get; set; }
		public string currentPath { get; set; }

		public Node(int x, int y, string currentSteps, Node parent)
		{
			this.parent = parent;
			this.x = x;
			this.y = y;
			this.currentPath = currentSteps;
		}

		public override string ToString()
		{
			string str = "(" + x + "," + y + ") -> ";

			if (parent != null)
			{
				str += parent.ToString();
			}
			else
			{
				str += "null";
			}

			return str;
		}
	}

	class Program
	{
		const int MAX_X = 4;
		const int MAX_Y = 4;

		const int SRC_X = 1;
		const int SRC_Y = 1;

		const int DST_X = 4;
		const int DST_Y = 4;

		public static string input { get; set; }
		public static List<Node> tree { get; set; }
		public static bool isVaultFound { get; set; }
		public static bool isPart2 { get; set; }
		public static int longestPath { get; set; }

		static void Main(string[] args)
		{
			input = "lpvhkcbi";
			input = "hijkl"; //DUR
			input = "ihgpwlah"; //DDRRRD
			//input = "kglvqrro"; //DDUDRLRRUDRD
			//input = "ulqzkmiv"; //DRURDRUDDLLDLUURRDULRLDUUDDDRR

			tree = new List<Node>();
			isVaultFound = false;
			longestPath = 0;

			isPart2 = true;

			tree.Add(new Node(SRC_X, SRC_Y, "", null));

			for (int i = 0; i < tree.Count; i++)
			{
				addNeighbours(tree[i]);

				if (isVaultFound && !isPart2)
				{
					break;
				}
			}

			Console.Read();
		}

		private static void addNeighbours(Node parent)
		{
			using (MD5 md5Hash = MD5.Create())
			{
				string hash = getMd5Hash(md5Hash, input + parent.currentPath);

				if (isOpen(hash[0]))
				{
					addNeighbour(parent.x, parent.y - 1, parent, 'U');
				}

				if (isOpen(hash[1]))
				{
					addNeighbour(parent.x, parent.y + 1, parent, 'D');
				}

				if (isOpen(hash[2]))
				{
					addNeighbour(parent.x - 1, parent.y, parent, 'L');
				}

				if (isOpen(hash[3]))
				{
					addNeighbour(parent.x + 1, parent.y, parent, 'R');
				}
			}
		}

		private static void addNeighbour(int x, int y, Node parent, char direction)
		{
			if (x >= 0 && y >= 0 && x <= MAX_X && y <= MAX_Y)
			{
				tree.Add(new Node(x, y, parent.currentPath + direction, parent));

				if (x == DST_X && y == DST_Y)
				{
					int steps = tree.Last().currentPath.Length;
					
					//Console.WriteLine("reached destination in " + steps.ToString() + " steps, path: " + tree.Last().currentPath);

					if (steps > longestPath)
					{
						longestPath = steps;
						Console.WriteLine("longest path is " + longestPath.ToString() + " steps");
					}

					//Console.WriteLine("-------");
					isVaultFound = true;
				}
			}
		}

		private static bool isOpen(char c)
		{
			if (c == 'b' || c == 'c' || c == 'd' || c == 'e' || c == 'f')
			{
				return true;
			}

			return false;
		}

		static string getMd5Hash(MD5 md5Hash, string input)
		{
			byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
			StringBuilder sBuilder = new StringBuilder();

			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			return sBuilder.ToString();
		}
	}
}
