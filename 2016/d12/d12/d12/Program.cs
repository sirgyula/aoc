﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace d12
{
	class Program
	{
		public static string[] lines { get; set; }
		public static Dictionary<char, int> registers { get; set; }

		static void Main(string[] args)
		{
			//lines = System.IO.File.ReadAllLines("input.txt");						//part1
			lines = System.IO.File.ReadAllLines("input2.txt");						//part2
			registers = new Dictionary<char, int>();

			registers.Add('a', 0);
			registers.Add('b', 0);
			registers.Add('c', 0);
			registers.Add('d', 0);

			for (int i = 0; i < lines.Length; i++)
			{
				string[] parsedCommand = lines[i].Split(' ');

				switch (parsedCommand[0])
				{
					case "cpy":
						try
						{
							registers[parsedCommand[2][0]] = Int32.Parse(parsedCommand[1]);
						}
						catch (Exception)
						{
							registers[parsedCommand[2][0]] = registers[parsedCommand[1][0]];
						}

						break;
					case "inc":
						registers[parsedCommand[1][0]]++;
						break;
					case "dec":
						registers[parsedCommand[1][0]]--;
						break;
					case "jnz":
						try
						{
							if (Int32.Parse(parsedCommand[1]) != 0)
							{
								i += Int32.Parse(parsedCommand[2]);
								i--;
							}
						}
						catch (Exception)
						{
							if (registers[parsedCommand[1][0]] != 0)
							{
								i += Int32.Parse(parsedCommand[2]);
								i--;
							}
						}

						break;
					default:
						throw new Exception("Unknown command");
				}
			}

			Console.WriteLine(string.Join(";", registers));
			Console.Read();
		}
	}
}
