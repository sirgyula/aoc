﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace d8
{
	class Program
	{
		public static char[, ] screen { get; set; }
		public static string[] lines { get; set; }
		public const int MAX_COL = 50;
		public const int MAX_ROW = 6;

		static void Main(string[] args)
		{
			lines = System.IO.File.ReadAllLines("input.txt");

			part1();

			Console.Read();
		}

		private static void part1()
		{
			initScreen();
			printScreen();

			foreach (var item in lines)
			{
				if (item.StartsWith("rect "))
				{
					string prms = item.Split(' ')[1];
					rect(Int32.Parse(prms.Split('x')[0]), Int32.Parse(prms.Split('x')[1]));
				}
				else if (item.StartsWith("rotate row "))
				{
					string[] parts = item.Split(new string[] { " ", "=" }, StringSplitOptions.None);
					rotateRow(Int32.Parse(parts[3]), Int32.Parse(parts[5]));
				}
				else if (item.StartsWith("rotate column "))
				{
					string[] parts = item.Split(new string[] { " ", "=" }, StringSplitOptions.None);
					rotateColumn(Int32.Parse(parts[3]), Int32.Parse(parts[5]));
				}
				else
				{
					throw new NotImplementedException();
				}

				printScreen();
			}
		}

		private static void rect(int column, int row)
		{
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < column; j++)
				{
					screen[i, j] = '#';
				}
			}
		}

		private static void rotateColumn(int column, int by)
		{
			List<char> selectedColumn = new List<char>();

			for (int i = 0; i < MAX_ROW; i++)
			{
				selectedColumn.Add(screen[i, column]);
			}

			for (int i = 0; i < by; i++)
			{
				char last = selectedColumn[selectedColumn.Count - 1];
				selectedColumn.RemoveAt(selectedColumn.Count - 1);
				selectedColumn.Insert(0, last);
			}

			for (int i = 0; i < MAX_ROW; i++)
			{
				screen[i, column] = selectedColumn[i];
			}
		}

		private static void rotateRow(int row, int by)
		{
			List<char> selectedRow = new List<char>();

			for (int j = 0; j < MAX_COL; j++)
			{
				selectedRow.Add(screen[row, j]);
			}

			for (int j = 0; j < by; j++)
			{
				char last = selectedRow[selectedRow.Count - 1];
				selectedRow.RemoveAt(selectedRow.Count - 1);
				selectedRow.Insert(0, last);
			}

			for (int j = 0; j < MAX_COL; j++)
			{
				screen[row, j] = selectedRow[j];
			}
		}

		private static void initScreen()
		{
			screen = new char[MAX_ROW, MAX_COL];

			for (int x = 0; x < MAX_ROW; x++)
			{
				for (int y = 0; y < MAX_COL; y++)
				{
					screen[x, y] = '.';
				}
			}
		}

		private static void printScreen()
		{
			Thread.Sleep(100);
			Console.Clear();

			for (int x = 0; x < MAX_ROW; x++)
			{
				for (int y = 0; y < MAX_COL; y++)
				{
					Console.Write(screen[x, y]);
				}
				Console.WriteLine();

			}
			Console.WriteLine("lit pixel count: " + countLitPixels().ToString());
		}

		private static int countLitPixels()
		{
			int litPixels = 0;

			for (int x = 0; x < MAX_ROW; x++)
			{
				for (int y = 0; y < MAX_COL; y++)
				{
					if(screen[x, y] == '#')
					{
						litPixels++;
					}
				}
			}

			return litPixels;
		}
	}
}
