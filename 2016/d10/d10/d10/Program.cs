﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d10
{
	class Bot : IEnumerable
	{
		public Bot(int id)
		{
			this.id = id;
			value1 = -1;
			value2 = -1;
		}

		public void setValue(int value)
		{
			if (value1 == -1) value1 = value;
			else if (value2 == -1) value2 = value;
			else throw new Exception("all values set already");
		}

		public int[] getValue()
		{
			int[] retval = new int[2];

			/*if (value1 != -1 && value2 != -1)
			{*/
			if (value1 > value2)
			{
				retval[0] = value2;
				retval[1] = value1;
			}
			else
			{
				retval[0] = value1;
				retval[1] = value2;
			}
			/*}
			else
			{
				throw new Exception("not all values set");
			}*/

			return retval;
		}

		public void print()
		{
			int[] values = getValue();

			Console.WriteLine(id.ToString() + " has low: " + values[0].ToString() + ", high: " + values[1].ToString());
		}

		public IEnumerator GetEnumerator()
		{
			throw new NotImplementedException();
		}

		public int id { get; set; }
		private int value1 { get; set; }
		private int value2 { get; set; }
	}

	class Program
	{
		public static List<Bot> bots { get; set; }
		public static List<string> lines { get; set; }

		static void Main(string[] args)
		{
			lines = System.IO.File.ReadAllLines("input.txt").ToList();
			bots = new List<Bot>();



			initBots();

			/*foreach (var item in lines)
			{
				string[] words = item.Split(' ');
				int id = Int32.Parse(words[1]);
				int lowTo = Int32.Parse(words[6]);
				int highTo = Int32.Parse(words[11]);

				try
				{
					Bot b = getBotById(id);
				}
				catch (Exception e)
				{
					Bot b = new Bot(id);
					b.setValue();
				}


			}*/










			Console.Read();
		}

		private static void initBots()
		{
			for (int i = 0; i < lines.Count; i++)
			{
				if (lines[i].StartsWith("value"))
				{
					string[] words = lines[i].Split(' ');
					Bot b = new Bot(Int32.Parse(words[5]));
					b.setValue(Int32.Parse(words[1]));
					b.print();
					bots.Add(b);
					lines.RemoveAt(i);
				}
			}
		}

		public static Bot getBotById(int id)
		{
			foreach (var item in bots)
			{
				if (item.id == id)
					return item;
			}

			throw new Exception("no bot with given id");
		}
	}
}
