﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace d14
{
	class Program
	{
		const int MAX_KEY = 64;

		public static string input { get; set; }
		public static uint index { get; set; }
		public static List<string> list { get; set; }
		public static Dictionary<uint, string> hashes { get; set; }
		public static bool isPart2 { get; set; }

		static void Main(string[] args)
		{
			input = "ihaygndm";
			isPart2 = false;

			list = new List<string>();
			hashes = new Dictionary<uint, string>();

			using (MD5 md5Hash = MD5.Create())
			{
				for (index = 0; index < UInt32.MaxValue; index++)
				{
					if (list.Count == MAX_KEY)
					{
						Console.WriteLine("count reached, index = " + (index - 1).ToString());
						break;
					}

					string hash = "";
					if (hashes.ContainsKey(index))
					{
						hash = hashes[index];
					}
					else
					{
						hash = GetMd5Hash(md5Hash, input + index.ToString());

						if (isPart2)
						{
							for (int i = 0; i < 2016; i++)
							{
								hash = GetMd5Hash(md5Hash, hash);
							} 
						}

						hashes.Add(index, hash);
					}

					for (int j = 0; j < hash.Length - 2; j++)
					{
						string str = hash[j].ToString() + hash[j].ToString() + hash[j].ToString();
						string substr = hash.Substring(j, 3);

						if (str.Equals(substr))
						{
							Console.WriteLine(index.ToString() + " " + hash + " " + str);
							searchThousand(hash[j].ToString() + hash[j].ToString() + hash[j].ToString() + hash[j].ToString() + hash[j].ToString());
							break;
						}
					}
				}
			}

			Console.Read();
		}

		private static void searchThousand(string containing)
		{
			uint tempIndex = index;

			using (MD5 md5Hash = MD5.Create())
			{
				for (tempIndex = index + 1; tempIndex < index + 1000; tempIndex++)
				{
					string hash = "";
					if (hashes.ContainsKey(tempIndex))
					{
						hash = hashes[tempIndex];
					}
					else
					{
						hash = GetMd5Hash(md5Hash, input + tempIndex.ToString());

						if (isPart2)
						{
							for (int i = 0; i < 2016; i++)
							{
								hash = GetMd5Hash(md5Hash, hash);
							} 
						}

						hashes.Add(tempIndex, hash);
					}

					if (hash.Contains(containing))
					{
						list.Add(list.Count.ToString() + " " + tempIndex.ToString() + " " + hash);
						Console.WriteLine(list.Count.ToString() + " " + tempIndex.ToString() + " " + hash);
						Console.WriteLine("-----------------");
						break;
					}
				}
			}
		}

		static string GetMd5Hash(MD5 md5Hash, string input)
		{
			byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
			StringBuilder sBuilder = new StringBuilder();

			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			return sBuilder.ToString();
		}
	}
}
