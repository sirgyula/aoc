﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d3
{
	class Program
	{
		public static string[] lines { get; set; }
		public static int validTriangles { get; set; }

		static void Main(string[] args)
		{
			lines = System.IO.File.ReadAllLines("input_formatted.txt");

			part1();
			part2();

			Console.Read();
		}

		private static void part1()
		{
			validTriangles = 0;

			foreach (string item in lines)
			{
				string[] sides = item.Split(',');

				triangle(Int32.Parse(sides[0]), Int32.Parse(sides[1]), Int32.Parse(sides[2]));
			}

			Console.WriteLine("-------------- part 1 --------------");
			Console.WriteLine("all triangles: " + lines.Length.ToString());
			Console.WriteLine("validTriangles: " + validTriangles.ToString());
		}

		private static void part2()
		{
			validTriangles = 0;

			for (int i = 0; i < lines.Length; i += 3)
			{
				string[] sides1 = lines[i].Split(',');
				string[] sides2 = lines[i+1].Split(',');
				string[] sides3 = lines[i+2].Split(',');

				triangle(Int32.Parse(sides1[0]), Int32.Parse(sides2[0]), Int32.Parse(sides3[0]));
				triangle(Int32.Parse(sides1[1]), Int32.Parse(sides2[1]), Int32.Parse(sides3[1]));
				triangle(Int32.Parse(sides1[2]), Int32.Parse(sides2[2]), Int32.Parse(sides3[2]));
			}

			Console.WriteLine("-------------- part 2 --------------");
			Console.WriteLine("all triangles: " + lines.Length.ToString());
			Console.WriteLine("validTriangles: " + validTriangles.ToString());
		}

		private static void triangle(int a, int b, int c)
		{
			if (a + b > c && a + c > b && b + c > a)
			{
				validTriangles++;
			}
		}
	}
}
