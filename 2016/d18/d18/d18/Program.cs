﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d18
{
	class Program
	{
		public static List<string> floor { get; set; }
		public static int floorSize { get; set; }
		public static int rowSize { get; set; }

		static void Main(string[] args)
		{
			floor = new List<string>();
			floorSize = 40;			//part1
			floorSize = 400000;		//part2
			floor.Add("^.^^^..^^...^.^..^^^^^.....^...^^^..^^^^.^^.^^^^^^^^.^^.^^^^...^^...^^^^.^.^..^^..^..^.^^.^.^.......");

			rowSize = floor[0].Length;

			for (int i = 1; i < floorSize; i++)
			{
				string newRow = "";

				for (int j = 0; j < rowSize; j++)
				{
					string tileTrio = "";

					if (j == 0)
						tileTrio += '.';
					else
						tileTrio += floor[i - 1][j - 1];

					tileTrio += floor[i - 1][j]; 

					if (j == rowSize - 1)
						tileTrio += '.';
					else
						tileTrio += floor[i - 1][j + 1];

					newRow += calculateTile(tileTrio);
				}

				floor.Add(newRow);
			}

			int safeTileCnt = countSafeTiles();

			printFloor();

			Console.WriteLine("safe tile count: " + safeTileCnt.ToString());
			Console.Read();
		}

		private static void printFloor()
		{
			foreach (var item in floor)
			{
				Console.WriteLine(item);
			}
		}

		private static int countSafeTiles()
		{
			int safeTileCnt = 0;

			foreach (var item in floor)
			{
				safeTileCnt += item.Count(f => f == '.');
			}

			return safeTileCnt;
		}

		private static char calculateTile(string tileTrio)
		{
			if (tileTrio == "^^." || tileTrio == ".^^" || tileTrio == "^.." || tileTrio == "..^")
				return '^';
			else
				return '.';
		}
	}
}
