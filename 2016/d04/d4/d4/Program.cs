﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d4
{
	class Program
	{
		public static int sum { get; set; }

		static void Main(string[] args)
		{
			string[] lines = System.IO.File.ReadAllLines("input.txt");

			foreach (string item in lines)
			{
				sum += checkItem(item);
			}
			
			Console.WriteLine("sector id sum: " + sum.ToString());

			Console.Read();
		}

		private static int checkItem(string item)
		{
			string name = "";
			string nameDecrypted = "";
			int id = 0;
			string checksum = "";

			parse(item, out name, out id, out checksum);
			List<KeyValuePair<char, int>> occurences = getOccurences(name);

			for (int i = 0; i < checksum.Length; i++)
			{
				if (checksum[i] != occurences[i].Key)
				{
					return 0;
				}
			}

			foreach (var chr in name)
			{
				nameDecrypted += shift(chr, id);
			}

			if (nameDecrypted.Contains("north") && nameDecrypted.Contains("pole") && nameDecrypted.Contains("objects"))
			{
				Console.WriteLine(id);
				Console.WriteLine(nameDecrypted);
			}

			return id;
		}

		private static char shift(char item, int id)
		{
			string abc = "abcdefghijklmnopqrstuvwxyz";
			int remainder = id % abc.Length;

			int newChar = abc.IndexOf(item) + remainder;

			if (newChar < abc.Length)
			{
				return abc[newChar];
			}
			else
			{
				return abc[newChar - abc.Length];
			}
		}

		private static void parse(string item, out string name, out int id, out string checksum)
		{
			checksum = item.Substring(item.IndexOf('[') + 1);
			checksum = checksum.Remove(checksum.Length - 1, 1);

			string[] temp = item.Substring(0, item.IndexOf('[')).Split('-');

			id = Int32.Parse(temp[temp.Length - 1]);

			name = String.Join("", temp.Where(w => w != temp[temp.Length - 1]).ToArray());
		}

		private static List<KeyValuePair<char, int>> getOccurences(string name)
		{
			Dictionary<char, int> occurences = new Dictionary<char, int>();

			foreach (char character in name)
			{
				if (occurences.ContainsKey(character))
				{
					int prevOcc = occurences[character];
					prevOcc++;

					occurences.Remove(character);

					occurences.Add(character, prevOcc);
				}
				else
				{
					occurences.Add(character, 1);
				}
			}

			return occurences.OrderByDescending(c => c.Value).ThenBy(n => n.Key).ToList();
		}
	}
}
