﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d6
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] lines = System.IO.File.ReadAllLines("input.txt");
			//string[] lines = System.IO.File.ReadAllLines("inputorig.txt");

			short wordSize = (short)lines[0].Length;
			List<Dictionary<char, int>> allOccurences;

			getAllOccurences(lines, wordSize, out allOccurences);

			part1(allOccurences);
			part2(allOccurences);

			Console.Read();
		}

		private static void getAllOccurences(string[] lines, short wordSize, out List<Dictionary<char, int>> allOccurences)
		{
			allOccurences = new List<Dictionary<char, int>>();

			for (int i = 0; i < wordSize; i++)
			{
				allOccurences.Add(new Dictionary<char, int>());
			}

			for (int i = 0; i < wordSize; i++)
			{
				foreach (var item in lines)
				{
					if (allOccurences[i].ContainsKey(item[i]))
					{
						int prevOcc = -1;
						allOccurences[i].TryGetValue(item[i], out prevOcc);
						prevOcc++;

						allOccurences[i].Remove(item[i]);

						allOccurences[i].Add(item[i], prevOcc);
					}
					else
					{
						allOccurences[i].Add(item[i], 1);
					}
				}
			}
		}

		private static void part1(List<Dictionary<char, int>> allOccurences)
		{
			foreach (var item in allOccurences)
			{
				Dictionary<char, int> orderedDict = item.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
				Console.Write(orderedDict.ToList()[0].Key);
			}

			Console.WriteLine();
		}

		private static void part2(List<Dictionary<char, int>> allOccurences)
		{
			foreach (var item in allOccurences)
			{
				Dictionary<char, int> orderedDict = item.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
				Console.Write(orderedDict.ToList()[0].Key);
			}

			Console.WriteLine();
		}
	}
}
