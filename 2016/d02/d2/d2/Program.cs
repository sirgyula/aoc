﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d2
{
	class Program
	{
		public static char position { get; set; }
		public static string codePart1 { get; set; }
		public static string codePart2 { get; set; }

		public static List<List<char>> directions { get; set; }

		static void Main(string[] args)
		{
			directions = new List<List<char>>();

			directions.Add(new List<char> { 'L', 'L', 'R', 'R', 'L', 'L', 'R', 'L', 'D', 'D', 'U', 'U', 'R', 'L', 'L', 'R', 'D', 'U', 'U', 'U', 'D', 'U', 'L', 'U', 'D', 'L', 'U', 'U', 'L', 'D', 'R', 'D', 'D', 'D', 'U', 'L', 'L', 'L', 'R', 'D', 'D', 'L', 'L', 'L', 'R', 'R', 'D', 'D', 'R', 'R', 'U', 'D', 'D', 'U', 'R', 'D', 'U', 'R', 'L', 'R', 'D', 'D', 'U', 'L', 'R', 'R', 'R', 'L', 'L', 'U', 'L', 'L', 'U', 'L', 'L', 'R', 'U', 'U', 'L', 'D', 'L', 'D', 'D', 'D', 'U', 'U', 'U', 'R', 'R', 'R', 'R', 'U', 'R', 'U', 'R', 'D', 'U', 'D', 'L', 'L', 'R', 'R', 'L', 'D', 'L', 'L', 'R', 'R', 'D', 'R', 'D', 'L', 'L', 'L', 'D', 'D', 'R', 'R', 'L', 'U', 'D', 'D', 'L', 'D', 'D', 'L', 'R', 'D', 'R', 'D', 'R', 'D', 'D', 'R', 'U', 'D', 'D', 'R', 'U', 'U', 'U', 'R', 'L', 'D', 'U', 'D', 'R', 'R', 'L', 'U', 'L', 'L', 'L', 'D', 'R', 'D', 'R', 'R', 'D', 'L', 'L', 'R', 'R', 'L', 'D', 'L', 'D', 'R', 'R', 'R', 'R', 'L', 'U', 'R', 'L', 'L', 'U', 'R', 'L', 'R', 'D', 'L', 'L', 'R', 'U', 'D', 'D', 'R', 'L', 'R', 'D', 'R', 'R', 'U', 'R', 'L', 'D', 'U', 'L', 'U', 'R', 'D', 'L', 'U', 'U', 'D', 'U', 'R', 'L', 'D', 'R', 'U', 'R', 'D', 'R', 'D', 'L', 'U', 'L', 'L', 'L', 'L', 'D', 'U', 'D', 'R', 'L', 'L', 'U', 'R', 'R', 'L', 'R', 'U', 'R', 'U', 'U', 'R', 'D', 'R', 'R', 'R', 'U', 'L', 'L', 'R', 'U', 'L', 'L', 'D', 'R', 'R', 'D', 'D', 'D', 'U', 'L', 'D', 'U', 'R', 'D', 'R', 'D', 'D', 'R', 'D', 'U', 'D', 'U', 'D', 'R', 'U', 'R', 'R', 'R', 'R', 'U', 'U', 'U', 'R', 'R', 'D', 'U', 'U', 'D', 'U', 'D', 'D', 'D', 'L', 'R', 'R', 'U', 'U', 'D', 'D', 'U', 'U', 'D', 'D', 'D', 'U', 'D', 'L', 'D', 'R', 'D', 'L', 'R', 'D', 'U', 'U', 'L', 'L', 'R', 'U', 'U', 'D', 'R', 'R', 'R', 'D', 'U', 'R', 'L', 'D', 'D', 'D', 'L', 'D', 'L', 'U', 'U', 'L', 'U', 'D', 'L', 'L', 'R', 'D', 'U', 'D', 'D', 'D', 'D', 'L', 'D', 'U', 'R', 'R', 'R', 'D', 'R', 'L', 'L', 'R', 'U', 'U', 'U', 'U', 'D', 'R', 'L', 'U', 'L', 'L', 'U', 'U', 'D', 'R', 'L', 'L', 'R', 'D', 'L', 'U', 'R', 'L', 'U', 'R', 'U', 'D', 'U', 'R', 'U', 'L', 'U', 'D', 'U', 'L', 'U', 'D', 'U', 'R', 'U', 'D', 'D', 'U', 'L', 'D', 'L', 'D', 'L', 'R', 'R', 'U', 'U', 'D', 'R', 'D', 'D', 'D', 'R', 'L', 'L', 'R', 'R', 'R', 'R', 'L', 'D', 'R', 'R', 'R', 'D' });
			directions.Add(new List<char> { 'D', 'R', 'R', 'R', 'D', 'U', 'L', 'L', 'R', 'U', 'R', 'U', 'D', 'R', 'L', 'R', 'D', 'L', 'R', 'U', 'L', 'R', 'R', 'L', 'R', 'L', 'D', 'L', 'U', 'D', 'L', 'U', 'U', 'R', 'U', 'U', 'U', 'R', 'U', 'R', 'U', 'L', 'R', 'L', 'R', 'U', 'D', 'R', 'U', 'R', 'R', 'R', 'L', 'L', 'U', 'D', 'R', 'L', 'L', 'D', 'U', 'D', 'U', 'L', 'L', 'U', 'U', 'D', 'L', 'L', 'U', 'U', 'U', 'D', 'D', 'R', 'L', 'R', 'U', 'D', 'D', 'D', 'D', 'L', 'D', 'D', 'U', 'U', 'D', 'U', 'L', 'D', 'R', 'R', 'R', 'D', 'U', 'L', 'U', 'U', 'L', 'D', 'U', 'L', 'D', 'R', 'U', 'U', 'U', 'L', 'R', 'U', 'D', 'D', 'D', 'U', 'D', 'R', 'R', 'L', 'R', 'L', 'U', 'D', 'D', 'U', 'R', 'L', 'L', 'D', 'R', 'L', 'U', 'D', 'U', 'D', 'U', 'R', 'U', 'U', 'D', 'R', 'L', 'U', 'U', 'R', 'R', 'L', 'U', 'U', 'U', 'D', 'U', 'U', 'R', 'U', 'D', 'U', 'R', 'L', 'U', 'U', 'U', 'D', 'R', 'D', 'R', 'R', 'R', 'D', 'R', 'D', 'R', 'U', 'L', 'L', 'U', 'U', 'R', 'U', 'R', 'D', 'L', 'U', 'U', 'L', 'L', 'D', 'U', 'U', 'L', 'U', 'U', 'D', 'U', 'L', 'L', 'L', 'D', 'U', 'R', 'L', 'U', 'D', 'R', 'U', 'R', 'U', 'L', 'D', 'L', 'D', 'L', 'R', 'D', 'R', 'L', 'R', 'L', 'U', 'U', 'R', 'D', 'D', 'R', 'L', 'D', 'D', 'L', 'R', 'R', 'U', 'R', 'U', 'D', 'L', 'U', 'D', 'D', 'D', 'L', 'D', 'R', 'L', 'U', 'L', 'L', 'D', 'R', 'L', 'L', 'L', 'U', 'R', 'U', 'L', 'L', 'U', 'U', 'R', 'L', 'U', 'D', 'D', 'U', 'R', 'R', 'D', 'D', 'L', 'D', 'D', 'D', 'D', 'R', 'D', 'U', 'U', 'U', 'L', 'U', 'R', 'D', 'L', 'U', 'U', 'U', 'L', 'R', 'R', 'L', 'R', 'D', 'L', 'D', 'R', 'D', 'D', 'D', 'R', 'L', 'L', 'R', 'U', 'D', 'U', 'L', 'R', 'R', 'R', 'U', 'D', 'R', 'R', 'L', 'D', 'R', 'R', 'U', 'U', 'L', 'U', 'D', 'D', 'L', 'L', 'D', 'U', 'D', 'D', 'R', 'L', 'R', 'R', 'D', 'L', 'D', 'D', 'U', 'L', 'L', 'L', 'R', 'D', 'U', 'R', 'R', 'U', 'R', 'L', 'L', 'U', 'L', 'U', 'R', 'R', 'L', 'U', 'U', 'L', 'U', 'L', 'R', 'D', 'L', 'U', 'L', 'L', 'U', 'U', 'U', 'L', 'R', 'R', 'R', 'L', 'R', 'U', 'D', 'L', 'R', 'U', 'U', 'D', 'D', 'R', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'U', 'R', 'L', 'D', 'R', 'R', 'U', 'U', 'R', 'L', 'D', 'U', 'L', 'D', 'L', 'D', 'D', 'R', 'L', 'L', 'L', 'R', 'D', 'L', 'L', 'L', 'D', 'L', 'R', 'U', 'U', 'D', 'R', 'U', 'R', 'D', 'R', 'D', 'L', 'U', 'U', 'L', 'D', 'D', 'R', 'L', 'L', 'R', 'R', 'U', 'R', 'R', 'D', 'U', 'L', 'L', 'U', 'L', 'U', 'R', 'R', 'D', 'U', 'L', 'R', 'U', 'D', 'U', 'D', 'R', 'L', 'U', 'U', 'D', 'D', 'D', 'D', 'U', 'U', 'L', 'D', 'D', 'D', 'U', 'U', 'D', 'U', 'R', 'L', 'R', 'U', 'D', 'D', 'U', 'L', 'D', 'D', 'D', 'D', 'R', 'U', 'U', 'L', 'U', 'U', 'D', 'L', 'U', 'D', 'D', 'R', 'D', 'R', 'D' });
			directions.Add(new List<char> { 'R', 'R', 'R', 'U', 'L', 'L', 'R', 'U', 'L', 'D', 'R', 'D', 'L', 'D', 'U', 'D', 'R', 'R', 'D', 'U', 'L', 'L', 'R', 'L', 'U', 'U', 'D', 'L', 'U', 'L', 'L', 'R', 'U', 'U', 'L', 'U', 'L', 'U', 'R', 'D', 'D', 'D', 'L', 'L', 'L', 'U', 'L', 'R', 'U', 'R', 'L', 'L', 'U', 'R', 'U', 'D', 'L', 'R', 'D', 'L', 'U', 'R', 'R', 'R', 'L', 'R', 'L', 'D', 'L', 'L', 'R', 'R', 'U', 'R', 'U', 'D', 'L', 'D', 'L', 'R', 'U', 'L', 'D', 'D', 'U', 'L', 'L', 'L', 'U', 'U', 'D', 'L', 'D', 'U', 'L', 'L', 'D', 'R', 'D', 'L', 'R', 'U', 'U', 'L', 'D', 'R', 'L', 'U', 'R', 'R', 'R', 'R', 'U', 'D', 'D', 'L', 'U', 'D', 'L', 'D', 'D', 'R', 'U', 'D', 'D', 'U', 'U', 'L', 'L', 'R', 'L', 'U', 'U', 'D', 'L', 'U', 'D', 'U', 'D', 'R', 'L', 'R', 'U', 'U', 'L', 'U', 'R', 'U', 'D', 'U', 'L', 'D', 'L', 'U', 'U', 'D', 'D', 'R', 'L', 'L', 'U', 'U', 'U', 'R', 'R', 'U', 'R', 'U', 'D', 'D', 'R', 'U', 'R', 'D', 'L', 'D', 'R', 'R', 'D', 'R', 'U', 'L', 'R', 'R', 'R', 'R', 'U', 'U', 'U', 'D', 'R', 'D', 'L', 'U', 'U', 'D', 'D', 'D', 'U', 'D', 'R', 'L', 'R', 'L', 'D', 'R', 'R', 'R', 'R', 'U', 'D', 'D', 'R', 'L', 'L', 'R', 'D', 'R', 'L', 'U', 'D', 'R', 'U', 'R', 'D', 'U', 'L', 'U', 'U', 'U', 'R', 'U', 'U', 'L', 'L', 'R', 'D', 'U', 'U', 'U', 'L', 'R', 'U', 'L', 'R', 'U', 'L', 'L', 'R', 'U', 'L', 'R', 'L', 'U', 'D', 'U', 'D', 'D', 'U', 'L', 'U', 'R', 'D', 'D', 'L', 'L', 'U', 'R', 'R', 'R', 'U', 'L', 'D', 'R', 'U', 'L', 'D', 'U', 'U', 'D', 'D', 'U', 'L', 'D', 'U', 'L', 'D', 'R', 'L', 'R', 'U', 'U', 'L', 'D', 'R', 'D', 'L', 'D', 'U', 'D', 'R', 'D', 'U', 'D', 'L', 'U', 'R', 'L', 'L', 'U', 'R', 'R', 'D', 'L', 'L', 'D', 'U', 'L', 'L', 'D', 'R', 'U', 'L', 'D', 'L', 'L', 'R', 'D', 'U', 'L', 'L', 'R', 'U', 'R', 'R', 'D', 'U', 'L', 'U', 'D', 'L', 'U', 'L', 'R', 'R', 'U', 'D', 'D', 'U', 'L', 'R', 'L', 'D', 'L', 'D', 'L', 'L', 'L', 'D', 'U', 'D', 'L', 'U', 'R', 'U', 'R', 'R', 'L', 'U', 'D', 'R', 'R', 'U', 'R', 'L', 'D', 'D', 'U', 'R', 'U', 'L', 'D', 'U', 'R', 'R', 'D', 'U', 'D', 'U', 'U', 'R', 'U', 'R', 'U', 'L', 'L', 'L', 'U', 'D', 'D', 'L', 'D', 'U', 'R', 'U', 'R', 'R', 'U', 'R', 'D', 'D', 'D', 'R', 'R', 'D', 'R', 'U', 'R', 'R', 'U', 'U', 'R', 'R', 'L', 'D', 'D', 'L', 'R', 'R', 'L', 'D', 'D', 'U', 'L', 'R', 'L', 'L', 'L', 'D', 'D', 'U', 'D', 'R', 'U', 'L', 'U', 'U', 'L', 'L', 'U', 'L', 'U', 'U', 'L', 'D', 'R', 'L', 'U', 'R', 'R', 'R', 'L', 'R', 'R', 'R', 'L', 'D', 'R', 'R', 'L', 'U', 'L', 'R', 'L', 'R', 'L', 'U', 'R', 'D', 'U', 'U', 'L', 'D', 'D', 'U', 'D', 'L', 'L', 'L', 'U', 'U', 'R', 'R', 'R', 'L', 'D', 'L', 'U', 'D', 'R', 'L', 'L', 'L', 'R', 'R', 'U', 'U' });
			directions.Add(new List<char> { 'U', 'R', 'L', 'D', 'D', 'D', 'L', 'D', 'R', 'D', 'D', 'D', 'U', 'R', 'R', 'R', 'L', 'U', 'R', 'R', 'R', 'R', 'L', 'U', 'L', 'U', 'R', 'L', 'D', 'D', 'U', 'D', 'R', 'D', 'U', 'D', 'D', 'L', 'U', 'R', 'U', 'R', 'L', 'L', 'R', 'D', 'U', 'R', 'D', 'D', 'R', 'L', 'R', 'U', 'U', 'R', 'L', 'D', 'L', 'L', 'R', 'D', 'L', 'R', 'U', 'U', 'U', 'R', 'L', 'R', 'L', 'D', 'L', 'D', 'R', 'U', 'D', 'D', 'D', 'U', 'L', 'L', 'D', 'U', 'L', 'L', 'D', 'U', 'U', 'L', 'U', 'R', 'L', 'D', 'R', 'D', 'U', 'D', 'R', 'R', 'L', 'R', 'R', 'L', 'U', 'L', 'R', 'D', 'D', 'U', 'L', 'U', 'D', 'U', 'L', 'D', 'U', 'L', 'L', 'U', 'L', 'D', 'L', 'R', 'R', 'L', 'R', 'R', 'L', 'L', 'U', 'L', 'R', 'U', 'L', 'D', 'L', 'L', 'D', 'U', 'L', 'R', 'R', 'L', 'D', 'U', 'R', 'R', 'R', 'R', 'D', 'L', 'U', 'R', 'D', 'L', 'U', 'D', 'U', 'U', 'U', 'D', 'L', 'U', 'R', 'R', 'R', 'R', 'R', 'U', 'D', 'D', 'U', 'D', 'U', 'U', 'D', 'U', 'L', 'D', 'L', 'U', 'R', 'R', 'D', 'R', 'L', 'R', 'L', 'U', 'D', 'U', 'D', 'U', 'U', 'D', 'U', 'L', 'D', 'D', 'U', 'R', 'U', 'D', 'D', 'R', 'D', 'R', 'U', 'D', 'L', 'R', 'R', 'U', 'D', 'R', 'U', 'L', 'D', 'U', 'L', 'R', 'D', 'R', 'L', 'D', 'R', 'U', 'D', 'R', 'L', 'L', 'R', 'U', 'U', 'D', 'D', 'R', 'L', 'U', 'R', 'U', 'R', 'D', 'R', 'R', 'L', 'R', 'U', 'R', 'U', 'L', 'L', 'D', 'U', 'U', 'D', 'R', 'D', 'L', 'U', 'L', 'R', 'U', 'U', 'L', 'U', 'D', 'U', 'R', 'R', 'U', 'L', 'L', 'R', 'L', 'U', 'U', 'U', 'U', 'U', 'D', 'U', 'L', 'R', 'L', 'U', 'U', 'D', 'R', 'D', 'U', 'U', 'U', 'L', 'L', 'U', 'L', 'U', 'D', 'U', 'D', 'D', 'L', 'L', 'R', 'R', 'L', 'D', 'U', 'R', 'R', 'D', 'D', 'D', 'L', 'U', 'D', 'L', 'U', 'U', 'D', 'U', 'L', 'U', 'U', 'U', 'L', 'D', 'L', 'L', 'L', 'L', 'U', 'U', 'D', 'U', 'R', 'R', 'U', 'D', 'U', 'D', 'L', 'U', 'L', 'D', 'R', 'R', 'R', 'U', 'L', 'L', 'L', 'U', 'R', 'D', 'U', 'R', 'D', 'D', 'L', 'R', 'R', 'U', 'L', 'U', 'R', 'U', 'D', 'U', 'R', 'U', 'L', 'R', 'D', 'R', 'U', 'L', 'L', 'R', 'U', 'R', 'U', 'R', 'R', 'U', 'D', 'U', 'U', 'L', 'R', 'U', 'L', 'U', 'U', 'D', 'D', 'U', 'D', 'D', 'U', 'U', 'R', 'L', 'R', 'L', 'U', 'R', 'R', 'R', 'R', 'D', 'L', 'U', 'L', 'R', 'R', 'L', 'D', 'R', 'R', 'D', 'U', 'R', 'U', 'D', 'U', 'R', 'U', 'L', 'U', 'L', 'L', 'R', 'U', 'U', 'R', 'L', 'L', 'D', 'R', 'D', 'R', 'U', 'R', 'L', 'L', 'L', 'U', 'U', 'U', 'R', 'U', 'U', 'D', 'D', 'D', 'L', 'D', 'U', 'R', 'R', 'L', 'L', 'U', 'U', 'U', 'U', 'U', 'R', 'L', 'L', 'D', 'U', 'D', 'L', 'R', 'U', 'R', 'U', 'U', 'U', 'D', 'L', 'R', 'L', 'R', 'R', 'L', 'R', 'L', 'D', 'U', 'R', 'U', 'R', 'R', 'U', 'R', 'L', 'U', 'L', 'D', 'L', 'R', 'D', 'L', 'U', 'D', 'D', 'U', 'L', 'L', 'D', 'U', 'D', 'L', 'U', 'L', 'L', 'U', 'U', 'U', 'D', 'L', 'R', 'L', 'D', 'U', 'U', 'R', 'R', 'R' });
			directions.Add(new List<char> { 'R', 'L', 'L', 'D', 'R', 'D', 'U', 'R', 'R', 'U', 'D', 'U', 'L', 'L', 'U', 'R', 'L', 'R', 'L', 'L', 'U', 'R', 'U', 'D', 'D', 'L', 'U', 'L', 'U', 'U', 'L', 'R', 'R', 'R', 'D', 'R', 'L', 'U', 'L', 'D', 'D', 'R', 'L', 'U', 'D', 'R', 'D', 'U', 'R', 'L', 'U', 'U', 'L', 'D', 'U', 'D', 'D', 'D', 'D', 'D', 'U', 'D', 'D', 'D', 'D', 'L', 'D', 'U', 'D', 'R', 'D', 'R', 'R', 'L', 'R', 'L', 'R', 'L', 'U', 'R', 'D', 'U', 'R', 'R', 'U', 'R', 'D', 'L', 'U', 'R', 'D', 'U', 'R', 'R', 'U', 'U', 'U', 'L', 'U', 'L', 'L', 'U', 'U', 'R', 'D', 'L', 'U', 'R', 'D', 'U', 'U', 'R', 'R', 'D', 'L', 'D', 'L', 'D', 'D', 'U', 'U', 'R', 'D', 'D', 'U', 'R', 'L', 'D', 'D', 'D', 'R', 'U', 'U', 'R', 'L', 'D', 'U', 'R', 'R', 'U', 'R', 'U', 'L', 'U', 'R', 'L', 'R', 'R', 'L', 'U', 'D', 'D', 'U', 'D', 'D', 'D', 'L', 'L', 'U', 'L', 'U', 'D', 'U', 'U', 'U', 'D', 'R', 'U', 'L', 'L', 'L', 'L', 'U', 'L', 'L', 'R', 'D', 'D', 'R', 'D', 'L', 'R', 'D', 'R', 'R', 'D', 'R', 'R', 'D', 'L', 'D', 'L', 'D', 'D', 'U', 'U', 'R', 'R', 'R', 'D', 'D', 'U', 'L', 'R', 'U', 'U', 'U', 'R', 'U', 'D', 'R', 'D', 'D', 'L', 'R', 'L', 'R', 'L', 'R', 'R', 'D', 'L', 'D', 'R', 'D', 'L', 'L', 'D', 'R', 'R', 'D', 'L', 'L', 'U', 'U', 'L', 'U', 'D', 'L', 'L', 'U', 'D', 'U', 'U', 'D', 'R', 'D', 'L', 'U', 'R', 'R', 'R', 'R', 'U', 'L', 'D', 'R', 'D', 'R', 'U', 'D', 'U', 'L', 'R', 'L', 'L', 'L', 'L', 'R', 'R', 'U', 'L', 'D', 'L', 'D', 'U', 'U', 'U', 'U', 'R', 'L', 'D', 'U', 'L', 'D', 'D', 'L', 'L', 'D', 'D', 'R', 'L', 'L', 'U', 'R', 'L', 'U', 'D', 'U', 'L', 'U', 'R', 'R', 'R', 'U', 'U', 'L', 'U', 'R', 'D', 'R', 'U', 'D', 'L', 'R', 'L', 'L', 'L', 'R', 'D', 'D', 'L', 'U', 'L', 'L', 'D', 'R', 'U', 'R', 'D', 'D', 'L', 'L', 'D', 'U', 'D', 'R', 'U', 'D', 'R', 'L', 'R', 'R', 'L', 'U', 'L', 'L', 'D', 'R', 'R', 'D', 'U', 'L', 'D', 'L', 'R', 'D', 'D', 'R', 'D', 'U', 'U', 'R', 'D', 'R', 'R', 'R', 'L', 'R', 'D', 'L', 'D', 'U', 'D', 'D', 'D', 'L', 'L', 'U', 'D', 'U', 'R', 'R', 'U', 'U', 'U', 'L', 'L', 'D', 'R', 'L', 'U', 'D', 'L', 'D', 'R', 'R', 'R', 'R', 'D', 'D', 'D', 'L', 'L', 'R', 'R', 'D', 'U', 'U', 'R', 'U', 'R', 'L', 'R', 'U', 'R', 'R', 'D', 'U', 'D', 'U', 'U', 'R', 'R', 'D', 'R', 'R', 'U', 'D', 'R', 'L', 'U', 'R', 'L', 'U', 'D', 'D', 'D', 'L', 'U', 'D', 'U', 'D', 'R', 'D', 'R', 'U', 'R', 'R', 'D', 'D', 'D', 'D', 'R', 'D', 'L', 'R', 'U', 'D', 'R', 'D', 'R', 'L', 'L', 'D', 'U', 'L', 'R', 'U', 'R', 'U', 'L', 'U', 'L', 'D', 'R', 'L', 'R', 'L', 'R', 'R', 'L', 'D', 'U', 'R', 'R', 'R', 'U', 'L' });

			solvePart1();
			solvePart2();

			Console.Read();
		}

		private static void solvePart1()
		{
			position = '5';
			codePart1 = "";

			foreach (List<char> direction in directions)
			{
				foreach (char item in direction)
				{
					position = move(position, item);
				}

				codePart1 = codePart1 + position.ToString();
			}

			Console.WriteLine("codePart1: " + codePart1);
		}

		private static void solvePart2()
		{
			position = '5';
			codePart2 = "";

			foreach (List<char> direction in directions)
			{
				foreach (char item in direction)
				{
					position = move2(position, item);
				}

				codePart2 = codePart2 + position.ToString();
			}

			Console.WriteLine("codePart2: " + codePart2);
		}

		private static char move(char position, char direction)
		{
			char newPos = 'X';

			if (direction == 'L')
			{
				newPos = left(position);
			}
			else if (direction == 'U')
			{
				newPos = up(position);
			}
			else if (direction == 'R')
			{
				newPos = right(position);
			}
			else if (direction == 'D')
			{
				newPos = down(position);
			}

			return newPos;
		}

		private static char move2(char position, char direction)
		{
			char newPos = 'X';

			if (direction == 'L')
			{
				newPos = left2(position);
			}
			else if (direction == 'U')
			{
				newPos = up2(position);
			}
			else if (direction == 'R')
			{
				newPos = right2(position);
			}
			else if (direction == 'D')
			{
				newPos = down2(position);
			}

			return newPos;
		}

		private static char down(char position)
		{
			switch (position)
			{
				case '1': return '4';
				case '2': return '5';
				case '3': return '6';
				case '4': return '7';
				case '5': return '8';
				case '6': return '9';
				default: return position;
			}
		}

		private static char right(char position)
		{
			switch (position)
			{
				case '1': return '2';
				case '2': return '3';
				case '4': return '5';
				case '5': return '6';
				case '7': return '8';
				case '8': return '9';
				default: return position;
			}
		}

		private static char up(char position)
		{
			switch (position)
			{
				case '4': return '1';
				case '5': return '2';
				case '6': return '3';
				case '7': return '4';
				case '8': return '5';
				case '9': return '6';
				default: return position;
			}
		}

		private static char left(char position)
		{
			switch (position)
			{
				case '2': return '1';
				case '3': return '2';
				case '5': return '4';
				case '6': return '5';
				case '8': return '7';
				case '9': return '8';
				default: return position;
			}
		}

		private static char down2(char position)
		{
			switch (position)
			{
				case '1': return '3';
				case '2': return '6';
				case '3': return '7';
				case '4': return '8';
				case '6': return 'A';
				case '7': return 'B';
				case '8': return 'C';
				case 'B': return 'D';
				default: return position;
			}
		}

		private static char right2(char position)
		{
			switch (position)
			{
				case '2': return '3';
				case '3': return '4';
				case '5': return '6';
				case '6': return '7';
				case '7': return '8';
				case '8': return '9';
				case 'A': return 'B';
				case 'B': return 'C';
				default: return position;
			}
		}

		private static char up2(char position)
		{
			switch (position)
			{
				case '3': return '1';
				case '6': return '2';
				case '7': return '3';
				case '8': return '4';
				case 'A': return '6';
				case 'B': return '7';
				case 'C': return '8';
				case 'D': return 'B';
				default: return position;
			}
		}

		private static char left2(char position)
		{
			switch (position)
			{
				case '3': return '2';
				case '4': return '3';
				case '6': return '5';
				case '7': return '6';
				case '8': return '7';
				case '9': return '8';
				case 'B': return 'A';
				case 'C': return 'B';
				default: return position;
			}
		}
	}
}
