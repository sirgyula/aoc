﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace d5
{
	class Program
	{
		static void Main(string[] args)
		{
			string inputWord = "uqwqemis";

			part1(inputWord);
			Console.WriteLine("------------");
			part2(inputWord);

			Console.Read();
		}

		private static void part1(string inputWord)
		{
			long index = 0;
			string password = "";
			string hash = "";

			using (MD5 md5Hash = MD5.Create())
			{
				do
				{
					string currentInput = inputWord + index.ToString();
					hash = GetMd5Hash(md5Hash, currentInput);

					if (hash.Substring(0, 5) == "00000")
					{
						password += hash[5];
						Console.WriteLine(password.Length);
					}

					index++;
				} while (password.Length != 8);

				Console.WriteLine(password);
			}
		}

		private static void part2(string inputWord)
		{
			long index = 0;
			char[] password = new char[] { '-', '-', '-', '-', '-', '-', '-', '-'};
			string hash = "";

			using (MD5 md5Hash = MD5.Create())
			{
				do
				{
					string currentInput = inputWord + index.ToString();
					hash = GetMd5Hash(md5Hash, currentInput);

					if (hash.Substring(0, 5) == "00000")
					{
						try
						{
							short position = (short)Int32.Parse(hash[5].ToString());
							char newChar = hash[6];

							if (password[position] == '-' && position < 8)
							{
								password[position] = newChar;
								Console.WriteLine(String.Join("", password));
							}
						}
						catch (Exception) { }
					}

					index++;
				} while (String.Join("", password).Contains('-'));

				Console.WriteLine("-----------------");
				Console.WriteLine(String.Join("", password));
			}
		}

		static string GetMd5Hash(MD5 md5Hash, string input)
		{
			// Convert the input string to a byte array and compute the hash.
			byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

			// Create a new Stringbuilder to collect the bytes
			// and create a string.
			StringBuilder sBuilder = new StringBuilder();

			// Loop through each byte of the hashed data 
			// and format each one as a hexadecimal string.
			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			// Return the hexadecimal string.
			return sBuilder.ToString();
		}
	}
}
