﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d19
{
	class Program
	{
		const int MAX_ELFS = 3014603;
		//const int MAX_ELFS = 5;

		public static Dictionary<int, bool> elfs { get; set; }

		static void Main(string[] args)
		{
			/*elfs = new Dictionary<int, bool>();
			initElfs();
			part1();*/

			elfs = new Dictionary<int, bool>();
			initElfs();
			part2();


			//printElfs();

			Console.Read();
		}

		private static void part2()
		{
			// azokat az elfeket, akitől elvettük az ajándékot, törölni kell az elfs-ből
			// !!!figyelni kell a nem létező kulcsú elfekre!!!
			// utána mindig az elfs.count/2 ül szembe, esetleg intre castolva ha tört

			int idx = 1;

			while (true)
			{
				if (elfs.ContainsKey(idx))
				{
					int oppositeGiftIndex = getOppositeGiftIndex(idx);

					if (oppositeGiftIndex != -1)
					{
						elfs.Remove(oppositeGiftIndex);
					}
					else
					{
						break;
					}
				}

				idx++;

				if (idx == MAX_ELFS + 1)
				{
					idx = 1;
				}
			}

			Console.WriteLine("part2: Elf " + idx.ToString() + " has all the presents");
		}

		private static int getOppositeGiftIndex(int idx)
		{
			if (elfs.Count == 1)
			{
				return -1;
			}

			int half = elfs.Keys.Count / 2;

			int a = half + idx;

			if(a > elfs.Count)
			{
				return a - elfs.Count;
			}

			return a;
		}

		private static void part1()
		{
			int idx = 1;

			while (true)
			{
				if (elfs[idx] == true)
				{
					int nextGiftIndex = getNextGiftIndex(idx);

					if (nextGiftIndex != -1)
					{
						elfs[nextGiftIndex] = false;
					}
					else
					{
						break;
					}
				}

				idx++;

				if (idx == MAX_ELFS + 1)
				{
					idx = 1;
				}
			}

			Console.WriteLine("part1: Elf " + idx.ToString() + " has all the presents");
		}

		private static int getNextGiftIndex(int currentElfIndex)
		{
			int idx = currentElfIndex + 1;

			while (idx != currentElfIndex)
			{
				if (idx == MAX_ELFS + 1)
				{
					idx = 1;
				}

				if (elfs[idx] == true)
				{
					return idx;
				}

				idx++;
			}

			return -1;
		}

		private static void printElfs()
		{
			foreach (var item in elfs)
			{
				Console.WriteLine(item);
			}
		}

		private static void initElfs()
		{
			for (int i = 1; i <= MAX_ELFS; i++)
			{
				elfs.Add(i, true);
			}
		}
	}
}
