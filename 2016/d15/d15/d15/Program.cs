﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d15
{
	public class Disc
	{
		public int positions { get; set; }
		public int current { get; set; }

		public Disc(int positions, int start)
		{
			this.positions = positions;
			this.current = start;
		}

		public void tick()
		{
			if((current + 1) % positions == 0)
			{
				current = 0;
			}
			else
			{
				current++;
			}
		}
	}

	class Program
	{
		public static List<Disc> discs { get; set; }
		public static int currentTick { get; set; }

		static void Main(string[] args)
		{
			discs = new List<Disc>();
			currentTick = 0;

			discs.Add(new Disc(17, 1));
			discs.Add(new Disc(7, 0));
			discs.Add(new Disc(19, 2));
			discs.Add(new Disc(5, 0));
			discs.Add(new Disc(3, 0));
			discs.Add(new Disc(13, 5));
			discs.Add(new Disc(11, 0));				// <- only for part2

			while (true)
			{
				bool isAligned = checkDiscs();

				if(isAligned)
				{
					break;
				}

				tickDiscs();
				currentTick++;
			}

			Console.WriteLine("aligned: " + currentTick.ToString());
			Console.Read();
		}

		private static bool checkDiscs()
		{
			for (int i = 0; i < discs.Count; i++)
			{
				int timedPosition = -1;

				if (i > discs[i].positions)
				{
					timedPosition = discs[i].positions - (i % discs[i].positions) - 1;
				}
				else
				{
					timedPosition = discs[i].positions - i - 1;
				}

				if (discs[i].current != timedPosition)
				{
					return false;
				}
			}

			return true;
		}

		private static void tickDiscs()
		{
			foreach (var item in discs)
			{
				item.tick();
			}
		}
	}
}
