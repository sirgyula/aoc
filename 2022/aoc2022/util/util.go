// You can edit this code!
// Click here and start typing.
package util

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

func Printme() {
	fmt.Println("Hello, 世界")
}

func Readfile(filename string) string {
	dat, err := os.ReadFile(filename)

	if err != nil {
		panic(err)
	}

	return string(dat)
}

func MyPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func GetFileScanner(filepath string) (*bufio.Scanner, *os.File) {
	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	return fileScanner, readFile
}

func PrintElaspedTime(start time.Time) {
	elapsed := time.Since(start)
	fmt.Printf("took %s, answer: ", elapsed)
}
