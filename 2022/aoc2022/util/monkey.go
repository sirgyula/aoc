package util

import (
	"errors"
	"strconv"
)

type Monkey struct {
	Items          []int
	Operation      string
	TestDivVal     int
	DstMonkeyTrue  int
	DstMonkeyFalse int
	InspectCount   int
}

func NewMonkey() *Monkey {
	m := new(Monkey)

	return m
}

func (m *Monkey) SetItems(items []string) {
	for _, i := range items {
		val, _ := strconv.ParseInt(i, 10, 64)
		m.Items = append(m.Items, int(val))
	}
}

func (m *Monkey) GetFirstItem() (int, error) {
	if len(m.Items) == 0 {
		return 0, errors.New("empty")
	}

	m.InspectCount++
	item := m.Items[0]
	m.Items = m.Items[1:]

	return item, nil
}

func (m *Monkey) AddItem(item int, worryFixer int) {
	if item > worryFixer {
		m.Items = append(m.Items, item%worryFixer)
	} else {
		m.Items = append(m.Items, item)
	}
}
