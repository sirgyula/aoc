module aoc2022

go 1.21.4

require (
	github.com/crsmithdev/goexpr v0.0.0-20150309021426-69a8c42346f1
	golang.org/x/exp v0.0.0-20221227203929-1b447090c38c
)

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/goccy/go-yaml v1.8.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mikefarah/yq/v3 v3.0.0-20201202084205-8846255d1c37 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/cobra v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/op/go-logging.v1 v1.0.0-20160211212156-b2cb9fa56473 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
