// You can edit this code!
// Click here and start typing.
package main

import (
	"aoc2022/days/d15"
)

func main() {
	//days.Solve01()
	//days.Solve02()
	//days.Solve03()
	//days.Solve04()
	//days.Solve05()
	//days.Solve06()
	//days.Solve07()
	//days.Solve08()
	//days.Solve09_1()
	//days.Solve09_2()
	//days.Solve10()
	//days.Solve11()
	//days.Solve12()
	//days.Solve13()
	//d14_1.Solve14()
	//d14_2.Solve14()
	d15.Solve()
}
