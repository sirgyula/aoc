// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"strconv"
	"strings"
)

var visited = make(map[string]struct{})

func updateVisited(tx int, ty int) {
	visited[strconv.Itoa(tx)+","+strconv.Itoa(ty)] = struct{}{}
}

func determineRelation(hx int, hy int, tx int, ty int) int {
	if hx == tx && hy == ty {
		return 1
	} else if hy == ty && hx-tx == 1 {
		return 2
	} else if hy == ty && tx-hx == 1 {
		return 3
	} else if hx == tx && ty-hy == 1 {
		return 4
	} else if hx == tx && hy-ty == 1 {
		return 5
	} else if hy+1 == ty && hx-1 == tx {
		return 6
	} else if hy+1 == ty && hx+1 == tx {
		return 7
	} else if hy-1 == ty && hx-1 == tx {
		return 8
	} else if hy-1 == ty && hx+1 == tx {
		return 9
	} else {
		return -1
	}
}

func relation1(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
	case "R":
		*hx++
	case "D":
		*hy--
	case "L":
		*hx--
	default:
		fmt.Println("FU")
	}
}

func relation2(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
	case "R":
		*hx++
		*tx++
	case "D":
		*hy--
	case "L":
		*hx--
	default:
		fmt.Println("FU")
	}
}

func relation3(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
	case "R":
		*hx++
	case "D":
		*hy--
	case "L":
		*hx--
		*tx--
	default:
		fmt.Println("FU")
	}
}

func relation4(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
	case "R":
		*hx++
	case "D":
		*hy--
		*ty--
	case "L":
		*hx--
	default:
		fmt.Println("FU")
	}
}

func relation5(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
		*ty++
	case "R":
		*hx++
	case "D":
		*hy--
	case "L":
		*hx--
	default:
		fmt.Println("FU")
	}
}

func relation6(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
	case "R":
		*hx++
		*tx++
		*ty--
	case "D":
		*hy--
		*tx++
		*ty--
	case "L":
		*hx--
	default:
		fmt.Println("FU")
	}
}

func relation7(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
	case "R":
		*hx++
	case "D":
		*hy--
		*tx--
		*ty--
	case "L":
		*hx--
		*tx--
		*ty--
	default:
		fmt.Println("FU")
	}
}

func relation8(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
		*tx++
		*ty++
	case "R":
		*hx++
		*tx++
		*ty++
	case "D":
		*hy--
	case "L":
		*hx--
	default:
		fmt.Println("FU")
	}
}

func relation9(hx *int, hy *int, tx *int, ty *int, direction string) {
	switch direction {
	case "U":
		*hy++
		*tx--
		*ty++
	case "R":
		*hx++
	case "D":
		*hy--
	case "L":
		*hx--
		*tx--
		*ty++
	default:
		fmt.Println("FU")
	}
}

func first09(filepath string) int {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	hx, hy, tx, ty := 0, 0, 0, 0
	visited = make(map[string]struct{})

	updateVisited(tx, ty)

	for fileScanner.Scan() {
		line := fileScanner.Text()

		lineParts := strings.Fields(line)

		move, _ := strconv.Atoi(lineParts[1])
		direction := lineParts[0]

		for m := 0; m < move; m++ {
			relation := determineRelation(hx, hy, tx, ty)

			switch relation {
			case 1:
				relation1(&hx, &hy, &tx, &ty, direction)
			case 2:
				relation2(&hx, &hy, &tx, &ty, direction)
			case 3:
				relation3(&hx, &hy, &tx, &ty, direction)
			case 4:
				relation4(&hx, &hy, &tx, &ty, direction)
			case 5:
				relation5(&hx, &hy, &tx, &ty, direction)
			case 6:
				relation6(&hx, &hy, &tx, &ty, direction)
			case 7:
				relation7(&hx, &hy, &tx, &ty, direction)
			case 8:
				relation8(&hx, &hy, &tx, &ty, direction)
			case 9:
				relation9(&hx, &hy, &tx, &ty, direction)
			default:
				fmt.Println("moving into 3d space")
			}

			updateVisited(tx, ty)
		}
	}

	return len(visited)
}

func Solve09_1() {
	fmt.Println(first09("input/09_0"))
	fmt.Println(first09("input/09_s"))
	fmt.Println("----------")
}
