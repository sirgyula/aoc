// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"bufio"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"golang.org/x/exp/slices"
)

const EMPTY = "EMPTY"
const NaA = "NaA" //Not an Array

var leftPacketMap map[string]string
var rightPacketMap map[string]string

func replaceInnerArray(s *string, from int, to int, r rune) *string {
	res := (*s)[:from] + string(r) + (*s)[to+1:]
	return &res
}

func parsePacket(packet *string, m *map[string]string) string {
	arrayIdentifierChar := 'a'

	for {
		firstClosingBracket := strings.IndexRune(*packet, ']')

		if firstClosingBracket == -1 { // no more inner arrays
			break
		}

		openerForFirstClosingBracket := strings.LastIndexByte((*packet)[:firstClosingBracket], '[')

		innerArray := (*packet)[openerForFirstClosingBracket+1 : firstClosingBracket]

		if innerArray == "" {
			innerArray = EMPTY // unique marker for empty arrays
		}

		packet = replaceInnerArray(packet, openerForFirstClosingBracket, firstClosingBracket, arrayIdentifierChar)
		(*m)[string(arrayIdentifierChar)] = innerArray
		arrayIdentifierChar++
	}

	arrayIdentifierChar--

	return (*m)[string(arrayIdentifierChar)]
}

func parsePair(fileScanner *bufio.Scanner) (*string, *string) {
	packet := fileScanner.Text()
	leftPacket := parsePacket(&packet, &leftPacketMap)

	fileScanner.Scan()

	packet = fileScanner.Text()
	rightPacket := parsePacket(&packet, &rightPacketMap)

	fileScanner.Scan()

	return &leftPacket, &rightPacket
}

func getNumOrArray(a *[]string, i *int, m *map[string]string) (int, string) {
	num, err := strconv.Atoi((*a)[*i])
	new := NaA

	if err != nil {
		new = (*m)[(*a)[*i]]
		num = -1
	}

	*i++

	return num, new
}

func isEmptyArray(a *[]string) bool {
	return len(*a) == 1 && string((*a)[0]) == EMPTY
}

func checkExitCondition(leftSplitted *[]string, rightSplitted *[]string, leftI *int, rightI *int) int {
	if len(*rightSplitted) == *rightI && len(*leftSplitted) == *leftI {
		return 0 //eq
	} else if len(*rightSplitted) == *rightI {
		return -1 //loose
	} else if len(*leftSplitted) == *leftI {
		return 1 //win
	}

	return 2 //needs further work
}

func compare(leftPacket *string, rightPacket *string, leftI *int, rightI *int) int {
	leftSplitted := strings.Split(*leftPacket, ",")
	rightSplitted := strings.Split(*rightPacket, ",")

	if isEmptyArray(&leftSplitted) && !isEmptyArray(&rightSplitted) {
		return 1
	} else if !isEmptyArray(&leftSplitted) && isEmptyArray(&rightSplitted) {
		return -1
	}

	for {
		exit := checkExitCondition(&leftSplitted, &rightSplitted, leftI, rightI)

		if exit != 2 {
			return exit
		}

		leftNum, newLeft := getNumOrArray(&leftSplitted, leftI, &leftPacketMap)
		rightNum, newRight := getNumOrArray(&rightSplitted, rightI, &rightPacketMap)

		if newLeft == EMPTY && newRight == EMPTY {
			continue
		} else if newLeft == NaA && newRight == NaA { //both int
			if leftNum == rightNum {
				continue
			} else if leftNum < rightNum {
				return 1 //win
			} else {
				return -1 //loose
			}
		} else if newLeft == NaA { //only left int
			nli, nri := 0, 0
			currentLeft := strconv.Itoa(leftNum)
			res := compare(&currentLeft, &newRight, &nli, &nri)

			if res != 0 {
				return res
			}
		} else if newRight == NaA { //only right int
			nli, nri := 0, 0
			currentRight := strconv.Itoa(rightNum)
			res := compare(&newLeft, &currentRight, &nli, &nri)

			if res != 0 {
				return res
			}
		} else if newLeft != NaA && newRight != NaA { //both array
			nli, nri := 0, 0
			res := compare(&newLeft, &newRight, &nli, &nri)

			if res != 0 {
				return res
			}
		} else {
			fmt.Println("what?")
		}
	}
}

func sortIt(packets []string) {
	sort.Slice(packets, func(i, j int) bool {
		leftPacketMap = make(map[string]string)
		rightPacketMap = make(map[string]string)

		leftPacket := parsePacket(&packets[i], &leftPacketMap)
		rightPacket := parsePacket(&packets[j], &rightPacketMap)

		leftI, rightI := 0, 0
		res := compare(&leftPacket, &rightPacket, &leftI, &rightI)

		return res == 1
	})
}

func getIndex(packets []string, searching string) int {
	return slices.IndexFunc(packets, func(packet string) bool { return packet == searching }) + 1
}

func first13(filepath string) int {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	sum := 0
	pairIndex := 1

	for fileScanner.Scan() {
		leftPacketMap = make(map[string]string)
		rightPacketMap = make(map[string]string)

		leftPacket, rightPacket := parsePair(fileScanner)
		leftI, rightI := 0, 0
		res := compare(leftPacket, rightPacket, &leftI, &rightI)

		if res == 1 {
			sum += pairIndex
		}

		if res == 0 {
			fmt.Println("how?!")
		}

		pairIndex++
	}

	return sum
}

func second13(filepath string) int {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	packetTwo, packetSix := "[[2]]", "[[6]]"
	var packets = make([]string, 0)

	packets = append(packets, packetTwo)
	packets = append(packets, packetSix)

	for fileScanner.Scan() {
		packetLeft := fileScanner.Text()
		fileScanner.Scan()
		packetRight := fileScanner.Text()
		fileScanner.Scan()

		packets = append(packets, packetLeft)
		packets = append(packets, packetRight)
	}

	sortIt(packets)

	return getIndex(packets, packetTwo) * getIndex(packets, packetSix)
}

func Solve13() {
	fmt.Println(first13("input/13_0"))
	fmt.Println(first13("input/13_s"))
	fmt.Println("----------")
	fmt.Println(second13("input/13_0"))
	fmt.Println(second13("input/13_s"))
	fmt.Println("----------")
}
