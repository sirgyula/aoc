// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
)

func first02(filepath string) int {
	var sumPoint int = 0
	var line string = ""
	var m = make(map[string]int)
	m["A X"] = 1 + 3
	m["B X"] = 1 + 0
	m["C X"] = 1 + 6

	m["A Y"] = 2 + 6
	m["B Y"] = 2 + 3
	m["C Y"] = 2 + 0

	m["A Z"] = 3 + 0
	m["B Z"] = 3 + 6
	m["C Z"] = 3 + 3

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		line = fileScanner.Text()

		sumPoint += m[line]
	}

	readFile.Close()

	return sumPoint
}

func second02(filepath string) int {
	var sumPoint int = 0
	var line string = ""
	var m = make(map[string]int)
	m["A X"] = 3 + 0
	m["B X"] = 1 + 0
	m["C X"] = 2 + 0

	m["A Y"] = 1 + 3
	m["B Y"] = 2 + 3
	m["C Y"] = 3 + 3

	m["A Z"] = 2 + 6
	m["B Z"] = 3 + 6
	m["C Z"] = 1 + 6

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		line = fileScanner.Text()

		sumPoint += m[line]
	}

	readFile.Close()

	return sumPoint
}

func Solve02() {
	fmt.Println(first02("input/02_0"))
	fmt.Println(first02("input/02_1"))
	fmt.Println("----------")
	fmt.Println(second02("input/02_0"))
	fmt.Println(second02("input/02_1"))
	fmt.Println("----------")
}
