// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"strconv"
	"strings"
)

var sumSize int = 0
var root *directory = nil
var totalDiskSize int = 70000000
var neededDiskSize int = 30000000
var sizeToDelete int

type directory struct {
	name    string
	parent  *directory
	subdirs map[string]*directory
	files   map[string]int
	size    int
}

func (dir *directory) addSubdir(subdir *directory, name string) {
	if _, ok := dir.subdirs[name]; ok {
		fmt.Println("miért létezik már a mappa?!")
	} else {
		dir.subdirs[name] = subdir
	}
}

func (dir *directory) addFile(name string, size int) {
	if _, ok := dir.files[name]; ok {
		fmt.Println("miért létezik már a fájl?!")
	} else {
		dir.files[name] = size
	}

	dir.size += size
	parent := dir.parent

	for parent != nil {
		parent.size += size
		parent = parent.parent
	}
}

func (dir *directory) findDir(name string) *directory {
	if dir, found := dir.subdirs[name]; found {
		return dir
	} else {
		fmt.Println("hol a mappa, Oszi?!")
		return nil
	}
}

func newDirectory(parent *directory, name string) *directory {
	dir := new(directory)
	dir.name = name
	dir.parent = parent
	dir.subdirs = make(map[string]*directory)
	dir.files = make(map[string]int)
	dir.size = 0

	return dir
}

func getDirSumSizeSmallerThenLimit(dir *directory, dirSizeLimit int) {
	if dir.size < dirSizeLimit {
		sumSize += dir.size
	}

	for _, d := range dir.subdirs {
		getDirSumSizeSmallerThenLimit(d, dirSizeLimit)
	}
}

func getSmallestDirLargerThanDesired(dir *directory, desiredFreeDisk int) {
	if dir.size > desiredFreeDisk && sizeToDelete > dir.size {
		sizeToDelete = dir.size
	}

	for _, d := range dir.subdirs {
		getSmallestDirLargerThanDesired(d, desiredFreeDisk)
	}
}

func buildTree(filepath string) {
	var currentDir *directory

	fileScanner, readFile := util.GetFileScanner(filepath)
	fileScanner.Scan()
	line := fileScanner.Text()

	for {
		preRead := false

		if lsResult := strings.Fields(line); lsResult[1] == "cd" {
			if lsResult[2] == "/" {
				currentDir = root
			} else if lsResult[2] == ".." {
				currentDir = currentDir.parent
			} else {
				currentDir = currentDir.findDir(lsResult[2])
			}
		} else if line == "$ ls" {
			for {
				fileScanner.Scan()
				line = fileScanner.Text()

				if line == "" {
					break
				}

				lsResult := strings.Fields(line)

				if lsResult[0] == "$" {
					preRead = true
					break
				} else if lsResult[0] == "dir" {
					newDir := newDirectory(currentDir, lsResult[1])
					currentDir.addSubdir(newDir, lsResult[1])
				} else if size, err := strconv.Atoi(lsResult[0]); err == nil {
					currentDir.addFile(lsResult[1], size)
				} else {
					fmt.Println("mit hozol má megin, róka?")
				}
			}
		} else {
			fmt.Println("wut?!")
		}

		if line == "" {
			break
		}

		if !preRead {
			fileScanner.Scan()
			line = fileScanner.Text()
		}
	}

	readFile.Close()
}

func first07(filepath string) int {
	sumSize = 0
	root = newDirectory(nil, "/")

	buildTree(filepath)
	getDirSumSizeSmallerThenLimit(root, 100000)

	return sumSize
}

func second07(filepath string) int {
	root = newDirectory(nil, "/")
	sizeToDelete = int(^uint(0) >> 1)

	buildTree(filepath)

	desiredFreeDisk := neededDiskSize - (totalDiskSize - root.size)

	getSmallestDirLargerThanDesired(root, desiredFreeDisk)

	return sizeToDelete
}

func Solve07() {
	fmt.Println(first07("input/07_0"))
	fmt.Println(first07("input/07_s"))
	fmt.Println("----------")
	fmt.Println(second07("input/07_0"))
	fmt.Println(second07("input/07_s"))
	fmt.Println("----------")
}
