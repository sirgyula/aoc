// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
)

type strucc struct {
	x, y, steps int
}

var matrix [][]rune
var start [2]int
var end [2]int
var q []strucc
var rowCnt int
var colCnt int
var explored [][]bool
var possibleStartingPoints [][2]int
var shortest int

func newStrucc(x int, y int, steps int) *strucc {
	s := new(strucc)
	s.x = x
	s.y = y
	s.steps = steps

	return s
}

func parseInput12(filepath string) [][]rune {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	var matrix [][]rune

	row := 0

	for fileScanner.Scan() {
		line := fileScanner.Text()
		var lineA []rune

		for col, runeValue := range line {
			if runeValue == 'S' {
				start = [2]int{row, col}
				possibleStartingPoints = append(possibleStartingPoints, start)
				lineA = append(lineA, 'a')
			} else if runeValue == 'E' {
				end = [2]int{row, col}
				lineA = append(lineA, 'z')
			} else if runeValue == 'a' {
				sp := [2]int{row, col}
				possibleStartingPoints = append(possibleStartingPoints, sp)
				lineA = append(lineA, runeValue)
			} else {
				lineA = append(lineA, runeValue)
			}

		}

		matrix = append(matrix, lineA)
		row++
	}

	return matrix
}

func enqueue(s strucc) {
	q = append(q, s)
}

func dequeue() strucc {
	s := q[0]
	q = q[1:]

	return s
}

func isValidStep(mid strucc, x int, y int) int {
	if x < rowCnt && y < colCnt && 0 <= x && 0 <= y {
		if start[0] == x && start[1] == y {
			return 0 //invalid, start
		} else if matrix[mid.x][mid.y]+1 >= matrix[x][y] {
			if end[0] == x && end[1] == y {
				return 1 //win, end
			}

			return 2 //valid
		} else {
			return 0 //invalid
		}
	}

	return 0
}

func getAdjacents(mid strucc) ([]*strucc, bool) {
	adjacents := make([]*strucc, 0)

	//d
	x, y := mid.x+1, mid.y
	v := isValidStep(mid, x, y)
	if v == 1 {
		return nil, true
	} else if v == 2 {
		adjacents = append(adjacents, newStrucc(x, y, mid.steps))
	}

	//r
	x, y = mid.x, mid.y+1
	v = isValidStep(mid, x, y)
	if v == 1 {
		return nil, true
	} else if v == 2 {
		adjacents = append(adjacents, newStrucc(x, y, mid.steps))
	}

	//u
	x, y = mid.x-1, mid.y
	v = isValidStep(mid, x, y)
	if v == 1 {
		return nil, true
	} else if v == 2 {
		adjacents = append(adjacents, newStrucc(x, y, mid.steps))
	}

	//l
	x, y = mid.x, mid.y-1
	v = isValidStep(mid, x, y)
	if v == 1 {
		return nil, true
	} else if v == 2 {
		adjacents = append(adjacents, newStrucc(x, y, mid.steps))
	}

	return adjacents, false
}

//	1  procedure BFS(G, root) is
//	2      let Q be a queue
//	3      label root as explored
//	4      Q.enqueue(root)
//	5      while Q is not empty do
//	6          v := Q.dequeue()
//	7          if v is the goal then
//	8              return v
//	9          for all edges from v to w in G.adjacentEdges(v) do
//
// 10              if w is not labeled as explored then
// 11                  label w as explored
// 12                  w.parent := v
// 13                  Q.enqueue(w)
func BFS() int {
	q = make([]strucc, 0)

	root := newStrucc(start[0], start[1], 1)
	enqueue(*root)

	for len(q) != 0 {
		v := dequeue()

		adjacents, found := getAdjacents(v)

		if found {
			return v.steps
		}

		for _, w := range adjacents {
			if !explored[w.x][w.y] {
				explored[w.x][w.y] = true
				w.steps++

				if w.steps >= shortest {
					return 0
				}

				enqueue(*w)
			}
		}
	}

	return 0
}

func createExploredMap() {
	explored = make([][]bool, rowCnt)

	for i := 0; i < rowCnt; i++ {
		explored[i] = make([]bool, colCnt)
	}
}

func first12(filepath string) int {
	possibleStartingPoints = make([][2]int, 0)
	matrix = parseInput12(filepath)
	colCnt = len(matrix[0])
	rowCnt = len(matrix)
	shortest = 1000000

	createExploredMap()

	res := BFS()

	return res
}

func second12(filepath string) int {
	possibleStartingPoints = make([][2]int, 0)
	matrix = parseInput12(filepath)
	colCnt = len(matrix[0])
	rowCnt = len(matrix)

	shortest = 1000000

	for len(possibleStartingPoints) > 0 {
		start = possibleStartingPoints[0]
		possibleStartingPoints = possibleStartingPoints[1:]

		createExploredMap()
		res := BFS()

		if res != 0 && res < shortest {
			shortest = res
		}
	}

	return shortest
}

func Solve12() {
	fmt.Println(first12("input/12_0"))
	fmt.Println(first12("input/12_s"))
	fmt.Println("----------")
	fmt.Println(second12("input/12_0"))
	fmt.Println(second12("input/12_s"))
	fmt.Println("----------")
}
