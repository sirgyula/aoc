// You can edit this code!
// Click here and start typing.
package d14_2

import (
	"aoc2022/util"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var occupiedCoords map[string]rune
var lowestY int = -1
var floor int = -1

const sandStartX, sandStartY = 500, 0
const rock = 'R'
const sand = 's'

func buildCoordString(x int, y int) string {
	return strconv.Itoa(x) + "," + strconv.Itoa(y)
}

func breakCoordString(coord *string) (int, int) {
	splitted := strings.Split(*coord, ",")

	x, err := strconv.Atoi(splitted[0])

	if err != nil {
		panic(err)
	}

	y, err := strconv.Atoi(splitted[1])

	if err != nil {
		panic(err)
	}

	return x, y
}

func addRocksHorizontal(x int, srcY int, dstY int) {
	if srcY < dstY {
		for y := srcY; y <= dstY; y++ {
			occupiedCoords[buildCoordString(x, y)] = rock

			if y > lowestY {
				lowestY = y
			}
		}
	} else {
		for y := dstY; y <= srcY; y++ {
			occupiedCoords[buildCoordString(x, y)] = rock

			if y > lowestY {
				lowestY = y
			}
		}
	}
}

func addRocksVertical(srcX int, dstX int, y int) {
	if y > lowestY {
		lowestY = y
	}

	if srcX < dstX {
		for i := srcX; i <= dstX; i++ {
			occupiedCoords[buildCoordString(i, y)] = rock
		}
	} else {
		for i := dstX; i <= srcX; i++ {
			occupiedCoords[buildCoordString(i, y)] = rock
		}
	}
}

func addRocksBetween(src *string, dst *string) {
	srcX, srcY := breakCoordString(src)
	dstX, dstY := breakCoordString(dst)

	if srcX == dstX {
		addRocksHorizontal(srcX, srcY, dstY)
	} else {
		addRocksVertical(srcX, dstX, dstY)
	}
}

func parseRocks(filepath *string) {
	fileScanner, readFile := util.GetFileScanner(*filepath)
	defer readFile.Close()

	var rgx = regexp.MustCompile(`(\d+,\d+)`)

	for fileScanner.Scan() {
		coords := rgx.FindAllString(fileScanner.Text(), -1)

		for i, coord := range coords {
			if i == len(coords)-1 {
				break
			}

			addRocksBetween(&coord, &coords[i+1])
		}
	}
}

func checkVictoryCondition(x int, y int, stays bool) bool {
	return stays && x == sandStartX && y == sandStartY
}

func isOccupied(x int, y int) bool {
	_, ok := occupiedCoords[buildCoordString(x, y)]

	return ok
}

func getNextCoord(x int, y int) (int, int, bool) {
	if y+1 == floor {
		return 0, 0, true
	} else if !isOccupied(x, y+1) { // down
		return x, y + 1, false
	} else if !isOccupied(x-1, y+1) { // down-left
		return x - 1, y + 1, false
	} else if !isOccupied(x+1, y+1) { // down-right
		return x + 1, y + 1, false
	} else { // stays
		return 0, 0, true
	}
}

func second14(filepath string) int {
	defer util.PrintElaspedTime(time.Now())
	occupiedCoords = make(map[string]rune)
	sandCnt := 0
	currentX, currentY := sandStartX, sandStartY

	parseRocks(&filepath)

	floor = lowestY + 2
 
	for {
		newX, newY, stays := getNextCoord(currentX, currentY)

		if checkVictoryCondition(currentX, currentY, stays) {
			sandCnt++
			break
		} else if stays {
			sandCnt++
			occupiedCoords[buildCoordString(currentX, currentY)] = sand
			currentX, currentY = sandStartX, sandStartY
		} else {
			currentX, currentY = newX, newY
		}
	}

	return sandCnt
}

func Solve14() {
	fmt.Println(second14("input/14_0"))
	fmt.Println(second14("input/14_s"))
}
