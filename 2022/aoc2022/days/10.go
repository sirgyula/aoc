// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"strconv"
	"strings"
)

func first10(filepath string) int {
	sumStrength, x, currentCycle, cycleCheck, cycleIncrement, cycleLimit := 0, 1, 0, 20, 40, 220

	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	for fileScanner.Scan() {
		command := strings.Fields(fileScanner.Text())

		if command[0] == "noop" {
			currentCycle++

			if currentCycle == cycleCheck {
				sumStrength += (x * currentCycle)
				cycleCheck += cycleIncrement
			}
		} else if command[0] == "addx" {
			for i := 0; i < 2; i++ {
				currentCycle++

				if currentCycle == cycleCheck {
					sumStrength += (x * currentCycle)
					cycleCheck += cycleIncrement
				}
			}

			increment, _ := strconv.Atoi(command[1])
			x += increment
		} else {
			panic("uknó kommand")
		}

		if currentCycle >= cycleLimit {
			break
		}
	}

	return sumStrength
}

var x, currentCycle, cycleCheck int

func doCycle() {
	if x-1 <= currentCycle && currentCycle <= x+1 {
		fmt.Print("#")
	} else {
		fmt.Print(".")
	}

	currentCycle++

	if currentCycle == cycleCheck {
		fmt.Println()
		currentCycle = 0
	}
}

func second10(filepath string) {
	x, currentCycle, cycleCheck = 1, 0, 40
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	for fileScanner.Scan() {
		command := strings.Fields(fileScanner.Text())

		if command[0] == "noop" {
			doCycle()
		} else if command[0] == "addx" {
			for i := 0; i < 2; i++ {
				doCycle()
			}

			increment, _ := strconv.Atoi(command[1])
			x += increment
		} else {
			panic("uknó kommand")
		}
	}
}

func Solve10() {
	fmt.Println(first10("input/10_0"))
	fmt.Println(first10("input/10_s"))
	fmt.Println("----------")
	second10("input/10_0")
	fmt.Println("----------")
	second10("input/10_s")
	fmt.Println("----------")
}
