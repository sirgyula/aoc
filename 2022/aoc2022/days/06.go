// You can edit this code!
// Click here and start typing.
package days

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func checkUnique(s string) bool {
	allKeys := make(map[rune]bool)

	for _, item := range s {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
		}
	}

	return len(allKeys) == len(s)
}

func find(filepath string, chars int) int {
	var s string = ""

	filebuffer, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	inputdata := string(filebuffer)
	data := bufio.NewScanner(strings.NewReader(inputdata))
	data.Split(bufio.ScanRunes)

	for i := 0; i < chars; i++ {
		data.Scan()
		s += data.Text()
	}

	sop := chars

	if checkUnique(s) {
		return sop
	}

	for data.Scan() {
		s = s[1:]
		s += data.Text()
		sop++

		if checkUnique(s) {
			break
		}
	}

	return sop
}

func first06(filepath string) int {
	return find(filepath, 4)
}

func second06(filepath string) int {
	return find(filepath, 14)
}

func Solve06() {
	fmt.Println(first06("input/06_0"))
	fmt.Println(first06("input/06_1"))
	fmt.Println(first06("input/06_2"))
	fmt.Println(first06("input/06_3"))
	fmt.Println(first06("input/06_4"))
	fmt.Println(first06("input/06_s"))
	fmt.Println("----------")
	fmt.Println(second06("input/06_0"))
	fmt.Println(second06("input/06_1"))
	fmt.Println(second06("input/06_2"))
	fmt.Println(second06("input/06_3"))
	fmt.Println(second06("input/06_4"))
	fmt.Println(second06("input/06_s"))
	fmt.Println("----------")
}
