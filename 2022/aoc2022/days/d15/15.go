// You can edit this code!
// Click here and start typing.
package d15

import (
	"aoc2022/util"
	"fmt"
	"image"
	"math"
	"regexp"
	"strconv"
)

type Sensor struct {
	Coord     image.Point
	Md        int
	Perimiter []image.Point
}

var rgx = regexp.MustCompile(`(-?\d+)`)

func calcManhattanDistance(a *image.Point, b *image.Point) int {
	return int(math.Abs(float64(b.X)-float64(a.X))) + int(math.Abs(float64(b.Y)-float64(a.Y)))
}

func parseSensorAndBeaconCoords(line string) (*image.Point, *image.Point) {
	coords := rgx.FindAllString(line, -1)
	sensor, beacon := image.Point{0, 0}, image.Point{0, 0}

	val, _ := strconv.Atoi(coords[0])
	sensor.X = val
	val, _ = strconv.Atoi(coords[1])
	sensor.Y = val

	val, _ = strconv.Atoi(coords[2])
	beacon.X = val
	val, _ = strconv.Atoi(coords[3])
	beacon.Y = val

	return &sensor, &beacon
}

func isBetweenAndBelow(y int, md int, checkedYCoord int) bool {
	return !(checkedYCoord < y || y+md < checkedYCoord)
}

func isBetweenAndAbove(y int, md int, checkedYCoord int) bool {
	return !(checkedYCoord > y || y-md > checkedYCoord)
}

func isInPlayArea(p *image.Point, maxXAndY int) bool {
	return p.X >= 0 && p.X <= maxXAndY && p.Y >= 0 && p.Y <= maxXAndY
}

func getPerimiterLine(limit int, p image.Point, perimiter *[]image.Point, offsetPoint image.Point, stepping image.Point, maxXAndY int) {
	for i := 0; i < limit; i++ {
		newPoint := p.Add(offsetPoint)
		offsetPoint = offsetPoint.Add(stepping)

		if isInPlayArea(&newPoint, maxXAndY) {
			*perimiter = append(*perimiter, newPoint)
		}
	}
}

func getPerimiterOfSensor(p *image.Point, md int, maxXAndY int) []image.Point {
	perimiter := make([]image.Point, 0)

	getPerimiterLine(md+1, p.Add(image.Point{0, -md}), &perimiter, image.Point{0, -1}, image.Point{1, 1}, maxXAndY)
	getPerimiterLine(md+1, p.Add(image.Point{md, 0}), &perimiter, image.Point{1, 0}, image.Point{-1, 1}, maxXAndY)
	getPerimiterLine(md+1, p.Add(image.Point{0, md}), &perimiter, image.Point{0, 1}, image.Point{-1, -1}, maxXAndY)
	getPerimiterLine(md+1, p.Add(image.Point{-md, 0}), &perimiter, image.Point{-1, 0}, image.Point{1, -1}, maxXAndY)

	return perimiter
}

func first(filepath string, checkedYCoord int) int {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	resultXPositions := make(map[int]interface{})
	occupiedXCoords := make(map[int]interface{}, 0) // on the checkedYCoord, cannot use these coords in the result

	for fileScanner.Scan() {
		sensor, beacon := parseSensorAndBeaconCoords(fileScanner.Text())
		md := calcManhattanDistance(sensor, beacon)

		if sensor.Y == checkedYCoord {
			occupiedXCoords[sensor.X] = nil
		}

		if beacon.Y == checkedYCoord {
			occupiedXCoords[beacon.X] = nil
		}

		distanceToCheckedYCoord := -1

		// leg A of a triangle
		if isBetweenAndBelow(sensor.Y, md, checkedYCoord) {
			distanceToCheckedYCoord = checkedYCoord - sensor.Y
		} else if isBetweenAndAbove(sensor.Y, md, checkedYCoord) {
			distanceToCheckedYCoord = sensor.Y - checkedYCoord
		} else if sensor.Y == checkedYCoord {
			distanceToCheckedYCoord = 0
		}

		if distanceToCheckedYCoord != -1 {
			//leg B of a triangle
			remainingDistance := md - distanceToCheckedYCoord

			for i := 0; i <= remainingDistance; i++ {
				resultXPositions[sensor.X+i] = nil
				resultXPositions[sensor.X-i] = nil
			}
		}
	}

	// removing surely (beacon or sensor) occupied x coords
	for k := range occupiedXCoords {
		delete(resultXPositions, k)
	}

	return len(resultXPositions)
}

func second(filepath string, maxXAndY int, xMultiplier int) int {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	sensors := make([]Sensor, 15)

	for fileScanner.Scan() {
		sensor, beacon := parseSensorAndBeaconCoords(fileScanner.Text())
		md := calcManhattanDistance(sensor, beacon)
		perimiter := getPerimiterOfSensor(sensor, md, maxXAndY)

		sensors = append(sensors, Sensor{*sensor, md, perimiter})
	}

	for sensorIndex, sensor := range sensors {
		for _, perimiterCoord := range sensor.Perimiter {
			isInside := false

			for checkingSensorIndex, checkingSensor := range sensors {
				if sensorIndex == checkingSensorIndex {
					continue
				}

				// if the manhattan distance of the perimiter smaller to the sensor coordinate,
				// than it's inside it's original beacon distance
				md := calcManhattanDistance(&perimiterCoord, &checkingSensor.Coord)

				isInside = isInside || md <= checkingSensor.Md

				// the perimiter point is inside one of the sensor's area, no need to check tis perimiter further
				if isInside {
					break
				}
			}

			if !isInside {
				return perimiterCoord.X*xMultiplier + perimiterCoord.Y
			}
		}
	}

	return -1
}

func Solve() {
	fmt.Println(first("input/15_0", 10))
	fmt.Println(first("input/15_s", 2000000))
	fmt.Println("----------")
	fmt.Println(second("input/15_0", 20, 4000000))
	fmt.Println(second("input/15_s", 4000000, 4000000))
}
