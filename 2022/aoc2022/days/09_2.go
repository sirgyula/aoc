// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"math"
	"strconv"
	"strings"
)

type coordinate struct {
	x, y int
}

func newCoordinate(x int, y int) *coordinate {
	c := new(coordinate)
	c.x = x
	c.y = y

	return c
}

var visited2 = make(map[string]struct{})

func updateVisited2(c coordinate) {
	visited2[strconv.Itoa(c.x)+","+strconv.Itoa(c.y)] = struct{}{}
}

func moveCoord[T rune | string](c *coordinate, direction T) {
	switch string(direction) {
	case "L":
		c.x--
	case "U":
		c.y++
	case "R":
		c.x++
	case "D":
		c.y--
	default:
		fmt.Println("moving to 3d space captain!")
	}
}

func isNegative(n int) bool {
	return n < 0
}

func isDL(c *coordinate) bool {
	return isNegative(c.x) && isNegative(c.y)
}

func isUR(c *coordinate) bool {
	return !isNegative(c.x) && !isNegative(c.y)
}

func isUL(c *coordinate) bool {
	return isNegative(c.x) && !isNegative(c.y)
}

func isDR(c *coordinate) bool {
	return !isNegative(c.x) && isNegative(c.y)
}

func detRel(c1 *coordinate, c2 *coordinate) string {
	cd := newCoordinate(c1.x-c2.x, c1.y-c2.y) //coord diff

	if math.Abs(float64(cd.x)) < 2 && math.Abs(float64(cd.y)) < 2 {
		return "-" // covers, or neighbours
	} else if cd.x == 0 { // same col
		if cd.y == 2 {
			return "U"
		} else {
			return "D"
		}
	} else if cd.y == 0 { // same row
		if cd.x == 2 {
			return "R"
		} else {
			return "L"
		}
	} else if isDL(cd) { // upper right corner
		return "DL"
	} else if isDR(cd) { // upper left corner
		return "DR"
	} else if isUL(cd) { // lower right corner
		return "UL"
	} else if isUR(cd) { // lower left corner
		return "UR"
	} else {
		fmt.Println("creating black hole in 3...")
		return "X"
	}
}

func second09_2(filepath string) int {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	knots := make([]coordinate, 10)

	visited2 = make(map[string]struct{})

	updateVisited2(knots[9])

	for fileScanner.Scan() {
		line := fileScanner.Text()

		lineParts := strings.Fields(line)

		move, _ := strconv.Atoi(lineParts[1])
		direction := lineParts[0]

		for m := 0; m < move; m++ {
			moveCoord(&knots[0], direction)

			for i := 1; i < len(knots); i++ {
				rel := detRel(&knots[i-1], &knots[i])

				if rel == "-" { //no movement, following won't move either
					break
				} else { //moveit moveit
					for _, r := range rel {
						moveCoord(&knots[i], r)
					}
				}

				if i == len(knots)-1 {
					updateVisited2(knots[9])
				}
			}
		}
	}

	return len(visited2)
}

func Solve09_2() {
	fmt.Println(second09_2("input/09_1"))
	fmt.Println(second09_2("input/09_s"))
	fmt.Println("----------")
}
