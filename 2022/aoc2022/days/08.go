// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"strconv"
)

func getMap(filepath string) [][]int {
	var mmap [][]int
	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		row := make([]int, 0)

		for _, r := range line {
			row = append(row, int(r-'0'))
		}

		mmap = append(mmap, row)
	}

	readFile.Close()

	return mmap
}

func first08(filepath string) int {
	hash := make(map[string]struct{})

	mmap := getMap(filepath)

	rowCnt := len(mmap)
	colCnt := len(mmap[0])

	max := -1

	for r := 1; r < rowCnt-1; r++ {
		for c := 0; c < colCnt-1; c++ {
			if mmap[r][c] > max {
				max = mmap[r][c]
				hash[strconv.Itoa(r)+","+strconv.Itoa(c)] = struct{}{}
			}
		}

		max = -1

		for c := colCnt - 1; c > 0; c-- {
			if mmap[r][c] > max {
				max = mmap[r][c]
				hash[strconv.Itoa(r)+","+strconv.Itoa(c)] = struct{}{}
			}
		}

		max = -1
	}

	max = -1

	for c := 1; c < colCnt-1; c++ {
		for r := 0; r < rowCnt-1; r++ {
			if mmap[r][c] > max {
				max = mmap[r][c]
				hash[strconv.Itoa(r)+","+strconv.Itoa(c)] = struct{}{}
			}
		}

		max = -1

		for r := rowCnt - 1; r > 0; r-- {
			if mmap[r][c] > max {
				max = mmap[r][c]
				hash[strconv.Itoa(r)+","+strconv.Itoa(c)] = struct{}{}
			}
		}

		max = -1
	}

	return len(hash) + 4 //csak a sarkokat kell hozzáadni, több a max = -1  miatt megvan
}

func second08(filepath string) int {
	mmap := getMap(filepath)
	maxScore := -1

	rowCnt := len(mmap)
	colCnt := len(mmap[0])

	for r := 1; r < rowCnt-1; r++ {
		for c := 2; c < colCnt-1; c++ {
			var u, d, l, ri int = 0, 0, 0, 0
			rr := r
			cc := c

			for rr > 0 {
				u++

				if mmap[rr-1][c] < mmap[r][c] {
					rr--
				} else {
					break
				}
			}

			rr = r

			for rr < rowCnt-1 {
				d++

				if mmap[rr+1][c] < mmap[r][c] {
					rr++
				} else {
					break
				}
			}

			for cc > 0 {
				l++

				if mmap[r][cc-1] < mmap[r][c] {
					cc--
				} else {
					break
				}
			}

			cc = c

			for cc < colCnt-1 {
				ri++

				if mmap[r][cc+1] < mmap[r][c] {
					cc++
				} else {
					break
				}
			}

			score := u * d * l * ri

			if maxScore < score {
				maxScore = score
			}
		}
	}

	return maxScore
}

func Solve08() {
	fmt.Println(first08("input/08_0"))
	fmt.Println(first08("input/08_s"))
	fmt.Println("----------")
	fmt.Println(second08("input/08_0"))
	fmt.Println(second08("input/08_s"))
	fmt.Println("----------")
}
