// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/crsmithdev/goexpr"
)

func parseInput(filepath string, worryFixer *int) []*util.Monkey {
	fileScanner, readFile := util.GetFileScanner(filepath)
	defer readFile.Close()

	var monkeys []*util.Monkey
	var currentMonkey *util.Monkey

	var regexpStarting = regexp.MustCompile(`(\d+)+`)
	var regexpSingleNum = regexp.MustCompile(`(\d+)`)

	for fileScanner.Scan() {
		line := strings.TrimLeft(fileScanner.Text(), " ")

		if strings.HasPrefix(line, "Monkey") {
			currentMonkey = util.NewMonkey()
		} else if strings.HasPrefix(line, "Starting") {
			items := regexpStarting.FindAllString(line, -1)
			currentMonkey.SetItems(items)
		} else if strings.HasPrefix(line, "Operation") {
			currentMonkey.Operation = strings.Replace(line, "Operation: new = ", "", 1)
		} else if strings.HasPrefix(line, "Test") {
			testVal, _ := strconv.Atoi(regexpSingleNum.FindAllString(line, -1)[0])
			currentMonkey.TestDivVal = testVal
			*worryFixer = *worryFixer * testVal
		} else if strings.HasPrefix(line, "If true") {
			throwTo, _ := strconv.Atoi(regexpSingleNum.FindAllString(line, -1)[0])
			currentMonkey.DstMonkeyTrue = throwTo
		} else if strings.HasPrefix(line, "If false") {
			throwTo, _ := strconv.Atoi(regexpSingleNum.FindAllString(line, -1)[0])
			currentMonkey.DstMonkeyFalse = throwTo
		} else if len(line) == 0 {
			monkeys = append(monkeys, currentMonkey)
		} else {
			panic("unknown line")
		}
	}

	monkeys = append(monkeys, currentMonkey) //add final monkey

	return monkeys
}

func getNewWorryLevel(wl *int, operation string) {
	parsed, _ := goexpr.Parse(operation)
	result, _ := goexpr.Evaluate(parsed, map[string]float64{
		"old": float64(*wl),
	})

	*wl = int(result)
}

func handleRelief(wl *int) {
	*wl = *wl / 3
}

func throwTest(wl int, divVal int) bool {
	return wl%divVal == 0
}

func doIt(filepath string, maxRounds int, hasRelief bool) int {
	/*The key to this is a modular arithmetic concept. First, you need to multiply all the 
		monkeys' mod values together. Let's call this M. Some say find the LCM but they're small
		enough you can get away with just the product. Now, after every operation you do to an
		item worry level, after doing the computation, mod that value with M. If you're adding,
		add then take that result and mod with M. If you're multiplying, multiply and then
		mod with M. This will keep your worry values small. Everything else is the same. 
		We are exploiting the property that for all integers k: (a mod km) mod m = a mod m
	*/
	worryFixer := 1
	monkeys := parseInput(filepath, &worryFixer)

	for rounds := 0; rounds < maxRounds; rounds++ {
		for _, monkey := range monkeys {
			for {
				worryLevel, err := monkey.GetFirstItem()

				if err != nil {
					break //no more item
				}

				getNewWorryLevel(&worryLevel, monkey.Operation)

				if hasRelief {
					handleRelief(&worryLevel)
				}

				testResult := throwTest(worryLevel, monkey.TestDivVal)

				if testResult {
					monkeys[monkey.DstMonkeyTrue].AddItem(worryLevel, worryFixer)
				} else {
					monkeys[monkey.DstMonkeyFalse].AddItem(worryLevel, worryFixer)
				}
			}
		}
	}

	monkeyBusinesses := make([]int, 0)

	for _, monkey := range monkeys {
		monkeyBusinesses = append(monkeyBusinesses, monkey.InspectCount)
	}

	sort.Slice(monkeyBusinesses, func(i, j int) bool { return monkeyBusinesses[i] < monkeyBusinesses[j] })

	return monkeyBusinesses[len(monkeyBusinesses)-1] * monkeyBusinesses[len(monkeyBusinesses)-2]
}

func first11(filepath string) int {
	return doIt(filepath, 20, true)
}

func second11(filepath string) int {
	return doIt(filepath, 10000, false)
}

func Solve11() {
	fmt.Println(first11("input/11_0"))
	fmt.Println(first11("input/11_s"))
	fmt.Println("----------")
	fmt.Println(second11("input/11_0"))
	fmt.Println(second11("input/11_s"))
	fmt.Println("----------")
}
