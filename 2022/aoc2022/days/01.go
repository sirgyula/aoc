// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func addSortClear(topCals *[]int, currentSumCal int) {
	if currentSumCal > (*topCals)[1] || currentSumCal > (*topCals)[2] || currentSumCal > (*topCals)[3] {
		(*topCals)[0] = currentSumCal
		sort.Ints((*topCals))

		(*topCals)[0] = 0
	}
}

func first01(filepath string) int {
	var maxCal, currentSumCal int = 0, 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()

		if len(line) == 0 {
			if currentSumCal > maxCal {
				maxCal = currentSumCal
			}

			currentSumCal = 0
		} else {
			currentCal, err := strconv.Atoi(line)
			util.MyPanic(err)

			currentSumCal += currentCal
		}
	}

	readFile.Close()

	return maxCal
}

func second01(filepath string) int {
	var currentSumCal int = 0
	var topCals = []int{0, 0, 0, 0}

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()

		if len(line) == 0 {
			addSortClear(&topCals, currentSumCal)

			currentSumCal = 0
		} else {
			currentCal, err := strconv.Atoi(line)
			util.MyPanic(err)

			currentSumCal += currentCal
		}
	}

	// last line must be handled
	addSortClear(&topCals, currentSumCal)

	readFile.Close()

	return topCals[1] + topCals[2] + topCals[3]
}

func Solve01() {
	fmt.Println(first01("input/01_0"))
	fmt.Println(first01("input/01_1"))
	fmt.Println("----------")
	fmt.Println(second01("input/01_0"))
	fmt.Println(second01("input/01_1"))
	fmt.Println("----------")
}
