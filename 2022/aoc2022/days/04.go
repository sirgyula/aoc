// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
	"regexp"
	"strconv"
)

func first04(filepath string) int {
	var sumOverlap int = 0
	var rgx = regexp.MustCompile(`^(\d+)-(\d+),(\d+)-(\d+)$`)
	var s = make([]int, 5)

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		for i, v := range rgx.FindStringSubmatch(fileScanner.Text()) {
			if i == 0 {
				continue
			}

			conv, err := strconv.Atoi(v)
			util.MyPanic(err)

			s[i] = conv
		}

		if ((s[1] <= s[3] && s[3] <= s[2]) && (s[2] >= s[4] && s[4] >= s[1])) ||
			((s[3] <= s[1] && s[1] <= s[4]) && (s[4] >= s[2] && s[2] >= s[3])) {
			sumOverlap++
		}
	}

	readFile.Close()

	return sumOverlap
}

func second04(filepath string) int {
	var sumOverlap int = 0
	var rgx = regexp.MustCompile(`^(\d+)-(\d+),(\d+)-(\d+)$`)
	var s = make([]int, 5)

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		for i, v := range rgx.FindStringSubmatch(fileScanner.Text()) {
			if i == 0 {
				continue
			}

			conv, err := strconv.Atoi(v)
			util.MyPanic(err)

			s[i] = conv
		}

		if ((s[1] <= s[3] && s[3] <= s[2]) || (s[2] >= s[4] && s[4] >= s[1])) ||
			((s[3] <= s[1] && s[1] <= s[4]) || (s[4] >= s[2] && s[2] >= s[3])) {
			sumOverlap++
		}
	}

	readFile.Close()

	return sumOverlap
}

func Solve04() {
	fmt.Println(first04("input/04_0"))
	fmt.Println(first04("input/04_1"))
	fmt.Println("----------")
	fmt.Println(second04("input/04_0"))
	fmt.Println(second04("input/04_1"))
	fmt.Println("----------")
}
