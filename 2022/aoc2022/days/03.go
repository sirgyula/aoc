// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"fmt"
)

func hashGeneric[T comparable](a []T, b []T) []T {
	set := make([]T, 0)
	hash := make(map[T]struct{})

	for _, v := range a {
		hash[v] = struct{}{}
	}

	for _, v := range b {
		if _, ok := hash[v]; ok {
			set = append(set, v)
		}
	}

	return set
}

func hashGenericTrio[T comparable](a []T, b []T, c []T) []T {
	set := make([]T, 0)
	hashA := make(map[T]struct{})
	hashB := make(map[T]struct{})

	for _, v := range a {
		hashA[v] = struct{}{}
	}

	for _, v := range b {
		hashB[v] = struct{}{}
	}

	for _, v := range c {
		_, okA := hashA[v]
		_, okB := hashB[v]

		if okA && okB {
			set = append(set, v)
		}
	}

	return set
}

func getPrio(r rune) int {
	val := int(r)

	if val < 94 {
		return val - 38
	} else {
		return val - 96
	}
}

func removeDuplicate[T string | int | rune](sliceList []T) []T {
	allKeys := make(map[T]bool)
	list := []T{}
	for _, item := range sliceList {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func first03(filepath string) int {
	var sumPrio int = 0
	var line string = ""

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		line = fileScanner.Text()
		compSize := len(line) / 2

		intersection := hashGeneric([]rune(line[0:compSize]), []rune(line[compSize:]))

		dedup := removeDuplicate(intersection)

		for _, v := range dedup {
			sumPrio += getPrio(v)
		}
	}

	readFile.Close()

	return sumPrio
}

func second03(filepath string) int {
	var sumPrio int = 0
	var line1, line2, line3 string = "", "", ""

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		line1 = fileScanner.Text()
		fileScanner.Scan()
		line2 = fileScanner.Text()
		fileScanner.Scan()
		line3 = fileScanner.Text()

		intersection := hashGenericTrio([]rune(line1), []rune(line2), []rune(line3))

		dedup := removeDuplicate(intersection)

		for _, v := range dedup {
			sumPrio += getPrio(v)
		}
	}

	readFile.Close()

	return sumPrio
}

func Solve03() {
	fmt.Println(first03("input/03_0"))
	fmt.Println(first03("input/03_1"))
	fmt.Println("----------")
	fmt.Println(second03("input/03_0"))
	fmt.Println(second03("input/03_1"))
	fmt.Println("----------")
}
