// You can edit this code!
// Click here and start typing.
package days

import (
	"aoc2022/util"
	"bytes"
	"fmt"
	"regexp"
	"strconv"
)

type Stack []string

var instuctions [][3]int
var stacks []Stack
var stackCount int

func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *Stack) Push(str string) {
	*s = append(*s, str)
}

func (s *Stack) Pop() (string, bool) {
	if s.IsEmpty() {
		return "", false
	} else {
		lastIndex := len(*s) - 1
		element := (*s)[lastIndex]
		*s = (*s)[:lastIndex]

		return element, true
	}
}

func parseStackLines(stackLines *[]string) {
	for i := len(*stackLines) - 1; i >= 0; i-- {
		current := (*stackLines)[i]

		stackIdPos := 1

		for stackId := 1; stackId <= stackCount; stackId++ {
			crateId := string(current[stackIdPos])

			if crateId != " " {
				stacks[stackId].Push(crateId)
			}

			stackIdPos += 4
		}
	}
}

func readAndParseInput(filepath string) {
	var stackLines = make([]string, 0)

	var stck = regexp.MustCompile(`^[ \[\]A-Z]*$`)
	var num = regexp.MustCompile(`^[0-9 ]+$`)
	var instr = regexp.MustCompile(`^move (\d+) from (\d) to (\d)$`)

	fileScanner, readFile := util.GetFileScanner(filepath)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		lineB := []byte(line)

		if len(line) == 0 {
			continue //to not match on empty line
		}
		if stck.Match(lineB) {
			stackLines = append(stackLines, line)
		} else if num.Match(lineB) {
			stackCount, _ = strconv.Atoi(string(line[len(line)-2]))

			i := 0
			stacks = append(stacks, Stack{}) //to match array index with stackId

			for i < stackCount {
				stacks = append(stacks, Stack{})
				i++
			}
		} else if instr.Match(lineB) {
			regexresult := instr.FindStringSubmatch(fileScanner.Text())

			var instruction [3]int

			instruction[0], _ = strconv.Atoi(regexresult[1])
			instruction[1], _ = strconv.Atoi(regexresult[2])
			instruction[2], _ = strconv.Atoi(regexresult[3])

			instuctions = append(instuctions, instruction)
		} else {
			fmt.Println("wut?!")
		}
	}

	readFile.Close()

	parseStackLines(&stackLines)
}

func executeInstructionFor9000(instruction *[3]int) {
	var times int = instruction[0]
	var src int = instruction[1]
	var dest int = instruction[2]

	for i := 0; i < times; i++ {
		crate, found := stacks[src].Pop()

		if found {
			stacks[dest].Push(crate)
		} else {
			fmt.Println("why?")
		}
	}
}

func executeInstructionFor9001(instruction *[3]int) {
	var times int = instruction[0]
	var src int = instruction[1]
	var dest int = instruction[2]

	var movingB bytes.Buffer

	for i := 0; i < times; i++ {
		crate, found := stacks[src].Pop()

		if found {
			movingB.WriteString(crate)
		} else {
			fmt.Println("why?")
		}
	}

	moving := movingB.String()

	for i := len(moving) - 1; i >= 0; i-- {
		stacks[dest].Push(string(moving[i]))
	}
}

func initGlobals() {
	instuctions = nil
	stacks = nil
	stackCount = 0
}

func first05(filepath string) string {
	initGlobals()

	readAndParseInput(filepath)

	for _, instruction := range instuctions {
		executeInstructionFor9000(&instruction)
	}

	var result bytes.Buffer

	for _, stack := range stacks {
		crate, _ := stack.Pop()
		result.WriteString(crate)
	}

	return result.String()
}

func second05(filepath string) string {
	initGlobals()

	readAndParseInput(filepath)

	for _, instruction := range instuctions {
		executeInstructionFor9001(&instruction)
	}

	var result bytes.Buffer

	for _, stack := range stacks {
		crate, _ := stack.Pop()
		result.WriteString(crate)
	}

	return result.String()
}

func Solve05() {
	fmt.Println(first05("input/05_0"))
	fmt.Println(first05("input/05_1"))
	fmt.Println("----------")
	fmt.Println(second05("input/05_0"))
	fmt.Println(second05("input/05_1"))
	fmt.Println("----------")
}
