module day1

export day1_1, day1_2

    function day1_1()
        solution = 0

        open("./input/day1.txt") do file
            @time begin
                for ln in eachline(file)
                    solution += calculate(parse(Int, ln))
                end
            end
        end

        println(solution)
    end

    function day1_2()
        solution = 0

        open("./input/day1.txt") do file
            @time begin
                for ln in eachline(file)
                    current = calculate(parse(Int, ln))
                    solution += current

                    while true
                        current = calculate(current)

                        if (current > 0)
                            solution += current
                        else
                            break
                        end
                    end
                end
            end
        end

        println(solution)
    end

    function calculate(num::Int)
        return trunc(Int, Base.floor(num / 3) - 2)
    end
end
