module day3

export day3_1, day3_2

    struct Point
        x::Float64
        y::Float64
    end

    Base.:(==)(o1::Point, o2::Point) = o1.x == o2.x && o1.y == o2.y
    Base.:(!=)(o1::Point, o2::Point) = o1.x != o2.x || o1.y != o2.y

    struct MovementExpr
        direction::String
        value::Int64
    end

    global infPoint = Point(9999999999999, 9999999999999)
    global origoPoint = Point(0, 0)
    global minManhattan = 999999999999

    function day3_1()
        global infPoint
        global origoPoint
        global minManhattan

        open("./input/day3.txt") do file
            @time begin
                wires = []

                for ln in eachline(file)
                    movements = split(ln, ",")
                    points = Point[]

                    currentPoint = origoPoint
                    push!(points, currentPoint)

                    for movement in movements
                        currentPoint = getNextPoint(currentPoint, parseMovement(movement))
                        push!(points, currentPoint)
                    end

                    push!(wires, points)
                end

                i = 1
                j = 1

                k = 0

                while i < size(wires[1])[1]
                    pointA = wires[1][i]
                    pointB = wires[1][i + 1]

                    while j < size(wires[2])[1]
                        pointC = wires[2][j]
                        pointD = wires[2][j + 1]

                        resultPoint = lineSegementsIntersect(pointA, pointB, pointC, pointD)

                        k = k + 1

                        if resultPoint != infPoint
                            currentManhattan = manhattan(resultPoint)

#                             println(resultPoint)
#                             println(currentManhattan)
#                             println("--------")

                            if currentManhattan < minManhattan && currentManhattan != 0
                                minManhattan = currentManhattan
                            end
                        end

                        j = j + 1
                    end

                    i = i + 1
                    j = 1
                end

                println(k)
            end
        end

        println(minManhattan)
    end

    function day3_2()

    end

    function getNextPoint(currentPoint::Point, movement::MovementExpr)
        newPoint = Point(1, 1)

        if movement.direction == "L"
            newPoint = Point(currentPoint.x - movement.value, currentPoint.y)
        elseif movement.direction == "U"
            newPoint = Point(currentPoint.x, currentPoint.y + movement.value)
        elseif movement.direction == "R"
            newPoint = Point(currentPoint.x + movement.value, currentPoint.y)
        elseif movement.direction == "D"
            newPoint = Point(currentPoint.x, currentPoint.y - movement.value)
        else
            println(string("lolwut!?" , movement))
        end

        return newPoint
    end

    function parseMovement(movement)
        return MovementExpr(movement[1:1], parse(Int, movement[2:length(movement)]))
    end

    function sub(v, w)
        return Point(v.x - w.x, v.y - w.y)
    end

    function add(v, w)
        return Point(v.x + w.x, v.y + w.y)
    end

    function mult(v, w)
        return v.x * w.x + v.y * w.y
    end

    function mulWithNumSecond(v, mult::Float64)
        return Point(v.x * mult, v.y * mult)
    end

    function mulWithNum(mult::Float64, v)
        return Point(v.x * mult, v.y * mult)
    end

    function cross(w, v)
        return w.x * v.y - w.y * v.x
    end

    function lineSegementsIntersect(p, p2, q, q2)
        global infPoint

        r = sub(p2, p)
        s = sub(q2, q)
        rxs = cross(r, s)
#         qpxr = cross(sub(q, p), r)

        if rxs == 0#= && qpxr == 0=#
#             if ((0 <= mult(sub(q, p), r) && mult(sub(q, p), r) <= mult(r, r)) || (0 <= mult(sub(p, q), s) && mult(sub(p, q), s) <= mult(s, s)))
#                 println("fuck")
#             end

            return infPoint
        end

        t = cross(sub(q, p), s) / rxs
        u = cross(sub(q, p), r) / rxs

        if (0 <= t && t <= 1) && (0 <= u && u <= 1)
            return add(p, mulWithNum(t, r))
        end

        return infPoint
    end

    function manhattan(q)
        return abs.(q.x) + abs.(q.y)
    end
end