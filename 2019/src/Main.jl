#=day1=#
# include("./day1.jl")
# using .day1
#
# println("day 1 problem 1")
# day1.day1_1()
# println("---")
#
# println("day 1 problem 2")
# day1.day1_2()
# println("---")

#=day2=#
# include("./day2.jl")
# using .day2
#
# println("day 2 problem 1")
# day2.day2_1()
# println("---")
#
# println("day 2 problem 2")
# day2.day2_2()
# println("---")

#=day3=#
include("./day3.jl")
using .day3

println("day 3 problem 1")
day3.day3_1()
println("---")

println("day 3 problem 2")
day3.day3_2()
println("---")


