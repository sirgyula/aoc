module day2

export day2_1, day2_2

    global noun = 0
    global verb = 0

    function day2_1()
        input = parse.(Int64, split(getInput(), ","))
            @time begin
                # a tomb 1tol indexel genyo!
                input[2] = 12
                input[3] = 2

                i = 1

                while true
                    cmd = input[i]

                    if cmd == 99
                        break
                    end

                    arg1Pos = input[i + 1] + 1
                    arg2Pos = input[i + 2] + 1
                    resPos = input[i + 3] + 1

                    input[resPos] = doOperation(cmd, input[arg1Pos], input[arg2Pos])

                    i += 4
                end

                println(input[1])
            end
    end

    function day2_2()
        global noun
        global verb

        s = getInput()

        @time begin
            while true
                input = parse.(Int64, split(s, ","))

                # a tomb 1tol indexel genyo!
                input[2] = noun
                input[3] = verb

                i = 1

                while true
                    cmd = input[i]

                    if cmd == 99
                        break
                    end

                    arg1Pos = input[i + 1] + 1
                    arg2Pos = input[i + 2] + 1
                    resPos = input[i + 3] + 1

                    input[resPos] = doOperation(cmd, input[arg1Pos], input[arg2Pos])

                    i += 4
                end

                if input[1] == 19690720
                    println(100 * input[2] + input[3])
                    break
                else
                    if verb == 99
                        noun = noun + 1
                        verb = 0
                    else
                        verb = verb + 1
                    end
                end
            end
        end
    end

    function getInput()
        s = open("input/day2.txt") do file
            read(file, String)
        end

#         return parse.(Int64,split(s, ","))
        return s
    end

    function doOperation(cmd, arg1Value, arg2Value)
        operationResult::Int64 = 0

        if cmd == 1
            operationResult = add(arg1Value, arg2Value)
        elseif cmd == 2
            operationResult = multiply(arg1Value, arg2Value)
        elseif cmd == 99
            println("lolwut?! 99")
        else
            println(string("lolwut ", cmd))
        end

        return operationResult
    end

    function add(arg1Value, arg2Value)
        return arg1Value + arg2Value
    end

    function multiply(arg1Value, arg2Value)
        return arg1Value * arg2Value
    end

end