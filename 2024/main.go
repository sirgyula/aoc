package main

import (
	"aoc2024/d01"
	"aoc2024/d02"
	"aoc2024/d03"
	"aoc2024/d04"
	"aoc2024/d05"
	"aoc2024/d06"
	"aoc2024/d07"
	"aoc2024/d08"
	"aoc2024/d09"
	"aoc2024/d10"
	"aoc2024/d11"
	"aoc2024/d12"
	"aoc2024/d13"
	"aoc2024/d14"
	"aoc2024/d151"
	"aoc2024/d152"
)

func main() {

	switch 152 {
	case 1:
		d01.Run()
	case 2:
		d02.Run()
	case 3:
		d03.Run()
	case 4:
		d04.Run()
	case 5:
		d05.Run()
	case 6:
		d06.Run()
	case 7:
		d07.Run()
	case 8:
		d08.Run()
	case 9:
		d09.Run()
	case 10:
		d10.Run()
	case 11:
		d11.Run()
	case 12:
		d12.Run()
	case 13:
		d13.Run()
	case 14:
		d14.Run()
	case 151:
		d151.Run()
	case 152:
		d152.Run()
	}
}
