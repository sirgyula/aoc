package d13

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"regexp"
)

var re *regexp.Regexp

func solveLinearSystem(coefficients [2][2]int, constants utils.Coordinate) (int, int, error) {
	det := coefficients[0][0]*coefficients[1][1] - coefficients[0][1]*coefficients[1][0]
	if det == 0 {
		return 0, 0, fmt.Errorf("no unique solution")
	}

	detN := constants.X*coefficients[1][1] - constants.Y*coefficients[0][1]
	detM := coefficients[0][0]*constants.Y - coefficients[1][0]*constants.X

	if det != 1 && det != -1 {
		// check result integer
		if detN%det != 0 || detM%det != 0 {
			return 0, 0, fmt.Errorf("no integer solutions")
		}
	}

	return detN / det, detM / det, nil
}

func matchToCoord(match [][]string) utils.Coordinate {
	return utils.Coordinate{X: utils.Atoi(match[0][1]), Y: utils.Atoi(match[1][1])}
}

func parseParts(buttonA string, buttonB string, prize string, add int) (utils.Coordinate, utils.Coordinate, utils.Coordinate) {
	prizeCoords := matchToCoord(re.FindAllStringSubmatch(prize, -1))
	prizeCoords.X = prizeCoords.X + add
	prizeCoords.Y = prizeCoords.Y + add

	return matchToCoord(re.FindAllStringSubmatch(buttonA, -1)),
		matchToCoord(re.FindAllStringSubmatch(buttonB, -1)),
		prizeCoords
}

func solve(filepath string, add int) int {
	res := 0

	re = regexp.MustCompile(`(\d+)`)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		buttonA := fileScanner.Text()
		if buttonA == "" {
			continue
		}

		fileScanner.Scan()
		buttonB := fileScanner.Text()
		fileScanner.Scan()
		prize := fileScanner.Text()

		A, B, P := parseParts(buttonA, buttonB, prize, add)

		coefficients := [2][2]int{
			{A.X, B.X},
			{A.Y, B.Y},
		}

		aCnt, bCnt, _ := solveLinearSystem(coefficients, P)

		res += ((aCnt * 3) + bCnt)
	}

	return res
}

func Run() {
	fmt.Println(solve("d13/demo.txt", 0))
	fmt.Println(solve("d13/input.txt", 0))
	fmt.Println(solve("d13/demo.txt", 10000000000000))
	fmt.Println(solve("d13/input.txt", 10000000000000))
}
