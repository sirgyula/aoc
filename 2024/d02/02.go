package d02

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func isIncreasing(a, b int) bool {
	return a < b
}

func first(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		reports := utils.ToIntArray(strings.Split(fileScanner.Text(), " "))
		lvlCnt := len(reports)
		isGood := true

		firstPairIncreasing := isIncreasing(reports[0], reports[1])

		for i, v := range reports {
			// last item
			if i == lvlCnt-1 {
				break
			}

			nextV := reports[i+1]
			diff := utils.Abs(v - nextV)

			// changed direction or bad diff
			if (firstPairIncreasing != isIncreasing(v, nextV)) || (diff < 1 || diff > 3) {
				isGood = false
				break
			}
		}

		if isGood {
			res++
		}
	}

	return res
}

func handle(reports []int) bool {
	lvlCnt := len(reports)
	isGood := true

	firstPairIncreasing := isIncreasing(reports[0], reports[1])

	for i, v := range reports {
		// last item
		if i == lvlCnt-1 {
			break
		}

		nextV := reports[i+1]
		diff := utils.Abs(v - nextV)

		// changed direction or bad diff
		if (firstPairIncreasing != isIncreasing(v, nextV)) || (diff < 1 || diff > 3) {
			isGood = false
			break
		}
	}

	return isGood
}

func second(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		reports := utils.ToIntArray(strings.Split(fileScanner.Text(), " "))
		isSafe := handle(reports)

		if isSafe {
			res++
		} else {
			for i := 0; i < len(reports); i++ {
				newslice := make([]int, len(reports))
				copy(newslice, reports)
				reportsLight := utils.RemoveIndexFromSlice(newslice, i)
				isSafe := handle(reportsLight)

				if isSafe {
					res++
					break
				}
			}
		}

	}

	return res
}

func Run() {
	fmt.Println(first("d02/demo.txt"))
	fmt.Println(first("d02/input.txt"))
	fmt.Println(second("d02/demo.txt"))
	fmt.Println(second("d02/input.txt"))
}
