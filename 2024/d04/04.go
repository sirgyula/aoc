package d04

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var input [][]rune
var width, height int
var res = 0

func checkUp(ri int, ci int, r []rune) {
	for c := 1; c <= len(r)-1; c++ {
		if ri-c < 0 {
			return
		}

		if input[ri-c][ci] != r[c] {
			return
		}
	}

	// fmt.Printf("checkUp %d %d\n", ri, ci)
	res++
}

func checkDown(ri int, ci int, r []rune) {
	for c := 1; c <= len(r)-1; c++ {
		if ri+c > height {
			return
		}

		if input[ri+c][ci] != r[c] {
			return
		}
	}

	// fmt.Printf("checkDown %d %d\n", ri, ci)
	res++
}

func checkUpRight(ri int, ci int, r []rune) {
	for c := 1; c <= len(r)-1; c++ {
		if ri-c < 0 || ci+c > width {
			return
		}

		if input[ri-c][ci+c] != r[c] {
			return
		}
	}

	// fmt.Printf("checkUpRight %d %d\n", ri, ci)
	res++
}

func checkDownRight(ri int, ci int, r []rune) {
	for c := 1; c <= len(r)-1; c++ {
		if ri+c > height || ci+c > width {
			return
		}

		if input[ri+c][ci+c] != r[c] {
			return
		}
	}

	// fmt.Printf("checkDownRight %d %d\n", ri, ci)
	res++
}

func checkDownLeft(ri int, ci int, r []rune) {
	for c := 1; c <= len(r)-1; c++ {
		if ri+c > height || ci-c < 0 {
			return
		}

		if input[ri+c][ci-c] != r[c] {
			return
		}
	}

	// fmt.Printf("checkDownLeft %d %d\n", ri, ci)
	res++
}

func checkUpLeft(ri int, ci int, r []rune) {
	for c := 1; c <= len(r)-1; c++ {
		if ri-c < 0 || ci-c < 0 {
			return
		}

		if input[ri-c][ci-c] != r[c] {
			return
		}
	}

	// fmt.Printf("checkUpLeft %d %d\n", ri, ci)
	res++
}

func first(filepath string) int {
	res = 0
	input = make([][]rune, 0)
	xmas := []rune{'X', 'M', 'A', 'S'}

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := fileScanner.Text()

		res += strings.Count(row, "XMAS")
		res += strings.Count(row, "SAMX")

		input = append(input, []rune(row))
	}

	width = len(input[0]) - 1
	height = len(input) - 1

	for ri, rv := range input { // on rows
		for ci, cv := range rv {
			if cv == xmas[0] {
				checkUp(ri, ci, xmas)
				checkUpRight(ri, ci, xmas)
				checkDownRight(ri, ci, xmas)
				checkDown(ri, ci, xmas)
				checkDownLeft(ri, ci, xmas)
				checkUpLeft(ri, ci, xmas)
			}
		}
	}

	return res
}

func second(filepath string) int {
	res = 0
	input = make([][]rune, 0)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		input = append(input, []rune(fileScanner.Text()))
	}

	width = len(input[0]) - 1
	height = len(input) - 1

	for ri, rv := range input { // on rows
		if ri == 0 || ri == height {
			continue
		}

		for ci, cv := range rv {
			if ci == 0 || ci == width {
				continue
			}

			if cv == 'A' {
				a := string(input[ri-1][ci-1]) + string(input[ri-1][ci+1]) + string(input[ri+1][ci-1]) + string(input[ri+1][ci+1])

				if strings.Count(a, "M") == 2 && strings.Count(a, "S") == 2 && a != "MSSM" && a != "SMMS" {
					res++
				}
			}
		}
	}

	return res
}

func Run() {
	fmt.Println(first("d04/demo.txt"))
	fmt.Println(first("d04/input.txt"))
	fmt.Println(second("d04/demo.txt"))
	fmt.Println(second("d04/input.txt"))
}
