package d12

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
)

var height, width = 0, 0
var areas = make(map[rune][][]utils.Coordinate)
var directions = []utils.Coordinate{{X: -1, Y: 0}, {X: 1, Y: 0}, {X: 0, Y: -1}, {X: 0, Y: 1}}
var field = make([][]rune, 0)

func isInBounds(x int, y int) bool {
	return x >= 0 && y >= 0 && x <= width && y <= height
}

func addToArea(label rune, area []utils.Coordinate) {
	if _, ok := areas[label]; ok {
		areas[label] = append(areas[label], area)
	} else {
		aa := make([][]utils.Coordinate, 0)
		areas[label] = append(aa, area)
	}
}

func findCoordinates(label rune) {
	visited := make([][]bool, height+1)

	for i := range visited {
		visited[i] = make([]bool, width+1)
	}

	for x := 0; x <= height; x++ {
		for y := 0; y <= width; y++ {
			if field[x][y] == label && !visited[x][y] {
				area := []utils.Coordinate{}
				exploreNeighbours(label, x, y, visited, &area)
				addToArea(label, area)
			}
		}
	}
}

func exploreNeighbours(label rune, x, y int, visited [][]bool, area *[]utils.Coordinate) {
	visited[x][y] = true
	*area = append(*area, utils.Coordinate{X: x, Y: y})

	for _, d := range directions {
		newX, newY := x+d.X, y+d.Y

		if isInBounds(newX, newY) && field[newX][newY] == label && !visited[newX][newY] {
			exploreNeighbours(label, newX, newY, visited, area)
		}
	}
}

func getFenceCountForCoord(areaCoord utils.Coordinate) int {
	res := 0

	for _, d := range directions {
		newX, newY := areaCoord.X+d.X, areaCoord.Y+d.Y

		if !isInBounds(newX, newY) || field[newX][newY] != field[areaCoord.X][areaCoord.Y] {
			res++
		}
	}

	return res
}

func getFenceCountForCoordP2(areaCoord utils.Coordinate) int {
	res := 0

	for _, d := range directions {
		newX, newY := areaCoord.X+d.X, areaCoord.Y+d.Y

		if !isInBounds(newX, newY) || field[newX][newY] != field[areaCoord.X][areaCoord.Y] {
			res++
		}
	}

	return res
}

func first(filepath string) int {
	res := 0

	height, width = 0, 0
	areas = make(map[rune][][]utils.Coordinate)
	field = make([][]rune, 0)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := []rune(fileScanner.Text())

		field = append(field, row)
	}

	height = len(field) - 1
	width = len(field[0]) - 1

	for target := 'A'; target <= 'Z'; target++ {
		findCoordinates(target)
	}

	for _, areasOfChar := range areas {
		// fmt.Printf("checking %c: ", character)

		for _, area := range areasOfChar {
			fenceCntSum := 0
			for _, areaCoord := range area {
				fenceCntSum += getFenceCountForCoord(areaCoord)
			}

			res += (len(area) * fenceCntSum)
			// fmt.Printf("%d * %d | ", len(area), fenceCntSum)
		}

		// fmt.Printf("\n")
	}

	return res
}

func second(filepath string) int {
	res := 0

	height, width = 0, 0
	areas = make(map[rune][][]utils.Coordinate)
	field = make([][]rune, 0)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := []rune(fileScanner.Text())

		field = append(field, row)
	}

	height = len(field) - 1
	width = len(field[0]) - 1

	for target := 'A'; target <= 'Z'; target++ {
		findCoordinates(target)
	}

	for _, areasOfChar := range areas {
		// fmt.Printf("checking %c: ", character)

		for _, area := range areasOfChar {
			fenceCntSum := 0
			for _, areaCoord := range area {
				fenceCntSum += getFenceCountForCoordP2(areaCoord)
			}

			res += (len(area) * fenceCntSum)
			// fmt.Printf("%d * %d | ", len(area), fenceCntSum)
		}

		// fmt.Printf("\n")
	}

	return res
}

func Run() {
	// fmt.Println(first("d12/demo1.txt"))
	// fmt.Println(first("d12/demo2.txt"))
	// fmt.Println(first("d12/demo3.txt"))
	// fmt.Println(first("d12/inputmate.txt"))
	// fmt.Println(first("d12/input.txt"))
	// fmt.Println(second("d12/demo1.txt"))
	// fmt.Println(second("d12/demo2.txt"))
	// fmt.Println(second("d12/demo3.txt"))
	// fmt.Println(second("d12/demo4.txt"))
	fmt.Println(second("d12/demo5.txt"))
	fmt.Println(second("d12/input.txt"))
}
