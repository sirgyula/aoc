package utils

import "fmt"

// Define a struct for a coordinate pair
type Coordinate struct {
	X int
	Y int
}

func NewCoordinate() Coordinate {
	return Coordinate{}
}

func (c *Coordinate) SetX(x int) {
	c.X = x
}

func (c *Coordinate) SetY(y int) {
	c.Y = y
}

func (c *Coordinate) SetCoord(x, y int) {
	c.SetX(x)
	c.SetY(y)
}

func (c *Coordinate) ToString() string {
	return fmt.Sprintf("%d,%d", c.X, c.Y)
}
