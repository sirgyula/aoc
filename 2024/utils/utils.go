package utils

import (
	"fmt"
	"strconv"
)

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func ToIntArray(a []string) (res []int) {
	for _, v := range a {
		val, _ := strconv.Atoi(v)
		res = append(res, val)
	}

	return
}

func RemoveIndexFromSlice(s []int, i int) []int {
	return append(s[:i], s[i+1:]...)
}

func Atoi(s string) int {
	val, err := strconv.Atoi(s)

	if err != nil {
		fmt.Println(err)
	}
	return val
}
func Atoi64(s string) int64 {
	val, _ := strconv.Atoi(s)
	return int64(val)
}
