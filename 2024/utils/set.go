package utils

// Define a set type using a map with empty struct values
type Set[T comparable] map[T]struct{}

// Function to add an element to the set
func (s Set[T]) Add(value T) {
	s[value] = struct{}{}
}

// Function to check if an element is in the set
func (s Set[T]) Contains(value T) bool {
	_, exists := s[value]
	return exists
}

// Function to remove an element from the set
func (s Set[T]) Remove(value T) {
	delete(s, value)
}

// Function to get the size of the set
func (s Set[T]) Size() int {
	return len(s)
}

func (s Set[T]) ToSlice() []T {
	var slice []T
	for key := range s {
		slice = append(slice, key)
	}

	return slice
}
