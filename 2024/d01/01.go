package d01

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

func add(p string) int {
	num, _ := strconv.Atoi(p)
	return num
}

func first(filepath string) int {
	var leftNum []int
	var rightNum []int
	distance := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		parts := strings.Split(fileScanner.Text(), "   ")

		leftNum = append(leftNum, add(parts[0]))
		rightNum = append(rightNum, add(parts[1]))
	}

	slices.Sort(leftNum)
	slices.Sort(rightNum)

	for i, v := range rightNum {
		distance += utils.Abs(v - leftNum[i])
	}

	return distance
}

func second(filepath string) int {
	var leftNum []int
	var rightNum []int
	simScore := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		parts := strings.Split(fileScanner.Text(), "   ")

		leftNum = append(leftNum, add(parts[0]))
		rightNum = append(rightNum, add(parts[1]))
	}

	slices.Sort(leftNum)
	slices.Sort(rightNum)

	for _, vl := range leftNum {
		occ := 0

		for _, vr := range rightNum {
			if vr < vl {
				continue
			} else if vr == vl {
				occ++
			} else {
				break
			}
		}

		simScore += vl * occ
	}

	return simScore
}

func Run() {
	fmt.Println(first("d01/demo.txt"))
	fmt.Println(first("d01/input.txt"))
	fmt.Println(second("d01/demo.txt"))
	fmt.Println(second("d01/input.txt"))
}
