package d151

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

var wh = make([][]rune, 0)
var robot utils.Coordinate
var needPrint = false

func printWarehouse() {
	if !needPrint {
		return
	}
	fmt.Println()
	for _, line := range wh {
		fmt.Println(string(line))
	}
}

func isWall(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == '#'
}

func isEmpty(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == '.'
}

func isBox(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == 'O'
}

func moveLeft() {
	checking := utils.Coordinate{X: robot.X - 1, Y: robot.Y}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.X--
	} else if isBox(checking) {
		boxes := make([]utils.Coordinate, 0)
		boxes = append(boxes, checking)

		checkingBox := checking
		canPush := true

		for {
			checkingBox.X--
			if isBox(checkingBox) {
				boxes = append(boxes, checkingBox)
			} else if isWall(checkingBox) {
				canPush = false
				break
			} else {
				break
			}
		}

		if canPush {
			for i := len(boxes) - 1; i >= 0; i-- {
				box := boxes[i]
				wh[box.Y][box.X-1] = 'O'
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.X--
		}
	}
}

func moveUp() {
	checking := utils.Coordinate{X: robot.X, Y: robot.Y - 1}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.Y--
	} else if isBox(checking) {
		boxes := make([]utils.Coordinate, 0)
		boxes = append(boxes, checking)

		checkingBox := checking
		canPush := true

		for {
			checkingBox.Y--
			if isBox(checkingBox) {
				boxes = append(boxes, checkingBox)
			} else if isWall(checkingBox) {
				canPush = false
				break
			} else {
				break
			}
		}

		if canPush {
			for i := len(boxes) - 1; i >= 0; i-- {
				box := boxes[i]
				wh[box.Y-1][box.X] = 'O'
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.Y--
		}
	}
}

func moveRight() {
	checking := utils.Coordinate{X: robot.X + 1, Y: robot.Y}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.X++
	} else if isBox(checking) {
		boxes := make([]utils.Coordinate, 0)
		boxes = append(boxes, checking)

		checkingBox := checking
		canPush := true

		for {
			checkingBox.X++
			if isBox(checkingBox) {
				boxes = append(boxes, checkingBox)
			} else if isWall(checkingBox) {
				canPush = false
				break
			} else {
				break
			}
		}

		if canPush {
			for i := len(boxes) - 1; i >= 0; i-- {
				box := boxes[i]
				wh[box.Y][box.X+1] = 'O'
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.X++
		}
	}
}

func moveDown() {
	checking := utils.Coordinate{X: robot.X, Y: robot.Y + 1}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.Y++
	} else if isBox(checking) {
		boxes := make([]utils.Coordinate, 0)
		boxes = append(boxes, checking)

		checkingBox := checking
		canPush := true

		for {
			checkingBox.Y++
			if isBox(checkingBox) {
				boxes = append(boxes, checkingBox)
			} else if isWall(checkingBox) {
				canPush = false
				break
			} else {
				break
			}
		}

		if canPush {
			for i := len(boxes) - 1; i >= 0; i-- {
				box := boxes[i]
				wh[box.Y+1][box.X] = 'O'
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.Y++
		}
	}
}

func calculateResult() int {
	res := 0

	for y, row := range wh {
		for x, p := range row {
			if p == 'O' {
				res += ((100 * y) + x)
			}
		}
	}

	return res
}

func solve(filepath string) int {
	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	height := 0
	wh = make([][]rune, 0)
	robot = utils.NewCoordinate()

	for fileScanner.Scan() {
		line := fileScanner.Text()

		if line == "" {
			break
		}

		wh = append(wh, []rune(line))

		if strings.Contains(line, "@") {
			robot.X = strings.Index(line, "@")
			robot.Y = height
		}

		height++
	}

	printWarehouse()
	time.Sleep(1 * time.Millisecond)

	for fileScanner.Scan() {
		line := fileScanner.Text()

		for _, step := range line {
			if step == '^' {
				moveUp()
			} else if step == '>' {
				moveRight()
			} else if step == 'v' {
				moveDown()
			} else if step == '<' {
				moveLeft()
			}

			printWarehouse()
			time.Sleep(1 * time.Millisecond)
		}
	}

	return calculateResult()
}

func Run() {
	fmt.Println(solve("d151/demo1.txt"))
	fmt.Println(solve("d151/demo2.txt"))
	fmt.Println(solve("d151/input.txt"))
}
