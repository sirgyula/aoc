package d09

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
)

var explodedDisk []string

func build(input string) {
	emptyMarker := false
	id := 0

	for _, c := range input {
		if !emptyMarker {
			for i := 0; i < int(c)-48; i++ {
				explodedDisk = append(explodedDisk, fmt.Sprintf("%d", id))
			}

			id++
		} else {
			for i := 0; i < int(c)-48; i++ {
				explodedDisk = append(explodedDisk, ".")
			}
		}

		emptyMarker = !emptyMarker
	}
}

func getLastNonEmpty() string {
	for i := len(explodedDisk) - 1; i >= 0; i-- {
		if explodedDisk[i] != "." {
			c := explodedDisk[i]
			explodedDisk[i] = "."

			return c
		}
	}

	return ""
}

func isFinished(i int) bool {
	for k := i; k < len(explodedDisk); k++ {
		if explodedDisk[k] != "." {
			return false
		}
	}

	return true
}

func first(filepath string) int {
	res := 0
	explodedDisk = make([]string, 0)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	fileScanner.Scan()
	build(fileScanner.Text())

	for i, v := range explodedDisk {
		if v == "." && isFinished(i) {
			break
		} else if v == "." {
			v = getLastNonEmpty()
			explodedDisk[i] = v
		}

		res += (i * utils.Atoi(v))
	}

	return res
}

func Run() {
	fmt.Println(first("d09/demo.txt"))
	fmt.Println(first("d09/input.txt"))
	// fmt.Println(second("d09/demo.txt"))
	// fmt.Println(second("d09/input.txt"))
}
