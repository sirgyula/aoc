package d03

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"regexp"
)

func first(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	input := ""

	for fileScanner.Scan() {
		input += fileScanner.Text()
	}

	reMul := regexp.MustCompile(`(mul\(\d{1,3},\d{1,3}\))`)
	match := reMul.FindAllStringSubmatch(input, -1)

	reNum := regexp.MustCompile(`(\d{1,3})`)

	for _, v := range match {
		nums := reNum.FindAllStringSubmatch(v[1], -1)
		res += (utils.Atoi(nums[0][1]) * utils.Atoi(nums[1][1]))
	}

	return res
}

func second(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	input := ""

	for fileScanner.Scan() {
		input += fileScanner.Text()
	}

	reMul := regexp.MustCompile(`(mul\(\d{1,3},\d{1,3}\))|(do\(\))|(don't\(\))`)
	match := reMul.FindAllStringSubmatch(input, -1)

	reNum := regexp.MustCompile(`(\d{1,3})`)
	isOn := true

	for _, v := range match {
		if v[0] == "do()" {
			isOn = true
		} else if v[0] == "don't()" {
			isOn = false
		} else {
			if isOn {
				nums := reNum.FindAllStringSubmatch(v[1], -1)
				res += (utils.Atoi(nums[0][1]) * utils.Atoi(nums[1][1]))
			}
		}
	}

	return res
}

func Run() {
	fmt.Println(first("d03/demo01.txt"))
	fmt.Println(first("d03/input.txt"))
	fmt.Println(second("d03/demo02.txt"))
	fmt.Println(second("d03/input.txt"))
}
