package d14

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"regexp"

	"github.com/fogleman/gg"
)

var width, height = 0, 0
var splitX, splitY = 0, 0
var quadrants = make([]int, 4)
var grid = make([][]rune, 0)

func calculatePos(pos int, velo int, max int, seconds int) int {
	res := (pos + (velo * seconds)) % max

	if res < 0 {
		res = max - utils.Abs(res)
	}

	return res
}

func calculateAfterSeconds(pos utils.Coordinate, velo utils.Coordinate, seconds int) utils.Coordinate {
	return utils.Coordinate{
		X: calculatePos(pos.X, velo.X, width, seconds),
		Y: calculatePos(pos.Y, velo.Y, height, seconds),
	}
}

func addToQuadrant(pos utils.Coordinate) {
	q := -1

	if pos.X < splitX && pos.Y < splitY {
		q = 0
	} else if pos.X > splitX && pos.Y < splitY {
		q = 1
	} else if pos.X < splitX && pos.Y > splitY {
		q = 2
	} else if pos.X > splitX && pos.Y > splitY {
		q = 3
	} else {
		return
	}

	quadrants[q]++
}

func initGrid() {
	grid = make([][]rune, height)
	for i, _ := range grid {
		grid[i] = make([]rune, width)

		for j, _ := range grid[i] {
			grid[i][j] = ' '
		}
	}
}

func printGrid(seconds int) {
	cellSize := 20
	rows := len(grid)
	cols := len(grid[0])

	dc := gg.NewContext(cols*cellSize, rows*cellSize)

	for y, row := range grid {
		for x, char := range row {
			if char == '\u2588' {
				dc.SetRGB(0, 0, 0) // Black
			} else {
				dc.SetRGB(1, 1, 1) // White
			}
			dc.DrawRectangle(float64(x*cellSize), float64(y*cellSize), float64(cellSize), float64(cellSize))
			dc.Fill()
		}
	}

	dc.SavePNG(fmt.Sprintf("matrix_image_%d.png", seconds))
}

func isInBounds(x int, y int) bool {
	return x >= 0 && y >= 0 && x <= width-1 && y <= height-1
}

func first(filepath string, w int, h int, seconds int) int {
	width = w
	height = h
	splitX = (width - 1) / 2
	splitY = (height - 1) / 2
	quadrants = make([]int, 4)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	re := regexp.MustCompile(`(-?\d{1,})`)

	for fileScanner.Scan() {
		match := re.FindAllStringSubmatch(fileScanner.Text(), -1)
		pos := utils.Coordinate{X: utils.Atoi(match[0][1]), Y: utils.Atoi(match[1][1])}
		velo := utils.Coordinate{X: utils.Atoi(match[2][1]), Y: utils.Atoi(match[3][1])}

		finalCoord := calculateAfterSeconds(pos, velo, seconds)
		addToQuadrant(finalCoord)
	}

	return quadrants[0] * quadrants[1] * quadrants[2] * quadrants[3]
}

func second(filepath string, w int, h int) int {
	width = w
	height = h
	splitX = (width - 1) / 2
	splitY = (height - 1) / 2
	quadrants = make([]int, 4)

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	re := regexp.MustCompile(`(-?\d{1,})`)
	positions := make([]utils.Coordinate, 0)
	velocities := make([]utils.Coordinate, 0)

	for fileScanner.Scan() {
		match := re.FindAllStringSubmatch(fileScanner.Text(), -1)
		pos := utils.Coordinate{X: utils.Atoi(match[0][1]), Y: utils.Atoi(match[1][1])}
		velo := utils.Coordinate{X: utils.Atoi(match[2][1]), Y: utils.Atoi(match[3][1])}

		positions = append(positions, pos)
		velocities = append(velocities, velo)
	}

	seconds := 1

	for {
		if seconds == 10001 {
			break
		}

		initGrid()

		for i, p := range positions {
			coord := calculateAfterSeconds(p, velocities[i], 1)
			grid[coord.Y][coord.X] = '\u2588'
			positions[i] = coord
		}

		if seconds == 6771 {
			printGrid(seconds)
			break
		}

		seconds++
	}

	return 0
}

func Run() {
	// fmt.Println(first("d14/demo.txt", 11, 7, 100))
	// fmt.Println(first("d14/input.txt", 101, 103, 100))
	// fmt.Println(second("d14/demo.txt", 11, 7))
	fmt.Println(second("d14/input.txt", 101, 103))
}
