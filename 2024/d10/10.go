package d10

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
)

var topo [][]rune
var width, height int
var scoreOfCurrent utils.Set[utils.Coordinate]
var ratingOfCurrent []utils.Coordinate

func doP1(p utils.Coordinate, next rune) {
	neighbours := getNeighbouring(p, next)

	for _, n := range neighbours {
		if next == '9' {
			scoreOfCurrent.Add(n)
		}

		doP1(n, next+1)
	}
}

func doP2(p utils.Coordinate, next rune) {
	neighbours := getNeighbouring(p, next)

	for _, n := range neighbours {
		if next == '9' {
			ratingOfCurrent = append(ratingOfCurrent, n)
		}

		doP2(n, next+1)
	}
}

func getNeighbouring(p utils.Coordinate, next rune) []utils.Coordinate {
	neighbours := make([]utils.Coordinate, 0)

	c := utils.Coordinate{X: p.X - 1, Y: p.Y}

	if isInBounds(c) && topo[c.Y][c.X] == next {
		neighbours = append(neighbours, c)
	}

	c = utils.Coordinate{X: p.X + 1, Y: p.Y}

	if isInBounds(c) && topo[c.Y][c.X] == next {
		neighbours = append(neighbours, c)
	}

	c = utils.Coordinate{X: p.X, Y: p.Y - 1}

	if isInBounds(c) && topo[c.Y][c.X] == next {
		neighbours = append(neighbours, c)
	}

	c = utils.Coordinate{X: p.X, Y: p.Y + 1}

	if isInBounds(c) && topo[c.Y][c.X] == next {
		neighbours = append(neighbours, c)
	}

	return neighbours
}

func isInBounds(p utils.Coordinate) bool {
	return p.X >= 0 && p.Y >= 0 && p.X <= width && p.Y <= height
}

func first(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	height = 0
	trailheads := make([]utils.Coordinate, 0)
	topo = make([][]rune, 0)

	for fileScanner.Scan() {
		row := []rune(fileScanner.Text())

		for i, v := range row {
			if v == '0' {
				trailheads = append(trailheads, utils.Coordinate{X: i, Y: height})
			}
		}

		topo = append(topo, row)
		height++
	}

	height -= 1
	width = len(topo[0]) - 1

	for _, trailhead := range trailheads {
		scoreOfCurrent = make(utils.Set[utils.Coordinate])

		doP1(trailhead, '1')

		res += len(scoreOfCurrent)
	}

	return res
}

func second(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	height = 0
	trailheads := make([]utils.Coordinate, 0)
	topo = make([][]rune, 0)

	for fileScanner.Scan() {
		row := []rune(fileScanner.Text())

		for i, v := range row {
			if v == '0' {
				trailheads = append(trailheads, utils.Coordinate{X: i, Y: height})
			}
		}

		topo = append(topo, row)
		height++
	}

	height -= 1
	width = len(topo[0]) - 1

	for _, trailhead := range trailheads {
		ratingOfCurrent = make([]utils.Coordinate, 0)

		doP2(trailhead, '1')

		res += len(ratingOfCurrent)
	}

	return res
}

func Run() {
	fmt.Println(first("d10/demo.txt"))
	fmt.Println(first("d10/input.txt"))
	fmt.Println(second("d10/demo.txt"))
	fmt.Println(second("d10/input.txt"))
}
