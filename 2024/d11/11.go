package d11

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func addToMap(m map[int]int, label int, times int) {
	if _, ok := m[label]; ok {
		m[label] = (m[label] + 1)
	} else {
		m[label] = 1
	}

	if times > 1 {
		m[label] = (m[label] + times - 1)
	}
}

func solve(filepath string, maxBlinks int) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	fileScanner.Scan()
	stones := make(map[int]int)

	for _, s := range strings.Split(fileScanner.Text(), " ") {
		addToMap(stones, utils.Atoi(s), 1)
	}

	blinks := 1

	for {
		newMap := make(map[int]int)

		for stoneLabel, stoneCnt := range stones {
			if stoneLabel == 0 {
				addToMap(newMap, 1, stoneCnt)
			} else if len(strconv.Itoa(stoneLabel))%2 == 0 {
				a := strconv.Itoa(stoneLabel)
				addToMap(newMap, utils.Atoi(a[:len(a)/2]), stoneCnt)
				addToMap(newMap, utils.Atoi(a[len(a)/2:]), stoneCnt)
			} else {
				addToMap(newMap, stoneLabel*2024, stoneCnt)
			}
		}

		stones = newMap

		if blinks == maxBlinks {
			break
		}

		blinks++
	}

	for _, cnt := range stones {
		res += cnt
	}

	return res
}

func Run() {
	fmt.Println(solve("d11/demo.txt", 25))
	fmt.Println(solve("d11/input.txt", 25))
	fmt.Println(solve("d11/demo.txt", 75))
	fmt.Println(solve("d11/input.txt", 75))
}
