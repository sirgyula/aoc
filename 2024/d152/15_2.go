package d152

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"time"
)

var wh = make([][]rune, 0)
var robot utils.Coordinate
var needPrint = false
var printMs = time.Duration(1)
var boxes = make([]utils.Coordinate, 0)

func checkPushUp(start utils.Coordinate, end utils.Coordinate) bool {
	ok := false

	if wh[start.Y-1][start.X] == '.' && wh[end.Y-1][end.X] == '.' {
		ok = true
	} else if wh[start.Y-1][start.X] == '#' || wh[end.Y-1][end.X] == '#' {
		ok = false
	} else if wh[start.Y-1][start.X] == '[' || wh[end.Y-1][end.X] == ']' {
		ok = checkPushUp(utils.Coordinate{X: start.X, Y: start.Y - 1}, utils.Coordinate{X: end.X, Y: end.Y - 1})
	} else if wh[start.Y-1][start.X] == '.' && wh[end.Y-1][end.X] == '[' {
		ok = checkPushUp(utils.Coordinate{X: end.X, Y: end.Y - 1}, utils.Coordinate{X: end.X + 1, Y: end.Y - 1})
	} else if wh[start.Y-1][start.X] == ']' && wh[end.Y-1][end.X] == '.' {
		ok = checkPushUp(utils.Coordinate{X: start.X - 1, Y: start.Y - 1}, utils.Coordinate{X: start.X, Y: start.Y - 1})
	} else if wh[start.Y-1][start.X] == ']' && wh[end.Y-1][end.X] == '[' {
		ok = checkPushUp(utils.Coordinate{X: start.X - 1, Y: start.Y - 1}, utils.Coordinate{X: start.X, Y: start.Y - 1}) &&
			checkPushUp(utils.Coordinate{X: end.X, Y: end.Y - 1}, utils.Coordinate{X: end.X + 1, Y: start.Y - 1})
	} else {
		panic("?")
	}

	if ok {
		boxes = append(boxes, start, end)
	}

	return ok
}

func checkPushDown(start utils.Coordinate, end utils.Coordinate) bool {
	ok := false

	if wh[start.Y+1][start.X] == '.' && wh[end.Y+1][end.X] == '.' {
		ok = true
	} else if wh[start.Y+1][start.X] == '#' || wh[end.Y+1][end.X] == '#' {
		ok = false
	} else if wh[start.Y+1][start.X] == '[' || wh[end.Y+1][end.X] == ']' {
		ok = checkPushDown(utils.Coordinate{X: start.X, Y: start.Y + 1}, utils.Coordinate{X: end.X, Y: end.Y + 1})
	} else if wh[start.Y+1][start.X] == '.' && wh[end.Y+1][end.X] == '[' {
		ok = checkPushDown(utils.Coordinate{X: end.X, Y: end.Y + 1}, utils.Coordinate{X: end.X + 1, Y: end.Y + 1})
	} else if wh[start.Y+1][start.X] == ']' && wh[end.Y+1][end.X] == '.' {
		ok = checkPushDown(utils.Coordinate{X: start.X - 1, Y: start.Y + 1}, utils.Coordinate{X: start.X, Y: start.Y + 1})
	} else if wh[start.Y+1][start.X] == ']' && wh[end.Y+1][end.X] == '[' {
		ok = checkPushDown(utils.Coordinate{X: start.X - 1, Y: start.Y + 1}, utils.Coordinate{X: start.X, Y: start.Y + 1}) &&
			checkPushDown(utils.Coordinate{X: end.X, Y: end.Y + 1}, utils.Coordinate{X: end.X + 1, Y: start.Y + 1})
	} else {
		panic("?")
	}

	if ok {
		boxes = append(boxes, start, end)
	}

	return ok
}

func printWarehouse(step rune) {
	if !needPrint {
		return
	}
	fmt.Println()
	for _, line := range wh {
		fmt.Println(string(line))
	}
	fmt.Println(string(step))
}

func isWall(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == '#'
}

func isEmpty(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == '.'
}

func isBox(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == '[' || wh[c.Y][c.X] == ']'
}

func isBoxStart(c utils.Coordinate) bool {
	return wh[c.Y][c.X] == '['
}

func isAlreadyMoved(b utils.Coordinate, i int) bool {
	for j := 0; j < i; j++ {
		box := boxes[j]
		if box.X == b.X && box.Y == b.Y {
			return true
		}
	}

	return false
}

func moveLeft() {
	checking := utils.Coordinate{X: robot.X - 1, Y: robot.Y}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.X--
	} else if isBox(checking) {
		boxes := make([]utils.Coordinate, 0)
		boxes = append(boxes, checking)

		checkingBox := checking
		canPush := true

		for {
			checkingBox.X--
			if isBox(checkingBox) {
				boxes = append(boxes, checkingBox)
			} else if isWall(checkingBox) {
				canPush = false
				break
			} else {
				break
			}
		}

		boxOpen := true

		if canPush {
			for i := len(boxes) - 1; i >= 0; i-- {
				box := boxes[i]

				if boxOpen {
					wh[box.Y][box.X-1] = '['
				} else {
					wh[box.Y][box.X-1] = ']'
				}

				boxOpen = !boxOpen
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.X--
		}
	}
}

func moveUp() {
	checking := utils.Coordinate{X: robot.X, Y: robot.Y - 1}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.Y--
	} else if isBox(checking) {
		boxes = make([]utils.Coordinate, 0)
		canPush := true

		if isBoxStart(checking) {
			canPush = checkPushUp(checking, utils.Coordinate{X: checking.X + 1, Y: checking.Y})
		} else {
			canPush = checkPushUp(utils.Coordinate{X: checking.X - 1, Y: checking.Y}, checking)
		}

		if canPush {
			for i := 0; i < len(boxes); i++ {
				box := boxes[i]
				if isAlreadyMoved(box, i) {
					continue
				}

				wh[box.Y-1][box.X] = wh[box.Y][box.X]
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.Y--
		}
	}
}

func moveRight() {
	checking := utils.Coordinate{X: robot.X + 1, Y: robot.Y}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.X++
	} else if isBox(checking) {
		boxes := make([]utils.Coordinate, 0)
		boxes = append(boxes, checking)

		checkingBox := checking
		canPush := true

		for {
			checkingBox.X++
			if isBox(checkingBox) {
				boxes = append(boxes, checkingBox)
			} else if isWall(checkingBox) {
				canPush = false
				break
			} else {
				break
			}
		}

		boxClose := true

		if canPush {
			for i := len(boxes) - 1; i >= 0; i-- {
				box := boxes[i]

				if boxClose {
					wh[box.Y][box.X+1] = ']'
				} else {
					wh[box.Y][box.X+1] = '['
				}

				boxClose = !boxClose
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.X++
		}
	}
}

func moveDown() {
	checking := utils.Coordinate{X: robot.X, Y: robot.Y + 1}

	if isWall(checking) {
		return
	} else if isEmpty(checking) {
		wh[checking.Y][checking.X] = '@'
		wh[robot.Y][robot.X] = '.'
		robot.Y++
	} else if isBox(checking) {
		boxes = make([]utils.Coordinate, 0)
		canPush := true

		if isBoxStart(checking) {
			canPush = checkPushDown(checking, utils.Coordinate{X: checking.X + 1, Y: checking.Y})
		} else {
			canPush = checkPushDown(utils.Coordinate{X: checking.X - 1, Y: checking.Y}, checking)
		}

		if canPush {
			for i := 0; i < len(boxes); i++ {
				box := boxes[i]
				if isAlreadyMoved(box, i) {
					continue
				}

				wh[box.Y+1][box.X] = wh[box.Y][box.X]
				wh[box.Y][box.X] = '.'
			}

			wh[checking.Y][checking.X] = '@'
			wh[robot.Y][robot.X] = '.'
			robot.Y++
		}
	}
}

func calculateResult() int {
	res := 0

	for y, row := range wh {
		for x, p := range row {
			if p == '[' {
				res += ((100 * y) + x)
			}
		}
	}

	return res
}

func solve(filepath string) int {
	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	height := 0
	wh = make([][]rune, 0)
	robot = utils.NewCoordinate()

	for fileScanner.Scan() {
		line := fileScanner.Text()

		if line == "" {
			break
		}

		expanded := make([]rune, 0)

		for i, c := range []rune(line) {
			if c == '#' || c == '.' {
				expanded = append(expanded, c, c)
			} else if c == '@' {
				expanded = append(expanded, c, '.')
				robot = utils.Coordinate{X: i * 2, Y: height}
			} else if c == 'O' {
				expanded = append(expanded, '[', ']')
			}
		}

		wh = append(wh, expanded)

		height++
	}

	printWarehouse(' ')
	time.Sleep(printMs * time.Millisecond)

	for fileScanner.Scan() {
		line := fileScanner.Text()

		for _, step := range line {
			if step == '^' {
				moveUp()
			} else if step == '>' {
				moveRight()
			} else if step == 'v' {
				moveDown()
			} else if step == '<' {
				moveLeft()
			}

			printWarehouse(step)
			time.Sleep(printMs * time.Millisecond)
		}
	}

	return calculateResult()
}

func Run() {
	// fmt.Println(solve("d152/demo2.txt"))

	// fmt.Println(solve("d152/demo3.txt"))
	// fmt.Println(solve("d152/demo4.txt"))
	// fmt.Println(solve("d152/demo5.txt"))

	// fmt.Println(solve("d152/demo6.txt"))
	// fmt.Println(solve("d152/demo7.txt"))
	// fmt.Println(solve("d152/demo8.txt"))
	// fmt.Println(solve("d152/demo9.txt"))

	// fmt.Println(solve("d152/demo10.txt"))

	fmt.Println(solve("d152/input.txt"))
}
