package d05

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"slices"
	"strings"
)

func first(filepath string) int {
	res := 0
	rules := make(map[string]struct{})
	var orders []string

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := fileScanner.Text()

		if len(row) == 0 {
			continue
		} else if strings.Contains(row, "|") {
			rules[row] = struct{}{}
		} else {
			orders = append(orders, row)
		}
	}

	for _, o := range orders {
		isCorrect := true
		nums := strings.Split(o, ",")

		for i := 1; i < len(nums); i++ {
			for ii := 0; ii < i; ii++ {
				pair := nums[i] + "|" + nums[ii]

				if _, ok := rules[pair]; ok {
					isCorrect = false
					break
				}
			}
		}

		if isCorrect {
			res += utils.Atoi(nums[(len(nums)-1)/2])
		}
	}

	return res
}

func fixIt(nums []string, rules map[string]struct{}) int {
	var fixed []string

	for i, n := range nums {
		if i == 0 {
			fixed = append(fixed, n)
			continue
		}

		inserted := false

		for fi, fn := range fixed {
			pair := fmt.Sprintf("%s|%s", n, fn)

			if _, ok := rules[pair]; ok {
				fixed = slices.Insert(fixed, fi, n)
				inserted = true
				break
			}
		}

		if !inserted {
			fixed = append(fixed, n)
		}
	}

	return utils.Atoi(fixed[(len(fixed)-1)/2])
}

func second(filepath string) int {
	res := 0
	rules := make(map[string]struct{})
	var orders []string

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := fileScanner.Text()

		if len(row) == 0 {
			continue
		} else if strings.Contains(row, "|") {
			rules[row] = struct{}{}
		} else {
			orders = append(orders, row)
		}
	}

	for _, o := range orders {
		isCorrect := true
		nums := strings.Split(o, ",")

		for i := 1; i < len(nums); i++ {
			for ii := 0; ii < i; ii++ {
				pair := nums[i] + "|" + nums[ii]

				if _, ok := rules[pair]; ok {
					isCorrect = false
					break
				}
			}
		}

		if !isCorrect {
			res += fixIt(nums, rules)
		}
	}

	return res
}

func Run() {
	fmt.Println(first("d05/demo.txt"))
	fmt.Println(first("d05/input.txt"))
	fmt.Println(second("d05/demo.txt"))
	fmt.Println(second("d05/input.txt"))
}
