package d07

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
	"slices"
	"strings"
)

func check(num int, result int) int8 {
	if num == result {
		return 0 //found
	} else if num > result {
		return 1 //overshot
	}

	return -1 //undershot
}

func check64(num int64, result int64) int8 {
	if num == result {
		return 0 //found
	} else if num > result {
		return 1 //overshot
	}

	return -1 //undershot
}

func first(filepath string) int {
	res := 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := fileScanner.Text()

		splitterIndex := strings.Index(row, ":")

		result := utils.Atoi(row[:splitterIndex])
		operands := strings.Split(row[splitterIndex+2:], " ")

		var operandCandidates []int

		for oi, ov := range operands {
			if oi == 0 {
				operandCandidates = append(operandCandidates, utils.Atoi(ov))
				continue
			}

			var newCandidates []int
			found := false

			for _, cv := range operandCandidates {
				added := cv + utils.Atoi(ov)
				r := check(added, result)
				if r == -1 {
					newCandidates = append(newCandidates, added)
				} else if r == 0 {
					found = true
					break
				}

				muld := cv * utils.Atoi(ov)
				r = check(muld, result)
				if r == -1 {
					newCandidates = append(newCandidates, muld)
				} else if r == 0 {
					found = true
					break
				}
			}

			if found {
				res += result
				break
			}

			operandCandidates = make([]int, len(newCandidates))
			copy(operandCandidates, newCandidates)
		}
	}

	return res
}

func second(filepath string) int64 {
	var res int64 = 0

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		row := fileScanner.Text()

		splitterIndex := strings.Index(row, ":")

		var result int64 = utils.Atoi64(row[:splitterIndex])
		operands := strings.Split(row[splitterIndex+2:], " ")

		var operandCandidates []int64

		for oi, ov := range operands {
			if oi == 0 {
				operandCandidates = append(operandCandidates, utils.Atoi64(ov))
				continue
			}

			var newCandidates []int64

			for _, cv := range operandCandidates {
				added := cv + utils.Atoi64(ov)
				r := check64(added, result)
				if r != 1 {
					newCandidates = append(newCandidates, added)
				}

				muld := cv * utils.Atoi64(ov)
				r = check64(muld, result)
				if r != 1 {
					newCandidates = append(newCandidates, muld)
				}

				concated := utils.Atoi64(fmt.Sprintf("%d%s", cv, ov))
				r = check64(concated, result)
				if r != 1 {
					newCandidates = append(newCandidates, concated)
				}
			}

			operandCandidates = make([]int64, len(newCandidates))
			copy(operandCandidates, newCandidates)
		}

		if slices.Contains(operandCandidates, result) {
			res += result
		}
	}

	return res
}

func Run() {
	fmt.Println(first("d07/demo.txt"))
	fmt.Println(first("d07/input.txt"))
	fmt.Println(second("d07/demo.txt"))
	fmt.Println(second("d07/dani.txt"))
	fmt.Println(second("d07/input.txt"))
}
