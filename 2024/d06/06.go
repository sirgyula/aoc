package d06

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
)

func between(a int, b int, c int) bool {
	return a < b && b < c
}

func getObstacleAhead(g utils.Coordinate, facing *int8, obstacles utils.Set[utils.Coordinate]) (utils.Coordinate, bool) {
	// 0:^ 1:> 2:V 3:<
	p := utils.NewCoordinate()
	exiting := true

	switch *facing {
	case 0:
		p = utils.Coordinate{Y: -1}
		for o := range obstacles {
			if o.X == g.X && between(p.Y, o.Y, g.Y) {
				p = o
				exiting = false
			}
		}
	case 1:
		p = utils.Coordinate{X: 99999}
		for o := range obstacles {
			if o.Y == g.Y && between(g.X, o.X, p.X) {
				p = o
				exiting = false
			}
		}
	case 2:
		p = utils.Coordinate{Y: 99999}
		for o := range obstacles {
			if o.X == g.X && between(g.Y, o.Y, p.Y) {
				p = o
				exiting = false
			}
		}
	case 3:
		p = utils.Coordinate{X: -1}
		for o := range obstacles {
			if o.Y == g.Y && between(p.X, o.X, g.X) {
				p = o
				exiting = false
			}
		}
	}

	return p, exiting
}

func first(filepath string) int {
	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	var width, height int
	steps := make(utils.Set[utils.Coordinate])
	obstacles := make(utils.Set[utils.Coordinate])
	guard := utils.NewCoordinate()
	var facing int8 = 0
	exitingSteps := 0

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	y := 0

	for fileScanner.Scan() {
		row := fileScanner.Text()
		width = len(row)

		for x, ch := range []rune(row) {
			if ch == '#' {
				obstacles.Add(utils.Coordinate{X: x, Y: y})
			} else if ch == '^' {
				guard.SetCoord(x, y)
				steps.Add(guard)
			}
		}

		y++
	}

	height = y

	// print(width)

	for {
		if exitingSteps != 0 {
			break
		}

		obstacleAhead, exiting := getObstacleAhead(guard, &facing, obstacles)

		switch facing {
		case 0:
			if exiting {
				exitingSteps = guard.Y
				break
			}

			dist := guard.Y - obstacleAhead.Y - 1

			for i := 1; i <= dist; i++ {
				c := utils.Coordinate{X: guard.X, Y: guard.Y - i}
				fmt.Printf("%v\n", c.ToString())
				steps.Add(utils.Coordinate{X: guard.X, Y: guard.Y - i})
			}

			guard.Y = obstacleAhead.Y + 1

			facing = 1
		case 1:
			if exiting {
				exitingSteps = width - guard.X - 1
				break
			}

			dist := obstacleAhead.X - guard.X - 1

			for i := 1; i <= dist; i++ {
				fmt.Printf("%v\n", utils.Coordinate{X: guard.X + i, Y: guard.Y})
				steps.Add(utils.Coordinate{X: guard.X + i, Y: guard.Y})
			}

			guard.X = obstacleAhead.X - 1

			facing = 2
		case 2:
			if exiting {
				exitingSteps = height - guard.Y - 1
				break
			}

			dist := obstacleAhead.Y - guard.Y - 1

			for i := 1; i <= dist; i++ {
				fmt.Printf("%v\n", utils.Coordinate{X: guard.X, Y: guard.Y + i})
				steps.Add(utils.Coordinate{X: guard.X, Y: guard.Y + i})
			}

			guard.Y = obstacleAhead.Y - 1

			facing = 3
		case 3:
			if exiting {
				exitingSteps = guard.X
				break
			}

			dist := guard.X - obstacleAhead.X - 1

			for i := 1; i <= dist; i++ {
				fmt.Printf("%v\n", utils.Coordinate{X: guard.X - i, Y: guard.Y})
				steps.Add(utils.Coordinate{X: guard.X - i, Y: guard.Y})
			}

			guard.X = obstacleAhead.X + 1

			facing = 0
		}
	}

	return len(steps) + exitingSteps
}

func Run() {
	fmt.Println(first("d06/demo.txt"))
	fmt.Println(first("d06/input.txt"))
	// fmt.Println(second("d06/demo.txt"))
	// fmt.Println(second("d06/input.txt"))
}
