package d08

import (
	"aoc2024/utils"
	"bufio"
	"fmt"
	"os"
)

func getVector(a utils.Coordinate, b utils.Coordinate) utils.Coordinate {
	return utils.Coordinate{X: b.X - a.X, Y: b.Y - a.Y}
}

func mirrorPoint(a utils.Coordinate, b utils.Coordinate) utils.Coordinate {
	vector := getVector(a, b)

	return utils.Coordinate{
		X: a.X - vector.X,
		Y: a.Y - vector.Y,
	}
}

func isInBounds(p utils.Coordinate, width int, height int) bool {
	return p.X >= 0 && p.Y >= 0 && p.X <= width && p.Y <= height
}

func resonants(a utils.Coordinate, b utils.Coordinate, width int, height int) []utils.Coordinate {
	vector := getVector(a, b)
	resonantCoords := make([]utils.Coordinate, 0)

	c := utils.Coordinate{X: a.X, Y: a.Y}
	resonantCoords = append(resonantCoords, c)

	for {
		c = utils.Coordinate{
			X: c.X - vector.X,
			Y: c.Y - vector.Y,
		}

		if isInBounds(c, width, height) {
			resonantCoords = append(resonantCoords, c)
		} else {
			break
		}
	}

	return resonantCoords
}

func first(filepath string) int {
	res := make(utils.Set[utils.Coordinate])

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	height, width := 0, 0
	antennas := make(map[rune][]utils.Coordinate, 0)

	for fileScanner.Scan() {
		row := fileScanner.Text()
		width = len(row) - 1

		for i, v := range []rune(row) {
			if v == '.' {
				continue
			}

			antennas[v] = append(antennas[v], utils.Coordinate{X: i, Y: height})
		}
		height++
	}

	height -= 1

	for _, antennasOfFreq := range antennas {
		// starting from zero, calculate mirrored points of every antenna pair
		for ai, av := range antennasOfFreq {
			for ai2 := ai + 1; ai2 < len(antennasOfFreq); ai2++ {
				mp1 := mirrorPoint(av, antennasOfFreq[ai2])
				mp2 := mirrorPoint(antennasOfFreq[ai2], av)

				if isInBounds(mp1, width, height) {
					res.Add(mp1)
				}

				if isInBounds(mp2, width, height) {
					res.Add(mp2)
				}
			}
		}
	}

	return len(res)
}

func second(filepath string) int {
	res := make(utils.Set[utils.Coordinate])

	readFile, err := os.Open(filepath)

	if err != nil {
		fmt.Println(err)
	}
	defer readFile.Close()

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	height, width := 0, 0
	antennas := make(map[rune][]utils.Coordinate, 0)

	for fileScanner.Scan() {
		row := fileScanner.Text()
		width = len(row) - 1

		for i, v := range []rune(row) {
			if v == '.' {
				continue
			}

			antennas[v] = append(antennas[v], utils.Coordinate{X: i, Y: height})
		}
		height++
	}

	height -= 1

	for _, antennasOfFreq := range antennas {
		// starting from zero, calculate resonants of every antenna pair
		for ai, av := range antennasOfFreq {
			for ai2 := ai + 1; ai2 < len(antennasOfFreq); ai2++ {
				ss1 := resonants(av, antennasOfFreq[ai2], width, height)
				for _, v := range ss1 {
					res.Add(v)
				}

				ss2 := resonants(antennasOfFreq[ai2], av, width, height)
				for _, v := range ss2 {
					res.Add(v)
				}
			}
		}
	}

	return len(res)
}

func Run() {
	fmt.Println(first("d08/demo.txt"))
	fmt.Println(first("d08/input.txt"))
	fmt.Println(second("d08/demo.txt"))
	fmt.Println(second("d08/input.txt"))
}
