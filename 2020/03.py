import timeit
import re


class Coord:
    def __init__(self, maplen, stepR, stepD):
        self.r = 0
        self.c = 0
        self.stepR = stepR
        self.stepD = stepD
        self.maplen = maplen

    def step(self):
        self.r += self.stepD
        self.c = \
            self.c + self.stepR - self.maplen + 1 if self.c + self.stepR >= self.maplen - 1 else self.c + self.stepR

    def check(self, line):
        return True if line[self.c] == "#" else False

    def __str__(self):
        return str(self.r) + " " + str(self.c)


def calculateHits(lines, stepR, stepD):
    hits = 0

    coord = Coord(len(lines[0]), stepR, stepD)
    coord.step()
    i = stepD

    while i < len(lines):
        hits = hits + 1 if coord.check(lines[i]) else hits
        coord.step()
        i += stepD

    return hits


def first(filename):
    lines = open(filename, "r").readlines()

    return calculateHits(lines, 3, 1)


def second(filename):
    lines = open(filename, "r").readlines()

    return calculateHits(lines, 1, 1) * calculateHits(lines, 3, 1) * calculateHits(lines, 5, 1) * \
        calculateHits(lines, 7, 1) * calculateHits(lines, 1, 2)


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("030.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("031.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("030.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("031.txt"))), number=1))
