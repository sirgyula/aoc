import timeit
import re


class Validator:
    def __init__(self):
        self.cnt = 0
        self.hasCid = False

    def validate(self, props):
        for prop in props:
            kv = prop.split(":")
            k = kv[0]
            v = kv[1]

            result = {
                'byr': lambda x: self.byr(x),
                'iyr': lambda x: self.iyr(x),
                'eyr': lambda x: self.eyr(x),
                'hgt': lambda x: self.hgt(x),
                'hcl': lambda x: self.hcl(x),
                'ecl': lambda x: self.ecl(x),
                'pid': lambda x: self.pid(x),
                'cid': lambda x: self.cid(x)
            }[k](v)

            if not result:
                return False

        if (self.hasCid and self.cnt == 8) or (not self.hasCid and self.cnt == 7):
            return True
        else:
            return False

    def byr(self, v):
        self.cnt += 1
        return True if 1920 <= int(v) <= 2002 else False

    def iyr(self, v):
        self.cnt += 1
        return True if 2010 <= int(v) <= 2020 else False

    def eyr(self, v):
        self.cnt += 1
        return True if 2020 <= int(v) <= 2030 else False

    def hgt(self, v: str):
        self.cnt += 1
        if v.endswith("cm"):
            return True if 150 <= int(v.rstrip("cm")) <= 193 else False
        elif v.endswith("in"):
            return True if 59 <= int(v.rstrip("in")) <= 76 else False

    def hcl(self, v: str):
        self.cnt += 1
        return True if len(v) == 7 and re.match(r"^#[a-fA-F0-9]+$", v) else False

    def ecl(self, v):
        self.cnt += 1
        return True if any(v in c for c in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']) else False

    def pid(self, v: str):
        self.cnt += 1
        return True if re.match(r'^[0-9]{9}$', v) else False

    def cid(self, v):
        self.cnt += 1
        self.hasCid = True
        return True


def first(filename):
    props = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    content = open(filename, "r").read().rstrip()
    creds = re.split(r'\n\n', content)

    return len([cred for cred in creds if all(p in cred for p in props)])


def second(filename):
    content = open(filename, "r").read().rstrip()
    creds = re.split(r'\n\n', content)
    valid = 0

    for cred in creds:
        validator = Validator()
        single = cred.rstrip().lstrip().replace('\n', ' ')
        props = list(re.findall(r'([a-z0-9]+:[a-z0-9#]+)[ ]?', single))
        isValid = validator.validate(props)

        if isValid:
            valid += 1

    return valid


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("040.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("041.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("040.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("041.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("042.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("043.txt"))), number=1))
