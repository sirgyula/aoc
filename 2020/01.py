import timeit


def first(filename):
    lines = list([int(line) for line in open(filename, "r")])
    l = len(lines)

    for i in range(0, l):
        a = int(lines[i])

        for j in range(i + 1, l):
            if a + lines[j] == 2020:
                return a * lines[j]

    return -1


def second(filename):
    lines = list([int(line) for line in open(filename, "r")])
    l = len(lines)

    for i in range(0, l):
        a = int(lines[i])

        for j in range(i + 1, l):
            b = int(lines[j])

            for k in range(j + 1, l):
                if a + b + lines[k] == 2020:
                    return a * b * lines[k]

    return -1


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("010.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("011.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("010.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("011.txt"))), number=1))
