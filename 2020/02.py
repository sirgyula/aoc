import timeit
import re


def first(filename):
    pws = list([re.search(r'^([0-9]+)-([0-9]+)\s([a-z]+):\s([a-z]+)$', line.rstrip()).groups() for line in open(filename, "r")])
    checked = [int(pw[0]) <= pw[3].count(pw[2]) <= int(pw[1]) for pw in pws]
    return len(list(filter(lambda i: i, checked)))


def second(filename):
    pws = list([re.search(r'^([0-9]+)-([0-9]+)\s([a-z]+):\s([a-z]+)$', line.rstrip()).groups() for line in open(filename, "r")])
    checked = [bool(pw[3][int(pw[0]) - 1] == pw[2]) != bool(pw[3][int(pw[1]) - 1] == pw[2]) for pw in pws]
    return len(list(filter(lambda i: i, checked)))


if __name__ == '__main__':
    print(timeit.timeit(lambda: print("1-0: " + str(first("020.txt"))), number=1))
    print(timeit.timeit(lambda: print("1-1: " + str(first("021.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-0: " + str(second("020.txt"))), number=1))
    print(timeit.timeit(lambda: print("2-1: " + str(second("021.txt"))), number=1))
